﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t1153838500_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0 = { 1, GenInst_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType Char_t2862622538_0_0_0;
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0 = { 1, GenInst_Char_t2862622538_0_0_0_Types };
extern const Il2CppType IConvertible_t2116191568_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2116191568_0_0_0_Types[] = { &IConvertible_t2116191568_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2116191568_0_0_0 = { 1, GenInst_IConvertible_t2116191568_0_0_0_Types };
extern const Il2CppType IComparable_t1391370361_0_0_0;
static const Il2CppType* GenInst_IComparable_t1391370361_0_0_0_Types[] = { &IComparable_t1391370361_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1391370361_0_0_0 = { 1, GenInst_IComparable_t1391370361_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772552329_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772552329_0_0_0_Types[] = { &IComparable_1_t2772552329_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772552329_0_0_0 = { 1, GenInst_IComparable_1_t2772552329_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411901105_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411901105_0_0_0_Types[] = { &IEquatable_1_t2411901105_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411901105_0_0_0 = { 1, GenInst_IEquatable_1_t2411901105_0_0_0_Types };
extern const Il2CppType ValueType_t1744280289_0_0_0;
static const Il2CppType* GenInst_ValueType_t1744280289_0_0_0_Types[] = { &ValueType_t1744280289_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1744280289_0_0_0 = { 1, GenInst_ValueType_t1744280289_0_0_0_Types };
extern const Il2CppType Int64_t1153838595_0_0_0;
static const Il2CppType* GenInst_Int64_t1153838595_0_0_0_Types[] = { &Int64_t1153838595_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1153838595_0_0_0 = { 1, GenInst_Int64_t1153838595_0_0_0_Types };
extern const Il2CppType UInt32_t24667981_0_0_0;
static const Il2CppType* GenInst_UInt32_t24667981_0_0_0_Types[] = { &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t24667981_0_0_0 = { 1, GenInst_UInt32_t24667981_0_0_0_Types };
extern const Il2CppType UInt64_t24668076_0_0_0;
static const Il2CppType* GenInst_UInt64_t24668076_0_0_0_Types[] = { &UInt64_t24668076_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t24668076_0_0_0 = { 1, GenInst_UInt64_t24668076_0_0_0_Types };
extern const Il2CppType Byte_t2862609660_0_0_0;
static const Il2CppType* GenInst_Byte_t2862609660_0_0_0_Types[] = { &Byte_t2862609660_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2862609660_0_0_0 = { 1, GenInst_Byte_t2862609660_0_0_0_Types };
extern const Il2CppType SByte_t1161769777_0_0_0;
static const Il2CppType* GenInst_SByte_t1161769777_0_0_0_Types[] = { &SByte_t1161769777_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1161769777_0_0_0 = { 1, GenInst_SByte_t1161769777_0_0_0_Types };
extern const Il2CppType Int16_t1153838442_0_0_0;
static const Il2CppType* GenInst_Int16_t1153838442_0_0_0_Types[] = { &Int16_t1153838442_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1153838442_0_0_0 = { 1, GenInst_Int16_t1153838442_0_0_0_Types };
extern const Il2CppType UInt16_t24667923_0_0_0;
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_Types[] = { &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0 = { 1, GenInst_UInt16_t24667923_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t3464557803_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t3464557803_0_0_0_Types[] = { &IEnumerable_t3464557803_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t3464557803_0_0_0 = { 1, GenInst_IEnumerable_t3464557803_0_0_0_Types };
extern const Il2CppType ICloneable_t1025544834_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1025544834_0_0_0_Types[] = { &ICloneable_t1025544834_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1025544834_0_0_0 = { 1, GenInst_ICloneable_t1025544834_0_0_0_Types };
extern const Il2CppType IComparable_1_t4212128644_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4212128644_0_0_0_Types[] = { &IComparable_1_t4212128644_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4212128644_0_0_0 = { 1, GenInst_IComparable_1_t4212128644_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3851477420_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3851477420_0_0_0_Types[] = { &IEquatable_1_t3851477420_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3851477420_0_0_0 = { 1, GenInst_IEquatable_1_t3851477420_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t2853506214_0_0_0;
static const Il2CppType* GenInst_IReflect_t2853506214_0_0_0_Types[] = { &IReflect_t2853506214_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2853506214_0_0_0 = { 1, GenInst_IReflect_t2853506214_0_0_0_Types };
extern const Il2CppType _Type_t2149739635_0_0_0;
static const Il2CppType* GenInst__Type_t2149739635_0_0_0_Types[] = { &_Type_t2149739635_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2149739635_0_0_0 = { 1, GenInst__Type_t2149739635_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1425685797_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types[] = { &ICustomAttributeProvider_t1425685797_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1425685797_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3353101921_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3353101921_0_0_0_Types[] = { &_MemberInfo_t3353101921_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3353101921_0_0_0 = { 1, GenInst__MemberInfo_t3353101921_0_0_0_Types };
extern const Il2CppType IFormattable_t382002946_0_0_0;
static const Il2CppType* GenInst_IFormattable_t382002946_0_0_0_Types[] = { &IFormattable_t382002946_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t382002946_0_0_0 = { 1, GenInst_IFormattable_t382002946_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772539451_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772539451_0_0_0_Types[] = { &IComparable_1_t2772539451_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772539451_0_0_0 = { 1, GenInst_IComparable_1_t2772539451_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411888227_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411888227_0_0_0_Types[] = { &IEquatable_1_t2411888227_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411888227_0_0_0 = { 1, GenInst_IEquatable_1_t2411888227_0_0_0_Types };
extern const Il2CppType Single_t4291918972_0_0_0;
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0 = { 1, GenInst_Single_t4291918972_0_0_0_Types };
extern const Il2CppType IComparable_1_t4201848763_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4201848763_0_0_0_Types[] = { &IComparable_1_t4201848763_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4201848763_0_0_0 = { 1, GenInst_IComparable_1_t4201848763_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3841197539_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3841197539_0_0_0_Types[] = { &IEquatable_1_t3841197539_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3841197539_0_0_0 = { 1, GenInst_IEquatable_1_t3841197539_0_0_0_Types };
extern const Il2CppType Double_t3868226565_0_0_0;
static const Il2CppType* GenInst_Double_t3868226565_0_0_0_Types[] = { &Double_t3868226565_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t3868226565_0_0_0 = { 1, GenInst_Double_t3868226565_0_0_0_Types };
extern const Il2CppType Decimal_t1954350631_0_0_0;
static const Il2CppType* GenInst_Decimal_t1954350631_0_0_0_Types[] = { &Decimal_t1954350631_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1954350631_0_0_0 = { 1, GenInst_Decimal_t1954350631_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768291_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768291_0_0_0_Types[] = { &IComparable_1_t1063768291_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768291_0_0_0 = { 1, GenInst_IComparable_1_t1063768291_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117067_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117067_0_0_0_Types[] = { &IEquatable_1_t703117067_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117067_0_0_0 = { 1, GenInst_IEquatable_1_t703117067_0_0_0_Types };
extern const Il2CppType Boolean_t476798718_0_0_0;
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0 = { 1, GenInst_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Delegate_t3310234105_0_0_0;
static const Il2CppType* GenInst_Delegate_t3310234105_0_0_0_Types[] = { &Delegate_t3310234105_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3310234105_0_0_0 = { 1, GenInst_Delegate_t3310234105_0_0_0_Types };
extern const Il2CppType ISerializable_t867484142_0_0_0;
static const Il2CppType* GenInst_ISerializable_t867484142_0_0_0_Types[] = { &ISerializable_t867484142_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t867484142_0_0_0 = { 1, GenInst_ISerializable_t867484142_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2235474049_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2235474049_0_0_0_Types[] = { &ParameterInfo_t2235474049_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2235474049_0_0_0 = { 1, GenInst_ParameterInfo_t2235474049_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2787166306_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2787166306_0_0_0_Types[] = { &_ParameterInfo_t2787166306_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2787166306_0_0_0 = { 1, GenInst__ParameterInfo_t2787166306_0_0_0_Types };
extern const Il2CppType ParameterModifier_t741930026_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t741930026_0_0_0_Types[] = { &ParameterModifier_t741930026_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t741930026_0_0_0 = { 1, GenInst_ParameterModifier_t741930026_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565010_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565010_0_0_0_Types[] = { &IComparable_1_t4229565010_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565010_0_0_0 = { 1, GenInst_IComparable_1_t4229565010_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913786_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913786_0_0_0_Types[] = { &IEquatable_1_t3868913786_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913786_0_0_0 = { 1, GenInst_IEquatable_1_t3868913786_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565068_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565068_0_0_0_Types[] = { &IComparable_1_t4229565068_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565068_0_0_0 = { 1, GenInst_IComparable_1_t4229565068_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913844_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913844_0_0_0_Types[] = { &IEquatable_1_t3868913844_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913844_0_0_0 = { 1, GenInst_IEquatable_1_t3868913844_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565163_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565163_0_0_0_Types[] = { &IComparable_1_t4229565163_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565163_0_0_0 = { 1, GenInst_IComparable_1_t4229565163_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913939_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913939_0_0_0_Types[] = { &IEquatable_1_t3868913939_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913939_0_0_0 = { 1, GenInst_IEquatable_1_t3868913939_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768233_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768233_0_0_0_Types[] = { &IComparable_1_t1063768233_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768233_0_0_0 = { 1, GenInst_IComparable_1_t1063768233_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117009_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117009_0_0_0_Types[] = { &IEquatable_1_t703117009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117009_0_0_0 = { 1, GenInst_IEquatable_1_t703117009_0_0_0_Types };
extern const Il2CppType IComparable_1_t1071699568_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1071699568_0_0_0_Types[] = { &IComparable_1_t1071699568_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1071699568_0_0_0 = { 1, GenInst_IComparable_1_t1071699568_0_0_0_Types };
extern const Il2CppType IEquatable_1_t711048344_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t711048344_0_0_0_Types[] = { &IEquatable_1_t711048344_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t711048344_0_0_0 = { 1, GenInst_IEquatable_1_t711048344_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768386_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768386_0_0_0_Types[] = { &IComparable_1_t1063768386_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768386_0_0_0 = { 1, GenInst_IComparable_1_t1063768386_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117162_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117162_0_0_0_Types[] = { &IEquatable_1_t703117162_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117162_0_0_0 = { 1, GenInst_IEquatable_1_t703117162_0_0_0_Types };
extern const Il2CppType IComparable_1_t3778156356_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3778156356_0_0_0_Types[] = { &IComparable_1_t3778156356_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3778156356_0_0_0 = { 1, GenInst_IComparable_1_t3778156356_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3417505132_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3417505132_0_0_0_Types[] = { &IEquatable_1_t3417505132_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3417505132_0_0_0 = { 1, GenInst_IEquatable_1_t3417505132_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t209867187_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t209867187_0_0_0_Types[] = { &_FieldInfo_t209867187_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t209867187_0_0_0 = { 1, GenInst__FieldInfo_t209867187_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3971289384_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3971289384_0_0_0_Types[] = { &_MethodInfo_t3971289384_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3971289384_0_0_0 = { 1, GenInst__MethodInfo_t3971289384_0_0_0_Types };
extern const Il2CppType MethodBase_t318515428_0_0_0;
static const Il2CppType* GenInst_MethodBase_t318515428_0_0_0_Types[] = { &MethodBase_t318515428_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t318515428_0_0_0 = { 1, GenInst_MethodBase_t318515428_0_0_0_Types };
extern const Il2CppType _MethodBase_t3971068747_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3971068747_0_0_0_Types[] = { &_MethodBase_t3971068747_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3971068747_0_0_0 = { 1, GenInst__MethodBase_t3971068747_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t4136801618_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t4136801618_0_0_0_Types[] = { &ConstructorInfo_t4136801618_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t4136801618_0_0_0 = { 1, GenInst_ConstructorInfo_t4136801618_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3408715251_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3408715251_0_0_0_Types[] = { &_ConstructorInfo_t3408715251_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3408715251_0_0_0 = { 1, GenInst__ConstructorInfo_t3408715251_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t3372848153_0_0_0;
static const Il2CppType* GenInst_TableRange_t3372848153_0_0_0_Types[] = { &TableRange_t3372848153_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t3372848153_0_0_0 = { 1, GenInst_TableRange_t3372848153_0_0_0_Types };
extern const Il2CppType TailoringInfo_t3025807515_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t3025807515_0_0_0_Types[] = { &TailoringInfo_t3025807515_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t3025807515_0_0_0 = { 1, GenInst_TailoringInfo_t3025807515_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3222658402_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0 = { 1, GenInst_KeyValuePair_2_t3222658402_0_0_0_Types };
extern const Il2CppType Link_t2063667470_0_0_0;
static const Il2CppType* GenInst_Link_t2063667470_0_0_0_Types[] = { &Link_t2063667470_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2063667470_0_0_0 = { 1, GenInst_Link_t2063667470_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1751606614_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0 = { 1, GenInst_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1873037576_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1873037576_0_0_0_Types[] = { &KeyValuePair_2_t1873037576_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1873037576_0_0_0 = { 1, GenInst_KeyValuePair_2_t1873037576_0_0_0_Types };
extern const Il2CppType Contraction_t3998770676_0_0_0;
static const Il2CppType* GenInst_Contraction_t3998770676_0_0_0_Types[] = { &Contraction_t3998770676_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t3998770676_0_0_0 = { 1, GenInst_Contraction_t3998770676_0_0_0_Types };
extern const Il2CppType Level2Map_t3664214860_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3664214860_0_0_0_Types[] = { &Level2Map_t3664214860_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3664214860_0_0_0 = { 1, GenInst_Level2Map_t3664214860_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373498_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373498_0_0_0_Types[] = { &BigInteger_t3334373498_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373498_0_0_0 = { 1, GenInst_BigInteger_t3334373498_0_0_0_Types };
extern const Il2CppType KeySizes_t2106826975_0_0_0;
static const Il2CppType* GenInst_KeySizes_t2106826975_0_0_0_Types[] = { &KeySizes_t2106826975_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t2106826975_0_0_0 = { 1, GenInst_KeySizes_t2106826975_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1944668977_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0 = { 1, GenInst_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
extern const Il2CppType IComparable_1_t386728509_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t386728509_0_0_0_Types[] = { &IComparable_1_t386728509_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t386728509_0_0_0 = { 1, GenInst_IComparable_1_t386728509_0_0_0_Types };
extern const Il2CppType IEquatable_1_t26077285_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t26077285_0_0_0_Types[] = { &IEquatable_1_t26077285_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t26077285_0_0_0 = { 1, GenInst_IEquatable_1_t26077285_0_0_0_Types };
extern const Il2CppType Slot_t2260530181_0_0_0;
static const Il2CppType* GenInst_Slot_t2260530181_0_0_0_Types[] = { &Slot_t2260530181_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2260530181_0_0_0 = { 1, GenInst_Slot_t2260530181_0_0_0_Types };
extern const Il2CppType Slot_t2072023290_0_0_0;
static const Il2CppType* GenInst_Slot_t2072023290_0_0_0_Types[] = { &Slot_t2072023290_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2072023290_0_0_0 = { 1, GenInst_Slot_t2072023290_0_0_0_Types };
extern const Il2CppType StackFrame_t1034942277_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1034942277_0_0_0_Types[] = { &StackFrame_t1034942277_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1034942277_0_0_0 = { 1, GenInst_StackFrame_t1034942277_0_0_0_Types };
extern const Il2CppType Calendar_t3558528576_0_0_0;
static const Il2CppType* GenInst_Calendar_t3558528576_0_0_0_Types[] = { &Calendar_t3558528576_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t3558528576_0_0_0 = { 1, GenInst_Calendar_t3558528576_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t595214213_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t595214213_0_0_0_Types[] = { &ModuleBuilder_t595214213_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t595214213_0_0_0 = { 1, GenInst_ModuleBuilder_t595214213_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1764509690_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1764509690_0_0_0_Types[] = { &_ModuleBuilder_t1764509690_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1764509690_0_0_0 = { 1, GenInst__ModuleBuilder_t1764509690_0_0_0_Types };
extern const Il2CppType Module_t1394482686_0_0_0;
static const Il2CppType* GenInst_Module_t1394482686_0_0_0_Types[] = { &Module_t1394482686_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1394482686_0_0_0 = { 1, GenInst_Module_t1394482686_0_0_0_Types };
extern const Il2CppType _Module_t2601912805_0_0_0;
static const Il2CppType* GenInst__Module_t2601912805_0_0_0_Types[] = { &_Module_t2601912805_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2601912805_0_0_0 = { 1, GenInst__Module_t2601912805_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3159962230_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3159962230_0_0_0_Types[] = { &ParameterBuilder_t3159962230_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3159962230_0_0_0 = { 1, GenInst_ParameterBuilder_t3159962230_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t4122453611_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t4122453611_0_0_0_Types[] = { &_ParameterBuilder_t4122453611_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t4122453611_0_0_0 = { 1, GenInst__ParameterBuilder_t4122453611_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3339007067_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3339007067_0_0_0_Types[] = { &TypeU5BU5D_t3339007067_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3339007067_0_0_0 = { 1, GenInst_TypeU5BU5D_t3339007067_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t2643922881_0_0_0;
static const Il2CppType* GenInst_ICollection_t2643922881_0_0_0_Types[] = { &ICollection_t2643922881_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t2643922881_0_0_0 = { 1, GenInst_ICollection_t2643922881_0_0_0_Types };
extern const Il2CppType IList_t1751339649_0_0_0;
static const Il2CppType* GenInst_IList_t1751339649_0_0_0_Types[] = { &IList_t1751339649_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1751339649_0_0_0 = { 1, GenInst_IList_t1751339649_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1354080954_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1354080954_0_0_0_Types[] = { &ILTokenInfo_t1354080954_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1354080954_0_0_0 = { 1, GenInst_ILTokenInfo_t1354080954_0_0_0_Types };
extern const Il2CppType LabelData_t3207823784_0_0_0;
static const Il2CppType* GenInst_LabelData_t3207823784_0_0_0_Types[] = { &LabelData_t3207823784_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3207823784_0_0_0 = { 1, GenInst_LabelData_t3207823784_0_0_0_Types };
extern const Il2CppType LabelFixup_t660379442_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t660379442_0_0_0_Types[] = { &LabelFixup_t660379442_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t660379442_0_0_0 = { 1, GenInst_LabelFixup_t660379442_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t553556921_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types[] = { &GenericTypeParameterBuilder_t553556921_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t553556921_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1918497079_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1918497079_0_0_0_Types[] = { &TypeBuilder_t1918497079_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1918497079_0_0_0 = { 1, GenInst_TypeBuilder_t1918497079_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3501492652_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3501492652_0_0_0_Types[] = { &_TypeBuilder_t3501492652_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3501492652_0_0_0 = { 1, GenInst__TypeBuilder_t3501492652_0_0_0_Types };
extern const Il2CppType MethodBuilder_t302405488_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t302405488_0_0_0_Types[] = { &MethodBuilder_t302405488_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t302405488_0_0_0 = { 1, GenInst_MethodBuilder_t302405488_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1471700965_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1471700965_0_0_0_Types[] = { &_MethodBuilder_t1471700965_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1471700965_0_0_0 = { 1, GenInst__MethodBuilder_t1471700965_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t3217839941_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t3217839941_0_0_0_Types[] = { &ConstructorBuilder_t3217839941_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t3217839941_0_0_0 = { 1, GenInst_ConstructorBuilder_t3217839941_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t788093754_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t788093754_0_0_0_Types[] = { &_ConstructorBuilder_t788093754_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t788093754_0_0_0 = { 1, GenInst__ConstructorBuilder_t788093754_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1754069893_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1754069893_0_0_0_Types[] = { &FieldBuilder_t1754069893_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1754069893_0_0_0 = { 1, GenInst_FieldBuilder_t1754069893_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t639782778_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t639782778_0_0_0_Types[] = { &_FieldBuilder_t639782778_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t639782778_0_0_0 = { 1, GenInst__FieldBuilder_t639782778_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3711326812_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3711326812_0_0_0_Types[] = { &_PropertyInfo_t3711326812_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3711326812_0_0_0 = { 1, GenInst__PropertyInfo_t3711326812_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t3301293422_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types[] = { &CustomAttributeTypedArgument_t3301293422_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t3059612989_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types[] = { &CustomAttributeNamedArgument_t3059612989_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t2955630591_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t2955630591_0_0_0_Types[] = { &CustomAttributeData_t2955630591_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2955630591_0_0_0 = { 1, GenInst_CustomAttributeData_t2955630591_0_0_0_Types };
extern const Il2CppType ResourceInfo_t4013605874_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t4013605874_0_0_0_Types[] = { &ResourceInfo_t4013605874_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t4013605874_0_0_0 = { 1, GenInst_ResourceInfo_t4013605874_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t2113902833_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t2113902833_0_0_0_Types[] = { &ResourceCacheItem_t2113902833_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t2113902833_0_0_0 = { 1, GenInst_ResourceCacheItem_t2113902833_0_0_0_Types };
extern const Il2CppType IContextProperty_t82913453_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t82913453_0_0_0_Types[] = { &IContextProperty_t82913453_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t82913453_0_0_0 = { 1, GenInst_IContextProperty_t82913453_0_0_0_Types };
extern const Il2CppType Header_t1689611527_0_0_0;
static const Il2CppType* GenInst_Header_t1689611527_0_0_0_Types[] = { &Header_t1689611527_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1689611527_0_0_0 = { 1, GenInst_Header_t1689611527_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2228500544_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2228500544_0_0_0_Types[] = { &ITrackingHandler_t2228500544_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2228500544_0_0_0 = { 1, GenInst_ITrackingHandler_t2228500544_0_0_0_Types };
extern const Il2CppType IContextAttribute_t3913746816_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t3913746816_0_0_0_Types[] = { &IContextAttribute_t3913746816_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t3913746816_0_0_0 = { 1, GenInst_IContextAttribute_t3913746816_0_0_0_Types };
extern const Il2CppType DateTime_t4283661327_0_0_0;
static const Il2CppType* GenInst_DateTime_t4283661327_0_0_0_Types[] = { &DateTime_t4283661327_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t4283661327_0_0_0 = { 1, GenInst_DateTime_t4283661327_0_0_0_Types };
extern const Il2CppType IComparable_1_t4193591118_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4193591118_0_0_0_Types[] = { &IComparable_1_t4193591118_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4193591118_0_0_0 = { 1, GenInst_IComparable_1_t4193591118_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3832939894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3832939894_0_0_0_Types[] = { &IEquatable_1_t3832939894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3832939894_0_0_0 = { 1, GenInst_IEquatable_1_t3832939894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1864280422_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1864280422_0_0_0_Types[] = { &IComparable_1_t1864280422_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1864280422_0_0_0 = { 1, GenInst_IComparable_1_t1864280422_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1503629198_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1503629198_0_0_0_Types[] = { &IEquatable_1_t1503629198_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1503629198_0_0_0 = { 1, GenInst_IEquatable_1_t1503629198_0_0_0_Types };
extern const Il2CppType TimeSpan_t413522987_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t413522987_0_0_0_Types[] = { &TimeSpan_t413522987_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t413522987_0_0_0 = { 1, GenInst_TimeSpan_t413522987_0_0_0_Types };
extern const Il2CppType IComparable_1_t323452778_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t323452778_0_0_0_Types[] = { &IComparable_1_t323452778_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t323452778_0_0_0 = { 1, GenInst_IComparable_1_t323452778_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4257768850_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4257768850_0_0_0_Types[] = { &IEquatable_1_t4257768850_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4257768850_0_0_0 = { 1, GenInst_IEquatable_1_t4257768850_0_0_0_Types };
extern const Il2CppType TypeTag_t2420703430_0_0_0;
static const Il2CppType* GenInst_TypeTag_t2420703430_0_0_0_Types[] = { &TypeTag_t2420703430_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t2420703430_0_0_0 = { 1, GenInst_TypeTag_t2420703430_0_0_0_Types };
extern const Il2CppType Enum_t2862688501_0_0_0;
static const Il2CppType* GenInst_Enum_t2862688501_0_0_0_Types[] = { &Enum_t2862688501_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2862688501_0_0_0 = { 1, GenInst_Enum_t2862688501_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2878058698_0_0_0;
static const Il2CppType* GenInst_StrongName_t2878058698_0_0_0_Types[] = { &StrongName_t2878058698_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2878058698_0_0_0 = { 1, GenInst_StrongName_t2878058698_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3884714306_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3884714306_0_0_0_Types[] = { &DateTimeOffset_t3884714306_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3884714306_0_0_0 = { 1, GenInst_DateTimeOffset_t3884714306_0_0_0_Types };
extern const Il2CppType Guid_t2862754429_0_0_0;
static const Il2CppType* GenInst_Guid_t2862754429_0_0_0_Types[] = { &Guid_t2862754429_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2862754429_0_0_0 = { 1, GenInst_Guid_t2862754429_0_0_0_Types };
extern const Il2CppType Version_t763695022_0_0_0;
static const Il2CppType* GenInst_Version_t763695022_0_0_0_Types[] = { &Version_t763695022_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t763695022_0_0_0 = { 1, GenInst_Version_t763695022_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2545618620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0 = { 1, GenInst_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1195997794_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1195997794_0_0_0_Types[] = { &KeyValuePair_2_t1195997794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1195997794_0_0_0 = { 1, GenInst_KeyValuePair_2_t1195997794_0_0_0_Types };
extern const Il2CppType X509Certificate_t3076817455_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t3076817455_0_0_0_Types[] = { &X509Certificate_t3076817455_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t3076817455_0_0_0 = { 1, GenInst_X509Certificate_t3076817455_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t675596727_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t675596727_0_0_0_Types[] = { &IDeserializationCallback_t675596727_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t675596727_0_0_0 = { 1, GenInst_IDeserializationCallback_t675596727_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t766901931_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t766901931_0_0_0_Types[] = { &X509ChainStatus_t766901931_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t766901931_0_0_0 = { 1, GenInst_X509ChainStatus_t766901931_0_0_0_Types };
extern const Il2CppType Capture_t754001812_0_0_0;
static const Il2CppType* GenInst_Capture_t754001812_0_0_0_Types[] = { &Capture_t754001812_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t754001812_0_0_0 = { 1, GenInst_Capture_t754001812_0_0_0_Types };
extern const Il2CppType Group_t2151468941_0_0_0;
static const Il2CppType* GenInst_Group_t2151468941_0_0_0_Types[] = { &Group_t2151468941_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t2151468941_0_0_0 = { 1, GenInst_Group_t2151468941_0_0_0_Types };
extern const Il2CppType Mark_t3811539797_0_0_0;
static const Il2CppType* GenInst_Mark_t3811539797_0_0_0_Types[] = { &Mark_t3811539797_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3811539797_0_0_0 = { 1, GenInst_Mark_t3811539797_0_0_0_Types };
extern const Il2CppType UriScheme_t1290668975_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1290668975_0_0_0_Types[] = { &UriScheme_t1290668975_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1290668975_0_0_0 = { 1, GenInst_UriScheme_t1290668975_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373499_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373499_0_0_0_Types[] = { &BigInteger_t3334373499_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373499_0_0_0 = { 1, GenInst_BigInteger_t3334373499_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t4260760469_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4260760469_0_0_0_Types[] = { &ByteU5BU5D_t4260760469_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4260760469_0_0_0 = { 1, GenInst_ByteU5BU5D_t4260760469_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t3167042548_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t3167042548_0_0_0_Types[] = { &ClientCertificateType_t3167042548_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t3167042548_0_0_0 = { 1, GenInst_ClientCertificateType_t3167042548_0_0_0_Types };
extern const Il2CppType Link_t2122599155_0_0_0;
static const Il2CppType* GenInst_Link_t2122599155_0_0_0_Types[] = { &Link_t2122599155_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2122599155_0_0_0 = { 1, GenInst_Link_t2122599155_0_0_0_Types };
extern const Il2CppType Object_t3071478659_0_0_0;
static const Il2CppType* GenInst_Object_t3071478659_0_0_0_Types[] = { &Object_t3071478659_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t3071478659_0_0_0 = { 1, GenInst_Object_t3071478659_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4128901257_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4128901257_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t655461400_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t655461400_0_0_0_Types[] = { &IAchievementDescription_t655461400_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t655461400_0_0_0 = { 1, GenInst_IAchievementDescription_t655461400_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1953253797_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types[] = { &IAchievementU5BU5D_t1953253797_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1953253797_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types };
extern const Il2CppType IAchievement_t2957812780_0_0_0;
static const Il2CppType* GenInst_IAchievement_t2957812780_0_0_0_Types[] = { &IAchievement_t2957812780_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t2957812780_0_0_0 = { 1, GenInst_IAchievement_t2957812780_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t250104726_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t250104726_0_0_0_Types[] = { &IScoreU5BU5D_t250104726_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t250104726_0_0_0 = { 1, GenInst_IScoreU5BU5D_t250104726_0_0_0_Types };
extern const Il2CppType IScore_t4279057999_0_0_0;
static const Il2CppType* GenInst_IScore_t4279057999_0_0_0_Types[] = { &IScore_t4279057999_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t4279057999_0_0_0 = { 1, GenInst_IScore_t4279057999_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3419104218_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types[] = { &IUserProfileU5BU5D_t3419104218_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3419104218_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types };
extern const Il2CppType IUserProfile_t598900827_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t598900827_0_0_0_Types[] = { &IUserProfile_t598900827_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t598900827_0_0_0 = { 1, GenInst_IUserProfile_t598900827_0_0_0_Types };
extern const Il2CppType AchievementDescription_t2116066607_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t2116066607_0_0_0_Types[] = { &AchievementDescription_t2116066607_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t2116066607_0_0_0 = { 1, GenInst_AchievementDescription_t2116066607_0_0_0_Types };
extern const Il2CppType UserProfile_t2280656072_0_0_0;
static const Il2CppType* GenInst_UserProfile_t2280656072_0_0_0_Types[] = { &UserProfile_t2280656072_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t2280656072_0_0_0 = { 1, GenInst_UserProfile_t2280656072_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t1820874799_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t1820874799_0_0_0_Types[] = { &GcLeaderboard_t1820874799_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1820874799_0_0_0 = { 1, GenInst_GcLeaderboard_t1820874799_0_0_0_Types };
extern const Il2CppType GcAchievementData_t3481375915_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t3481375915_0_0_0_Types[] = { &GcAchievementData_t3481375915_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3481375915_0_0_0 = { 1, GenInst_GcAchievementData_t3481375915_0_0_0_Types };
extern const Il2CppType Achievement_t344600729_0_0_0;
static const Il2CppType* GenInst_Achievement_t344600729_0_0_0_Types[] = { &Achievement_t344600729_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t344600729_0_0_0 = { 1, GenInst_Achievement_t344600729_0_0_0_Types };
extern const Il2CppType GcScoreData_t2181296590_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2181296590_0_0_0_Types[] = { &GcScoreData_t2181296590_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2181296590_0_0_0 = { 1, GenInst_GcScoreData_t2181296590_0_0_0_Types };
extern const Il2CppType Score_t3396031228_0_0_0;
static const Il2CppType* GenInst_Score_t3396031228_0_0_0_Types[] = { &Score_t3396031228_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t3396031228_0_0_0 = { 1, GenInst_Score_t3396031228_0_0_0_Types };
extern const Il2CppType Vector3_t4282066566_0_0_0;
static const Il2CppType* GenInst_Vector3_t4282066566_0_0_0_Types[] = { &Vector3_t4282066566_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t4282066566_0_0_0 = { 1, GenInst_Vector3_t4282066566_0_0_0_Types };
extern const Il2CppType Vector2_t4282066565_0_0_0;
static const Il2CppType* GenInst_Vector2_t4282066565_0_0_0_Types[] = { &Vector2_t4282066565_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t4282066565_0_0_0 = { 1, GenInst_Vector2_t4282066565_0_0_0_Types };
extern const Il2CppType Material_t3870600107_0_0_0;
static const Il2CppType* GenInst_Material_t3870600107_0_0_0_Types[] = { &Material_t3870600107_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t3870600107_0_0_0 = { 1, GenInst_Material_t3870600107_0_0_0_Types };
extern const Il2CppType Color_t4194546905_0_0_0;
static const Il2CppType* GenInst_Color_t4194546905_0_0_0_Types[] = { &Color_t4194546905_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t4194546905_0_0_0 = { 1, GenInst_Color_t4194546905_0_0_0_Types };
extern const Il2CppType Color32_t598853688_0_0_0;
static const Il2CppType* GenInst_Color32_t598853688_0_0_0_Types[] = { &Color32_t598853688_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t598853688_0_0_0 = { 1, GenInst_Color32_t598853688_0_0_0_Types };
extern const Il2CppType Keyframe_t4079056114_0_0_0;
static const Il2CppType* GenInst_Keyframe_t4079056114_0_0_0_Types[] = { &Keyframe_t4079056114_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t4079056114_0_0_0 = { 1, GenInst_Keyframe_t4079056114_0_0_0_Types };
extern const Il2CppType Camera_t2727095145_0_0_0;
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_Types[] = { &Camera_t2727095145_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0 = { 1, GenInst_Camera_t2727095145_0_0_0_Types };
extern const Il2CppType Behaviour_t200106419_0_0_0;
static const Il2CppType* GenInst_Behaviour_t200106419_0_0_0_Types[] = { &Behaviour_t200106419_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t200106419_0_0_0 = { 1, GenInst_Behaviour_t200106419_0_0_0_Types };
extern const Il2CppType Component_t3501516275_0_0_0;
static const Il2CppType* GenInst_Component_t3501516275_0_0_0_Types[] = { &Component_t3501516275_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3501516275_0_0_0 = { 1, GenInst_Component_t3501516275_0_0_0_Types };
extern const Il2CppType Display_t1321072632_0_0_0;
static const Il2CppType* GenInst_Display_t1321072632_0_0_0_Types[] = { &Display_t1321072632_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1321072632_0_0_0 = { 1, GenInst_Display_t1321072632_0_0_0_Types };
extern const Il2CppType Playable_t70832698_0_0_0;
static const Il2CppType* GenInst_Playable_t70832698_0_0_0_Types[] = { &Playable_t70832698_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t70832698_0_0_0 = { 1, GenInst_Playable_t70832698_0_0_0_Types };
extern const Il2CppType IDisposable_t1423340799_0_0_0;
static const Il2CppType* GenInst_IDisposable_t1423340799_0_0_0_Types[] = { &IDisposable_t1423340799_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t1423340799_0_0_0 = { 1, GenInst_IDisposable_t1423340799_0_0_0_Types };
extern const Il2CppType ContactPoint_t243083348_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t243083348_0_0_0_Types[] = { &ContactPoint_t243083348_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t243083348_0_0_0 = { 1, GenInst_ContactPoint_t243083348_0_0_0_Types };
extern const Il2CppType WebCamDevice_t3274004757_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t3274004757_0_0_0_Types[] = { &WebCamDevice_t3274004757_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t3274004757_0_0_0 = { 1, GenInst_WebCamDevice_t3274004757_0_0_0_Types };
extern const Il2CppType Font_t4241557075_0_0_0;
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_Types[] = { &Font_t4241557075_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0 = { 1, GenInst_Font_t4241557075_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t331591504_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t331591504_0_0_0_Types[] = { &GUILayoutOption_t331591504_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t331591504_0_0_0 = { 1, GenInst_GUILayoutOption_t331591504_0_0_0_Types };
extern const Il2CppType LayoutCache_t879908455_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4066860316_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0 = { 1, GenInst_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t775952400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t775952400_0_0_0_Types[] = { &KeyValuePair_2_t775952400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t775952400_0_0_0 = { 1, GenInst_KeyValuePair_2_t775952400_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t1336615025_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t1336615025_0_0_0_Types[] = { &GUILayoutEntry_t1336615025_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1336615025_0_0_0 = { 1, GenInst_GUILayoutEntry_t1336615025_0_0_0_Types };
extern const Il2CppType GUIStyle_t2990928826_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t2990928826_0_0_0_Types[] = { &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t2990928826_0_0_0 = { 1, GenInst_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3710127902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3710127902_0_0_0_Types[] = { &KeyValuePair_2_t3710127902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3710127902_0_0_0 = { 1, GenInst_KeyValuePair_2_t3710127902_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t62111112_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types[] = { &DisallowMultipleComponent_t62111112_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t62111112_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types };
extern const Il2CppType Attribute_t2523058482_0_0_0;
static const Il2CppType* GenInst_Attribute_t2523058482_0_0_0_Types[] = { &Attribute_t2523058482_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t2523058482_0_0_0 = { 1, GenInst_Attribute_t2523058482_0_0_0_Types };
extern const Il2CppType _Attribute_t3253047175_0_0_0;
static const Il2CppType* GenInst__Attribute_t3253047175_0_0_0_Types[] = { &_Attribute_t3253047175_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t3253047175_0_0_0 = { 1, GenInst__Attribute_t3253047175_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3132250205_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types[] = { &ExecuteInEditMode_t3132250205_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3132250205_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types };
extern const Il2CppType RequireComponent_t1687166108_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t1687166108_0_0_0_Types[] = { &RequireComponent_t1687166108_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t1687166108_0_0_0 = { 1, GenInst_RequireComponent_t1687166108_0_0_0_Types };
extern const Il2CppType HitInfo_t3209134097_0_0_0;
static const Il2CppType* GenInst_HitInfo_t3209134097_0_0_0_Types[] = { &HitInfo_t3209134097_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t3209134097_0_0_0 = { 1, GenInst_HitInfo_t3209134097_0_0_0_Types };
extern const Il2CppType PersistentCall_t2972625667_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t2972625667_0_0_0_Types[] = { &PersistentCall_t2972625667_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t2972625667_0_0_0 = { 1, GenInst_PersistentCall_t2972625667_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1559630662_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1559630662_0_0_0_Types[] = { &BaseInvokableCall_t1559630662_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1559630662_0_0_0 = { 1, GenInst_BaseInvokableCall_t1559630662_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType GameObject_t3674682005_0_0_0;
static const Il2CppType* GenInst_GameObject_t3674682005_0_0_0_Types[] = { &GameObject_t3674682005_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t3674682005_0_0_0 = { 1, GenInst_GameObject_t3674682005_0_0_0_Types };
extern const Il2CppType RenderTexture_t1963041563_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t1963041563_0_0_0_Types[] = { &RenderTexture_t1963041563_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t1963041563_0_0_0 = { 1, GenInst_RenderTexture_t1963041563_0_0_0_Types };
extern const Il2CppType Texture_t2526458961_0_0_0;
static const Il2CppType* GenInst_Texture_t2526458961_0_0_0_Types[] = { &Texture_t2526458961_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2526458961_0_0_0 = { 1, GenInst_Texture_t2526458961_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t2729214813_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t2729214813_0_0_0_Types[] = { &EyewearCalibrationReading_t2729214813_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t2729214813_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t2729214813_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t2475465524_0_0_0;
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types[] = { &Camera_t2727095145_0_0_0, &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 = { 2, GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Camera_t2727095145_0_0_0, &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1857232484_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1857232484_0_0_0_Types[] = { &KeyValuePair_2_t1857232484_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1857232484_0_0_0 = { 1, GenInst_KeyValuePair_2_t1857232484_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types };
extern const Il2CppType Renderer_t3076687687_0_0_0;
static const Il2CppType* GenInst_Renderer_t3076687687_0_0_0_Types[] = { &Renderer_t3076687687_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t3076687687_0_0_0 = { 1, GenInst_Renderer_t3076687687_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t4179556250_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t4179556250_0_0_0_Types[] = { &TrackableBehaviour_t4179556250_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t4179556250_0_0_0 = { 1, GenInst_TrackableBehaviour_t4179556250_0_0_0_Types };
extern const Il2CppType VuMarkTarget_t3123531359_0_0_0;
static const Il2CppType* GenInst_VuMarkTarget_t3123531359_0_0_0_Types[] = { &VuMarkTarget_t3123531359_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTarget_t3123531359_0_0_0 = { 1, GenInst_VuMarkTarget_t3123531359_0_0_0_Types };
extern const Il2CppType VuMarkAbstractBehaviour_t2231576857_0_0_0;
static const Il2CppType* GenInst_VuMarkAbstractBehaviour_t2231576857_0_0_0_Types[] = { &VuMarkAbstractBehaviour_t2231576857_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkAbstractBehaviour_t2231576857_0_0_0 = { 1, GenInst_VuMarkAbstractBehaviour_t2231576857_0_0_0_Types };
extern const Il2CppType List_1_t3599762409_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t3599762409_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t3599762409_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3495806354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3495806354_0_0_0_Types[] = { &KeyValuePair_2_t3495806354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3495806354_0_0_0 = { 1, GenInst_KeyValuePair_2_t3495806354_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3599762409_0_0_0_Types[] = { &List_1_t3599762409_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3599762409_0_0_0 = { 1, GenInst_List_1_t3599762409_0_0_0_Types };
extern const Il2CppType VuMarkTargetData_t3459596319_0_0_0;
static const Il2CppType* GenInst_VuMarkTargetData_t3459596319_0_0_0_Types[] = { &VuMarkTargetData_t3459596319_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetData_t3459596319_0_0_0 = { 1, GenInst_VuMarkTargetData_t3459596319_0_0_0_Types };
extern const Il2CppType VuMarkTargetResultData_t2938518044_0_0_0;
static const Il2CppType* GenInst_VuMarkTargetResultData_t2938518044_0_0_0_Types[] = { &VuMarkTargetResultData_t2938518044_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkTargetResultData_t2938518044_0_0_0 = { 1, GenInst_VuMarkTargetResultData_t2938518044_0_0_0_Types };
extern const Il2CppType Link_t3400588580_0_0_0;
static const Il2CppType* GenInst_Link_t3400588580_0_0_0_Types[] = { &Link_t3400588580_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3400588580_0_0_0 = { 1, GenInst_Link_t3400588580_0_0_0_Types };
extern const Il2CppType List_1_t2522024052_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t2522024052_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t2522024052_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2418067997_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2418067997_0_0_0_Types[] = { &KeyValuePair_2_t2418067997_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2418067997_0_0_0 = { 1, GenInst_KeyValuePair_2_t2418067997_0_0_0_Types };
extern const Il2CppType TrackableResultData_t395876724_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t395876724_0_0_0_Types[] = { &TrackableResultData_t395876724_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t395876724_0_0_0 = { 1, GenInst_TrackableResultData_t395876724_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t465046465_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t465046465_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t667441552_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t667441552_0_0_0_Types[] = { &MonoBehaviour_t667441552_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t667441552_0_0_0 = { 1, GenInst_MonoBehaviour_t667441552_0_0_0_Types };
extern const Il2CppType IViewerParameters_t2145870415_0_0_0;
static const Il2CppType* GenInst_IViewerParameters_t2145870415_0_0_0_Types[] = { &IViewerParameters_t2145870415_0_0_0 };
extern const Il2CppGenericInst GenInst_IViewerParameters_t2145870415_0_0_0 = { 1, GenInst_IViewerParameters_t2145870415_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t1613618350_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t1613618350_0_0_0_Types[] = { &ITrackableEventHandler_t1613618350_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t1613618350_0_0_0 = { 1, GenInst_ITrackableEventHandler_t1613618350_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t1860057025_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t1860057025_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0_Types };
extern const Il2CppType CameraField_t2874564333_0_0_0;
static const Il2CppType* GenInst_CameraField_t2874564333_0_0_0_Types[] = { &CameraField_t2874564333_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraField_t2874564333_0_0_0 = { 1, GenInst_CameraField_t2874564333_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t796991165_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t796991165_0_0_0_Types[] = { &ICloudRecoEventHandler_t796991165_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t796991165_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t796991165_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t3609410114_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t3609410114_0_0_0_Types[] = { &TargetSearchResult_t3609410114_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3609410114_0_0_0 = { 1, GenInst_TargetSearchResult_t3609410114_0_0_0_Types };
extern const Il2CppType Trackable_t3781061455_0_0_0;
static const Il2CppType* GenInst_Trackable_t3781061455_0_0_0_Types[] = { &Trackable_t3781061455_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t3781061455_0_0_0 = { 1, GenInst_Trackable_t3781061455_0_0_0_Types };
extern const Il2CppType VirtualButton_t704206407_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t704206407_0_0_0_Types[] = { &VirtualButton_t704206407_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t704206407_0_0_0 = { 1, GenInst_VirtualButton_t704206407_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t354375056_0_0_0;
extern const Il2CppType Image_t2247677317_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Image_t2247677317_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1718082560_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0 = { 1, GenInst_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Image_t2247677317_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4089910802_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4089910802_0_0_0_Types[] = { &KeyValuePair_2_t4089910802_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4089910802_0_0_0 = { 1, GenInst_KeyValuePair_2_t4089910802_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Trackable_t3781061455_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Trackable_t3781061455_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3677105400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3677105400_0_0_0_Types[] = { &KeyValuePair_2_t3677105400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3677105400_0_0_0 = { 1, GenInst_KeyValuePair_2_t3677105400_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButton_t704206407_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButton_t704206407_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t600250352_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t600250352_0_0_0_Types[] = { &KeyValuePair_2_t600250352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t600250352_0_0_0 = { 1, GenInst_KeyValuePair_2_t600250352_0_0_0_Types };
extern const Il2CppType DataSet_t2095838082_0_0_0;
static const Il2CppType* GenInst_DataSet_t2095838082_0_0_0_Types[] = { &DataSet_t2095838082_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t2095838082_0_0_0 = { 1, GenInst_DataSet_t2095838082_0_0_0_Types };
extern const Il2CppType DataSetImpl_t1837478850_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t1837478850_0_0_0_Types[] = { &DataSetImpl_t1837478850_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t1837478850_0_0_0 = { 1, GenInst_DataSetImpl_t1837478850_0_0_0_Types };
extern const Il2CppType Marker_t616694972_0_0_0;
static const Il2CppType* GenInst_Marker_t616694972_0_0_0_Types[] = { &Marker_t616694972_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t616694972_0_0_0 = { 1, GenInst_Marker_t616694972_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Marker_t616694972_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Marker_t616694972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t512738917_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t512738917_0_0_0_Types[] = { &KeyValuePair_2_t512738917_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t512738917_0_0_0 = { 1, GenInst_KeyValuePair_2_t512738917_0_0_0_Types };
extern const Il2CppType WordData_t1548845516_0_0_0;
static const Il2CppType* GenInst_WordData_t1548845516_0_0_0_Types[] = { &WordData_t1548845516_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t1548845516_0_0_0 = { 1, GenInst_WordData_t1548845516_0_0_0_Types };
extern const Il2CppType WordResultData_t1826866697_0_0_0;
static const Il2CppType* GenInst_WordResultData_t1826866697_0_0_0_Types[] = { &WordResultData_t1826866697_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t1826866697_0_0_0 = { 1, GenInst_WordResultData_t1826866697_0_0_0_Types };
extern const Il2CppType IdPair_t3522586765_0_0_0;
static const Il2CppType* GenInst_IdPair_t3522586765_0_0_0_Types[] = { &IdPair_t3522586765_0_0_0 };
extern const Il2CppGenericInst GenInst_IdPair_t3522586765_0_0_0 = { 1, GenInst_IdPair_t3522586765_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t3919795561_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t3919795561_0_0_0_Types[] = { &SmartTerrainRevisionData_t3919795561_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t3919795561_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t3919795561_0_0_0_Types };
extern const Il2CppType SurfaceData_t1737958143_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t1737958143_0_0_0_Types[] = { &SurfaceData_t1737958143_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t1737958143_0_0_0 = { 1, GenInst_SurfaceData_t1737958143_0_0_0_Types };
extern const Il2CppType PropData_t526813605_0_0_0;
static const Il2CppType* GenInst_PropData_t526813605_0_0_0_Types[] = { &PropData_t526813605_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t526813605_0_0_0 = { 1, GenInst_PropData_t526813605_0_0_0_Types };
static const Il2CppType* GenInst_Image_t2247677317_0_0_0_Types[] = { &Image_t2247677317_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2247677317_0_0_0 = { 1, GenInst_Image_t2247677317_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t1276145027_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t1276145027_0_0_0_Types[] = { &SmartTerrainTrackable_t1276145027_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t1276145027_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t1276145027_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t2826579942_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t2826579942_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2093487825_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0 = { 1, GenInst_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t24667923_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t28871114_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t28871114_0_0_0_Types[] = { &KeyValuePair_2_t28871114_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t28871114_0_0_0 = { 1, GenInst_KeyValuePair_2_t28871114_0_0_0_Types };
extern const Il2CppType RectangleData_t2265684451_0_0_0;
static const Il2CppType* GenInst_RectangleData_t2265684451_0_0_0_Types[] = { &RectangleData_t2265684451_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t2265684451_0_0_0 = { 1, GenInst_RectangleData_t2265684451_0_0_0_Types };
extern const Il2CppType WordResult_t1079862857_0_0_0;
static const Il2CppType* GenInst_WordResult_t1079862857_0_0_0_Types[] = { &WordResult_t1079862857_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t1079862857_0_0_0 = { 1, GenInst_WordResult_t1079862857_0_0_0_Types };
extern const Il2CppType Word_t2165514892_0_0_0;
static const Il2CppType* GenInst_Word_t2165514892_0_0_0_Types[] = { &Word_t2165514892_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t2165514892_0_0_0 = { 1, GenInst_Word_t2165514892_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t545947899_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t545947899_0_0_0_Types[] = { &WordAbstractBehaviour_t545947899_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t545947899_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t545947899_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordResult_t1079862857_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordResult_t1079862857_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t975906802_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t975906802_0_0_0_Types[] = { &KeyValuePair_2_t975906802_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t975906802_0_0_0 = { 1, GenInst_KeyValuePair_2_t975906802_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordAbstractBehaviour_t545947899_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordAbstractBehaviour_t545947899_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t441991844_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t441991844_0_0_0_Types[] = { &KeyValuePair_2_t441991844_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t441991844_0_0_0 = { 1, GenInst_KeyValuePair_2_t441991844_0_0_0_Types };
extern const Il2CppType List_1_t1914133451_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1914133451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1914133451_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2633332527_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2633332527_0_0_0_Types[] = { &KeyValuePair_2_t2633332527_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2633332527_0_0_0 = { 1, GenInst_KeyValuePair_2_t2633332527_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1914133451_0_0_0_Types[] = { &List_1_t1914133451_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1914133451_0_0_0 = { 1, GenInst_List_1_t1914133451_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t678501447_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t678501447_0_0_0_Types[] = { &ILoadLevelEventHandler_t678501447_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t678501447_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t678501447_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t2222092879_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0_Types[] = { &ISmartTerrainEventHandler_t2222092879_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t587327548_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t587327548_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0_Types };
extern const Il2CppType Prop_t2165309157_0_0_0;
static const Il2CppType* GenInst_Prop_t2165309157_0_0_0_Types[] = { &Prop_t2165309157_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t2165309157_0_0_0 = { 1, GenInst_Prop_t2165309157_0_0_0_Types };
extern const Il2CppType Surface_t3094389719_0_0_0;
static const Il2CppType* GenInst_Surface_t3094389719_0_0_0_Types[] = { &Surface_t3094389719_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t3094389719_0_0_0 = { 1, GenInst_Surface_t3094389719_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Surface_t3094389719_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Surface_t3094389719_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2990433664_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2990433664_0_0_0_Types[] = { &KeyValuePair_2_t2990433664_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2990433664_0_0_0 = { 1, GenInst_KeyValuePair_2_t2990433664_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t142368528_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &SurfaceAbstractBehaviour_t142368528_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &SurfaceAbstractBehaviour_t142368528_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38412473_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38412473_0_0_0_Types[] = { &KeyValuePair_2_t38412473_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38412473_0_0_0 = { 1, GenInst_KeyValuePair_2_t38412473_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Prop_t2165309157_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Prop_t2165309157_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2061353102_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2061353102_0_0_0_Types[] = { &KeyValuePair_2_t2061353102_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2061353102_0_0_0 = { 1, GenInst_KeyValuePair_2_t2061353102_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t1293468098_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PropAbstractBehaviour_t1293468098_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PropAbstractBehaviour_t1293468098_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1189512043_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1189512043_0_0_0_Types[] = { &KeyValuePair_2_t1189512043_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1189512043_0_0_0 = { 1, GenInst_KeyValuePair_2_t1189512043_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t1293468098_0_0_0_Types[] = { &PropAbstractBehaviour_t1293468098_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t1293468098_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t1293468098_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t142368528_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0_Types };
extern const Il2CppType MeshFilter_t3839065225_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3839065225_0_0_0_Types[] = { &MeshFilter_t3839065225_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3839065225_0_0_0 = { 1, GenInst_MeshFilter_t3839065225_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableBehaviour_t4179556250_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableBehaviour_t4179556250_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4075600195_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4075600195_0_0_0_Types[] = { &KeyValuePair_2_t4075600195_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4075600195_0_0_0 = { 1, GenInst_KeyValuePair_2_t4075600195_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t865486027_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t865486027_0_0_0_Types[] = { &MarkerAbstractBehaviour_t865486027_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t865486027_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t865486027_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t3541475369_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t3541475369_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t3340678586_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0_Types[] = { &DataSetTrackableBehaviour_t3340678586_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0_Types };
extern const Il2CppType IEditDataSetBehaviour_t1071163298_0_0_0;
static const Il2CppType* GenInst_IEditDataSetBehaviour_t1071163298_0_0_0_Types[] = { &IEditDataSetBehaviour_t1071163298_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditDataSetBehaviour_t1071163298_0_0_0 = { 1, GenInst_IEditDataSetBehaviour_t1071163298_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t1191238304_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t1191238304_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types };
extern const Il2CppType Status_t835151357_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Status_t835151357_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t731195302_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t731195302_0_0_0_Types[] = { &KeyValuePair_2_t731195302_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t731195302_0_0_0 = { 1, GenInst_KeyValuePair_2_t731195302_0_0_0_Types };
static const Il2CppType* GenInst_Status_t835151357_0_0_0_Types[] = { &Status_t835151357_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t835151357_0_0_0 = { 1, GenInst_Status_t835151357_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Status_t835151357_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Status_t835151357_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Status_t835151357_0_0_0, &Status_t835151357_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Status_t835151357_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Status_t835151357_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Status_t835151357_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_KeyValuePair_2_t731195302_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Status_t835151357_0_0_0, &KeyValuePair_2_t731195302_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_KeyValuePair_2_t731195302_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_KeyValuePair_2_t731195302_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t459380335_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t355424280_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0 = { 1, GenInst_KeyValuePair_2_t355424280_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0 = { 1, GenInst_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types };
extern const Il2CppType ImageTarget_t3520455670_0_0_0;
static const Il2CppType* GenInst_ImageTarget_t3520455670_0_0_0_Types[] = { &ImageTarget_t3520455670_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t3520455670_0_0_0 = { 1, GenInst_ImageTarget_t3520455670_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &ImageTarget_t3520455670_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &ImageTarget_t3520455670_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3416499615_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3416499615_0_0_0_Types[] = { &KeyValuePair_2_t3416499615_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3416499615_0_0_0 = { 1, GenInst_KeyValuePair_2_t3416499615_0_0_0_Types };
extern const Il2CppType ProfileData_t1961690790_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4030510692_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0 = { 1, GenInst_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0 = { 1, GenInst_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1961690790_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2680889866_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2680889866_0_0_0_Types[] = { &KeyValuePair_2_t2680889866_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2680889866_0_0_0 = { 1, GenInst_KeyValuePair_2_t2680889866_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonAbstractBehaviour_t1191238304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonAbstractBehaviour_t1191238304_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1087282249_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1087282249_0_0_0_Types[] = { &KeyValuePair_2_t1087282249_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1087282249_0_0_0 = { 1, GenInst_KeyValuePair_2_t1087282249_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t3566668545_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t3566668545_0_0_0_Types[] = { &ITrackerEventHandler_t3566668545_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t3566668545_0_0_0 = { 1, GenInst_ITrackerEventHandler_t3566668545_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t4010260306_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t4010260306_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0_Types };
extern const Il2CppType InitError_t432217960_0_0_0;
static const Il2CppType* GenInst_InitError_t432217960_0_0_0_Types[] = { &InitError_t432217960_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t432217960_0_0_0 = { 1, GenInst_InitError_t432217960_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t155716687_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t155716687_0_0_0_Types[] = { &ITextRecoEventHandler_t155716687_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t155716687_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t155716687_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t2660219000_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t2660219000_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0_Types };
extern const Il2CppType MeshRenderer_t2804666580_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t2804666580_0_0_0_Types[] = { &MeshRenderer_t2804666580_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t2804666580_0_0_0 = { 1, GenInst_MeshRenderer_t2804666580_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t2952053798_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0_Types[] = { &IVirtualButtonEventHandler_t2952053798_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0_Types };
extern const Il2CppType Collider_t2939674232_0_0_0;
static const Il2CppType* GenInst_Collider_t2939674232_0_0_0_Types[] = { &Collider_t2939674232_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t2939674232_0_0_0 = { 1, GenInst_Collider_t2939674232_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t433318935_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t433318935_0_0_0_Types[] = { &WireframeBehaviour_t433318935_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t433318935_0_0_0 = { 1, GenInst_WireframeBehaviour_t433318935_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t309087608_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types[] = { &IEnumerable_1_t309087608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t309087608_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1181603442_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types[] = { &Array_Sort_m1181603442_gp_0_0_0_0, &Array_Sort_m1181603442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3908760906_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3908760906_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types[] = { &Array_Sort_m3908760906_gp_0_0_0_0, &Array_Sort_m3908760906_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m646233104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0, &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0, &Array_Sort_m2404937677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m377069906_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types[] = { &Array_Sort_m377069906_gp_0_0_0_0, &Array_Sort_m377069906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1327718954_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1327718954_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types[] = { &Array_Sort_m1327718954_gp_0_0_0_0, &Array_Sort_m1327718954_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m4084526832_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0, &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0, &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3566161319_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types[] = { &Array_Sort_m3566161319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3566161319_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1767877396_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types[] = { &Array_Sort_m1767877396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1767877396_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0, &Array_qsort_m785378185_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m3718693973_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types[] = { &Array_compare_m3718693973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m3718693973_gp_0_0_0_0 = { 1, GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m3270161954_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types[] = { &Array_qsort_m3270161954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3270161954_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m2347367271_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types[] = { &Array_Resize_m2347367271_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m2347367271_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m123384663_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m123384663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1326446122_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types[] = { &Array_ForEach_m1326446122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1326446122_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1697145810_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1697145810_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1697145810_gp_0_0_0_0, &Array_ConvertAll_m1697145810_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m614217902_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m614217902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m653822311_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m653822311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1650781518_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1650781518_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m308210168_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types[] = { &Array_FindIndex_m308210168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m308210168_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3338256477_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3338256477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1605056024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1605056024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3507857443_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3507857443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1578426497_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1578426497_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3933814211_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3933814211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1779658273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1779658273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3823835921_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3823835921_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1695958374_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1695958374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3280162097_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3280162097_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m921616327_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m921616327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m4157241456_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m4157241456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m925096551_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m925096551_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1181152586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types[] = { &Array_FindAll_m1181152586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1181152586_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m2312185345_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types[] = { &Array_Exists_m2312185345_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m2312185345_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m4233614634_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m4233614634_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m3878993575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types[] = { &Array_Find_m3878993575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m3878993575_gp_0_0_0_0 = { 1, GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3671273393_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types[] = { &Array_FindLast_m3671273393_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3671273393_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t813279015_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t813279015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2706020430_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2706020430_gp_0_0_0_0_Types[] = { &IList_1_t2706020430_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2706020430_gp_0_0_0_0 = { 1, GenInst_IList_1_t2706020430_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1952910030_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types[] = { &ICollection_1_t1952910030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1952910030_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1122404262_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types[] = { &Nullable_1_t1122404262_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1122404262_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1701613202_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types[] = { &Comparer_1_t1701613202_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1701613202_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3219634540_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types[] = { &DefaultComparer_t3219634540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3219634540_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2982791755_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2982791755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4022413346_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0 = { 1, GenInst_KeyValuePair_2_t4022413346_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3290534805_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3290534805_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3290534805_gp_0_0_0_0, &ShimEnumerator_t3290534805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1176480892_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1176480892_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types[] = { &Enumerator_t1176480892_gp_0_0_0_0, &Enumerator_t1176480892_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912719906_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912719906_0_0_0_Types[] = { &KeyValuePair_2_t1912719906_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912719906_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912719906_0_0_0_Types };
extern const Il2CppType KeyCollection_t2018332325_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t2018332325_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4049866126_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4049866126_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0, &Enumerator_t4049866126_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3097895223_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3097895223_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2802076604_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2802076604_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_0_0_0_0, &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 2, GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2934485036_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2934485036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4247873286_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types[] = { &DefaultComparer_t4247873286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4247873286_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t961487589_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t961487589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2536192150_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2536192150_0_0_0_Types[] = { &KeyValuePair_2_t2536192150_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536192150_0_0_0 = { 1, GenInst_KeyValuePair_2_t2536192150_0_0_0_Types };
extern const Il2CppType IDictionary_2_t435039943_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t435039943_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types[] = { &IDictionary_2_t435039943_gp_0_0_0_0, &IDictionary_2_t435039943_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t911337330_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t911337330_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t911337330_gp_0_0_0_0, &KeyValuePair_2_t911337330_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t951551555_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t951551555_gp_0_0_0_0_Types[] = { &List_1_t951551555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t951551555_gp_0_0_0_0 = { 1, GenInst_List_1_t951551555_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2095969493_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types[] = { &Enumerator_t2095969493_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2095969493_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3473121937_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types[] = { &Collection_1_t3473121937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3473121937_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t4004629011_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2631682684_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types[] = { &LinkedList_1_t2631682684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2631682684_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2473886460_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types[] = { &Enumerator_t2473886460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2473886460_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t603318558_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t603318558_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4128415215_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types[] = { &Stack_1_t4128415215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4128415215_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2313787817_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types[] = { &Enumerator_t2313787817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2313787817_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t3763949723_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types[] = { &HashSet_1_t3763949723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3763949723_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1211810685_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types[] = { &Enumerator_t1211810685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1211810685_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3144359540_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types[] = { &PrimeHelper_t3144359540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3144359540_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m1224968048_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types[] = { &Enumerable_Any_m1224968048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m3421076499_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m3421076499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2837618484_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2837618484_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2710245799_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2710245799_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Empty_m1047782786_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0_Types[] = { &Enumerable_Empty_m1047782786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0 = { 1, GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1340241468_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types[] = { &Enumerable_First_m1340241468_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1340241468_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m600216364_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m600216364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m3865828353_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m3865828353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m2068134811_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m2548367508_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m885533373_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m290105122_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2190484233_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2983504498_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m228373778_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m228373778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2204190206_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2204190206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m482375811_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types[] = { &Component_GetComponents_m482375811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m482375811_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0_Types };
extern const Il2CppType GUILayer_t2983897946_0_0_0;
static const Il2CppType* GenInst_GUILayer_t2983897946_0_0_0_Types[] = { &GUILayer_t2983897946_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t2983897946_0_0_0 = { 1, GenInst_GUILayer_t2983897946_0_0_0_Types };
extern const Il2CppType VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0;
static const Il2CppType* GenInst_VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0_Types[] = { &VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0 = { 1, GenInst_VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0_Types };
extern const Il2CppType ObjectTracker_t455954211_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t455954211_0_0_0_Types[] = { &ObjectTracker_t455954211_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t455954211_0_0_0 = { 1, GenInst_ObjectTracker_t455954211_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t2067565974_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t2067565974_0_0_0_Types[] = { &SmartTerrainTracker_t2067565974_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t2067565974_0_0_0 = { 1, GenInst_SmartTerrainTracker_t2067565974_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t1091759131_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t1091759131_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0_Types };
extern const Il2CppType DeviceTracker_t2823650348_0_0_0;
static const Il2CppType* GenInst_DeviceTracker_t2823650348_0_0_0_Types[] = { &DeviceTracker_t2823650348_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceTracker_t2823650348_0_0_0 = { 1, GenInst_DeviceTracker_t2823650348_0_0_0_Types };
extern const Il2CppType RotationalDeviceTracker_t3411245443_0_0_0;
static const Il2CppType* GenInst_RotationalDeviceTracker_t3411245443_0_0_0_Types[] = { &RotationalDeviceTracker_t3411245443_0_0_0 };
extern const Il2CppGenericInst GenInst_RotationalDeviceTracker_t3411245443_0_0_0 = { 1, GenInst_RotationalDeviceTracker_t3411245443_0_0_0_Types };
extern const Il2CppType DistortionRenderingBehaviour_t3790169744_0_0_0;
static const Il2CppType* GenInst_DistortionRenderingBehaviour_t3790169744_0_0_0_Types[] = { &DistortionRenderingBehaviour_t3790169744_0_0_0 };
extern const Il2CppGenericInst GenInst_DistortionRenderingBehaviour_t3790169744_0_0_0 = { 1, GenInst_DistortionRenderingBehaviour_t3790169744_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t2347335889_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t2347335889_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0_Types };
extern const Il2CppType DigitalEyewearAbstractBehaviour_t2786128077_0_0_0;
static const Il2CppType* GenInst_DigitalEyewearAbstractBehaviour_t2786128077_0_0_0_Types[] = { &DigitalEyewearAbstractBehaviour_t2786128077_0_0_0 };
extern const Il2CppGenericInst GenInst_DigitalEyewearAbstractBehaviour_t2786128077_0_0_0 = { 1, GenInst_DigitalEyewearAbstractBehaviour_t2786128077_0_0_0_Types };
extern const Il2CppType MarkerTracker_t4028259784_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t4028259784_0_0_0_Types[] = { &MarkerTracker_t4028259784_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t4028259784_0_0_0 = { 1, GenInst_MarkerTracker_t4028259784_0_0_0_Types };
extern const Il2CppType Mesh_t4241756145_0_0_0;
static const Il2CppType* GenInst_Mesh_t4241756145_0_0_0_Types[] = { &Mesh_t4241756145_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t4241756145_0_0_0 = { 1, GenInst_Mesh_t4241756145_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t41970945_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t41970945_0_0_0_Types[] = { &ReconstructionFromTarget_t41970945_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t41970945_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t41970945_0_0_0_Types };
extern const Il2CppType TextTracker_t3721656949_0_0_0;
static const Il2CppType* GenInst_TextTracker_t3721656949_0_0_0_Types[] = { &TextTracker_t3721656949_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t3721656949_0_0_0 = { 1, GenInst_TextTracker_t3721656949_0_0_0_Types };
extern const Il2CppType BackgroundPlaneBehaviour_t1927425041_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneBehaviour_t1927425041_0_0_0_Types[] = { &BackgroundPlaneBehaviour_t1927425041_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneBehaviour_t1927425041_0_0_0 = { 1, GenInst_BackgroundPlaneBehaviour_t1927425041_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t3695833539_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t3695833539_0_0_0_Types[] = { &ReconstructionBehaviour_t3695833539_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t3695833539_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t3695833539_0_0_0_Types };
extern const Il2CppType DigitalEyewearBehaviour_t77362895_0_0_0;
static const Il2CppType* GenInst_DigitalEyewearBehaviour_t77362895_0_0_0_Types[] = { &DigitalEyewearBehaviour_t77362895_0_0_0 };
extern const Il2CppGenericInst GenInst_DigitalEyewearBehaviour_t77362895_0_0_0 = { 1, GenInst_DigitalEyewearBehaviour_t77362895_0_0_0_Types };
extern const Il2CppType VideoBackgroundManager_t2163551206_0_0_0;
static const Il2CppType* GenInst_VideoBackgroundManager_t2163551206_0_0_0_Types[] = { &VideoBackgroundManager_t2163551206_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundManager_t2163551206_0_0_0 = { 1, GenInst_VideoBackgroundManager_t2163551206_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t2489055709_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t2489055709_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t1845338141_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t1845338141_0_0_0_Types[] = { &VuforiaBehaviour_t1845338141_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t1845338141_0_0_0 = { 1, GenInst_VuforiaBehaviour_t1845338141_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t1204933533_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t1204933533_0_0_0_Types[] = { &MaskOutBehaviour_t1204933533_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t1204933533_0_0_0 = { 1, GenInst_MaskOutBehaviour_t1204933533_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t2253448098_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t2253448098_0_0_0_Types[] = { &VirtualButtonBehaviour_t2253448098_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t2253448098_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t2253448098_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t1278464685_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t1278464685_0_0_0_Types[] = { &TurnOffBehaviour_t1278464685_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t1278464685_0_0_0 = { 1, GenInst_TurnOffBehaviour_t1278464685_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t1735871187_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t1735871187_0_0_0_Types[] = { &ImageTargetBehaviour_t1735871187_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t1735871187_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t1735871187_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t3170674893_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t3170674893_0_0_0_Types[] = { &MarkerBehaviour_t3170674893_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t3170674893_0_0_0 = { 1, GenInst_MarkerBehaviour_t3170674893_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t2222860085_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t2222860085_0_0_0_Types[] = { &MultiTargetBehaviour_t2222860085_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t2222860085_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t2222860085_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t1850077856_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t1850077856_0_0_0_Types[] = { &CylinderTargetBehaviour_t1850077856_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t1850077856_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t1850077856_0_0_0_Types };
extern const Il2CppType WordBehaviour_t2034767101_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t2034767101_0_0_0_Types[] = { &WordBehaviour_t2034767101_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t2034767101_0_0_0 = { 1, GenInst_WordBehaviour_t2034767101_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t892056475_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t892056475_0_0_0_Types[] = { &TextRecoBehaviour_t892056475_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t892056475_0_0_0 = { 1, GenInst_TextRecoBehaviour_t892056475_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t2508606743_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t2508606743_0_0_0_Types[] = { &ObjectTargetBehaviour_t2508606743_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t2508606743_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t2508606743_0_0_0_Types };
extern const Il2CppType VuMarkBehaviour_t2797714203_0_0_0;
static const Il2CppType* GenInst_VuMarkBehaviour_t2797714203_0_0_0_Types[] = { &VuMarkBehaviour_t2797714203_0_0_0 };
extern const Il2CppGenericInst GenInst_VuMarkBehaviour_t2797714203_0_0_0 = { 1, GenInst_VuMarkBehaviour_t2797714203_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t731195302_0_0_0_KeyValuePair_2_t731195302_0_0_0_Types[] = { &KeyValuePair_2_t731195302_0_0_0, &KeyValuePair_2_t731195302_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t731195302_0_0_0_KeyValuePair_2_t731195302_0_0_0 = { 2, GenInst_KeyValuePair_2_t731195302_0_0_0_KeyValuePair_2_t731195302_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t731195302_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t731195302_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t731195302_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t731195302_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Status_t835151357_0_0_0_Il2CppObject_0_0_0_Types[] = { &Status_t835151357_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t835151357_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Status_t835151357_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Status_t835151357_0_0_0_Status_t835151357_0_0_0_Types[] = { &Status_t835151357_0_0_0, &Status_t835151357_0_0_0 };
extern const Il2CppGenericInst GenInst_Status_t835151357_0_0_0_Status_t835151357_0_0_0 = { 2, GenInst_Status_t835151357_0_0_0_Status_t835151357_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0, &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0 = { 2, GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 2, GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0, &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt16_t24667923_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &UInt16_t24667923_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0, &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0 = { 2, GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0, &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0 = { 2, GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[560] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0,
	&GenInst_Char_t2862622538_0_0_0,
	&GenInst_IConvertible_t2116191568_0_0_0,
	&GenInst_IComparable_t1391370361_0_0_0,
	&GenInst_IComparable_1_t2772552329_0_0_0,
	&GenInst_IEquatable_1_t2411901105_0_0_0,
	&GenInst_ValueType_t1744280289_0_0_0,
	&GenInst_Int64_t1153838595_0_0_0,
	&GenInst_UInt32_t24667981_0_0_0,
	&GenInst_UInt64_t24668076_0_0_0,
	&GenInst_Byte_t2862609660_0_0_0,
	&GenInst_SByte_t1161769777_0_0_0,
	&GenInst_Int16_t1153838442_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t3464557803_0_0_0,
	&GenInst_ICloneable_t1025544834_0_0_0,
	&GenInst_IComparable_1_t4212128644_0_0_0,
	&GenInst_IEquatable_1_t3851477420_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t2853506214_0_0_0,
	&GenInst__Type_t2149739635_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1425685797_0_0_0,
	&GenInst__MemberInfo_t3353101921_0_0_0,
	&GenInst_IFormattable_t382002946_0_0_0,
	&GenInst_IComparable_1_t2772539451_0_0_0,
	&GenInst_IEquatable_1_t2411888227_0_0_0,
	&GenInst_Single_t4291918972_0_0_0,
	&GenInst_IComparable_1_t4201848763_0_0_0,
	&GenInst_IEquatable_1_t3841197539_0_0_0,
	&GenInst_Double_t3868226565_0_0_0,
	&GenInst_Decimal_t1954350631_0_0_0,
	&GenInst_IComparable_1_t1063768291_0_0_0,
	&GenInst_IEquatable_1_t703117067_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0,
	&GenInst_Delegate_t3310234105_0_0_0,
	&GenInst_ISerializable_t867484142_0_0_0,
	&GenInst_ParameterInfo_t2235474049_0_0_0,
	&GenInst__ParameterInfo_t2787166306_0_0_0,
	&GenInst_ParameterModifier_t741930026_0_0_0,
	&GenInst_IComparable_1_t4229565010_0_0_0,
	&GenInst_IEquatable_1_t3868913786_0_0_0,
	&GenInst_IComparable_1_t4229565068_0_0_0,
	&GenInst_IEquatable_1_t3868913844_0_0_0,
	&GenInst_IComparable_1_t4229565163_0_0_0,
	&GenInst_IEquatable_1_t3868913939_0_0_0,
	&GenInst_IComparable_1_t1063768233_0_0_0,
	&GenInst_IEquatable_1_t703117009_0_0_0,
	&GenInst_IComparable_1_t1071699568_0_0_0,
	&GenInst_IEquatable_1_t711048344_0_0_0,
	&GenInst_IComparable_1_t1063768386_0_0_0,
	&GenInst_IEquatable_1_t703117162_0_0_0,
	&GenInst_IComparable_1_t3778156356_0_0_0,
	&GenInst_IEquatable_1_t3417505132_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t209867187_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3971289384_0_0_0,
	&GenInst_MethodBase_t318515428_0_0_0,
	&GenInst__MethodBase_t3971068747_0_0_0,
	&GenInst_ConstructorInfo_t4136801618_0_0_0,
	&GenInst__ConstructorInfo_t3408715251_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t3372848153_0_0_0,
	&GenInst_TailoringInfo_t3025807515_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_Link_t2063667470_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1873037576_0_0_0,
	&GenInst_Contraction_t3998770676_0_0_0,
	&GenInst_Level2Map_t3664214860_0_0_0,
	&GenInst_BigInteger_t3334373498_0_0_0,
	&GenInst_KeySizes_t2106826975_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_IComparable_1_t386728509_0_0_0,
	&GenInst_IEquatable_1_t26077285_0_0_0,
	&GenInst_Slot_t2260530181_0_0_0,
	&GenInst_Slot_t2072023290_0_0_0,
	&GenInst_StackFrame_t1034942277_0_0_0,
	&GenInst_Calendar_t3558528576_0_0_0,
	&GenInst_ModuleBuilder_t595214213_0_0_0,
	&GenInst__ModuleBuilder_t1764509690_0_0_0,
	&GenInst_Module_t1394482686_0_0_0,
	&GenInst__Module_t2601912805_0_0_0,
	&GenInst_ParameterBuilder_t3159962230_0_0_0,
	&GenInst__ParameterBuilder_t4122453611_0_0_0,
	&GenInst_TypeU5BU5D_t3339007067_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t2643922881_0_0_0,
	&GenInst_IList_t1751339649_0_0_0,
	&GenInst_ILTokenInfo_t1354080954_0_0_0,
	&GenInst_LabelData_t3207823784_0_0_0,
	&GenInst_LabelFixup_t660379442_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t553556921_0_0_0,
	&GenInst_TypeBuilder_t1918497079_0_0_0,
	&GenInst__TypeBuilder_t3501492652_0_0_0,
	&GenInst_MethodBuilder_t302405488_0_0_0,
	&GenInst__MethodBuilder_t1471700965_0_0_0,
	&GenInst_ConstructorBuilder_t3217839941_0_0_0,
	&GenInst__ConstructorBuilder_t788093754_0_0_0,
	&GenInst_FieldBuilder_t1754069893_0_0_0,
	&GenInst__FieldBuilder_t639782778_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3711326812_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0,
	&GenInst_CustomAttributeData_t2955630591_0_0_0,
	&GenInst_ResourceInfo_t4013605874_0_0_0,
	&GenInst_ResourceCacheItem_t2113902833_0_0_0,
	&GenInst_IContextProperty_t82913453_0_0_0,
	&GenInst_Header_t1689611527_0_0_0,
	&GenInst_ITrackingHandler_t2228500544_0_0_0,
	&GenInst_IContextAttribute_t3913746816_0_0_0,
	&GenInst_DateTime_t4283661327_0_0_0,
	&GenInst_IComparable_1_t4193591118_0_0_0,
	&GenInst_IEquatable_1_t3832939894_0_0_0,
	&GenInst_IComparable_1_t1864280422_0_0_0,
	&GenInst_IEquatable_1_t1503629198_0_0_0,
	&GenInst_TimeSpan_t413522987_0_0_0,
	&GenInst_IComparable_1_t323452778_0_0_0,
	&GenInst_IEquatable_1_t4257768850_0_0_0,
	&GenInst_TypeTag_t2420703430_0_0_0,
	&GenInst_Enum_t2862688501_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2878058698_0_0_0,
	&GenInst_DateTimeOffset_t3884714306_0_0_0,
	&GenInst_Guid_t2862754429_0_0_0,
	&GenInst_Version_t763695022_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1195997794_0_0_0,
	&GenInst_X509Certificate_t3076817455_0_0_0,
	&GenInst_IDeserializationCallback_t675596727_0_0_0,
	&GenInst_X509ChainStatus_t766901931_0_0_0,
	&GenInst_Capture_t754001812_0_0_0,
	&GenInst_Group_t2151468941_0_0_0,
	&GenInst_Mark_t3811539797_0_0_0,
	&GenInst_UriScheme_t1290668975_0_0_0,
	&GenInst_BigInteger_t3334373499_0_0_0,
	&GenInst_ByteU5BU5D_t4260760469_0_0_0,
	&GenInst_ClientCertificateType_t3167042548_0_0_0,
	&GenInst_Link_t2122599155_0_0_0,
	&GenInst_Object_t3071478659_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0,
	&GenInst_IAchievementDescription_t655461400_0_0_0,
	&GenInst_IAchievementU5BU5D_t1953253797_0_0_0,
	&GenInst_IAchievement_t2957812780_0_0_0,
	&GenInst_IScoreU5BU5D_t250104726_0_0_0,
	&GenInst_IScore_t4279057999_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3419104218_0_0_0,
	&GenInst_IUserProfile_t598900827_0_0_0,
	&GenInst_AchievementDescription_t2116066607_0_0_0,
	&GenInst_UserProfile_t2280656072_0_0_0,
	&GenInst_GcLeaderboard_t1820874799_0_0_0,
	&GenInst_GcAchievementData_t3481375915_0_0_0,
	&GenInst_Achievement_t344600729_0_0_0,
	&GenInst_GcScoreData_t2181296590_0_0_0,
	&GenInst_Score_t3396031228_0_0_0,
	&GenInst_Vector3_t4282066566_0_0_0,
	&GenInst_Vector2_t4282066565_0_0_0,
	&GenInst_Material_t3870600107_0_0_0,
	&GenInst_Color_t4194546905_0_0_0,
	&GenInst_Color32_t598853688_0_0_0,
	&GenInst_Keyframe_t4079056114_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0,
	&GenInst_Behaviour_t200106419_0_0_0,
	&GenInst_Component_t3501516275_0_0_0,
	&GenInst_Display_t1321072632_0_0_0,
	&GenInst_Playable_t70832698_0_0_0,
	&GenInst_IDisposable_t1423340799_0_0_0,
	&GenInst_ContactPoint_t243083348_0_0_0,
	&GenInst_WebCamDevice_t3274004757_0_0_0,
	&GenInst_Font_t4241557075_0_0_0,
	&GenInst_GUILayoutOption_t331591504_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t775952400_0_0_0,
	&GenInst_GUILayoutEntry_t1336615025_0_0_0,
	&GenInst_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3710127902_0_0_0,
	&GenInst_DisallowMultipleComponent_t62111112_0_0_0,
	&GenInst_Attribute_t2523058482_0_0_0,
	&GenInst__Attribute_t3253047175_0_0_0,
	&GenInst_ExecuteInEditMode_t3132250205_0_0_0,
	&GenInst_RequireComponent_t1687166108_0_0_0,
	&GenInst_HitInfo_t3209134097_0_0_0,
	&GenInst_PersistentCall_t2972625667_0_0_0,
	&GenInst_BaseInvokableCall_t1559630662_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_GameObject_t3674682005_0_0_0,
	&GenInst_RenderTexture_t1963041563_0_0_0,
	&GenInst_Texture_t2526458961_0_0_0,
	&GenInst_EyewearCalibrationReading_t2729214813_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1857232484_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0,
	&GenInst_Renderer_t3076687687_0_0_0,
	&GenInst_TrackableBehaviour_t4179556250_0_0_0,
	&GenInst_VuMarkTarget_t3123531359_0_0_0,
	&GenInst_VuMarkAbstractBehaviour_t2231576857_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t3599762409_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3495806354_0_0_0,
	&GenInst_List_1_t3599762409_0_0_0,
	&GenInst_VuMarkTargetData_t3459596319_0_0_0,
	&GenInst_VuMarkTargetResultData_t2938518044_0_0_0,
	&GenInst_Link_t3400588580_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t2522024052_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2418067997_0_0_0,
	&GenInst_TrackableResultData_t395876724_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0,
	&GenInst_MonoBehaviour_t667441552_0_0_0,
	&GenInst_IViewerParameters_t2145870415_0_0_0,
	&GenInst_ITrackableEventHandler_t1613618350_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0,
	&GenInst_CameraField_t2874564333_0_0_0,
	&GenInst_ICloudRecoEventHandler_t796991165_0_0_0,
	&GenInst_TargetSearchResult_t3609410114_0_0_0,
	&GenInst_Trackable_t3781061455_0_0_0,
	&GenInst_VirtualButton_t704206407_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4089910802_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3677105400_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t600250352_0_0_0,
	&GenInst_DataSet_t2095838082_0_0_0,
	&GenInst_DataSetImpl_t1837478850_0_0_0,
	&GenInst_Marker_t616694972_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t512738917_0_0_0,
	&GenInst_WordData_t1548845516_0_0_0,
	&GenInst_WordResultData_t1826866697_0_0_0,
	&GenInst_IdPair_t3522586765_0_0_0,
	&GenInst_SmartTerrainRevisionData_t3919795561_0_0_0,
	&GenInst_SurfaceData_t1737958143_0_0_0,
	&GenInst_PropData_t526813605_0_0_0,
	&GenInst_Image_t2247677317_0_0_0,
	&GenInst_SmartTerrainTrackable_t1276145027_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t28871114_0_0_0,
	&GenInst_RectangleData_t2265684451_0_0_0,
	&GenInst_WordResult_t1079862857_0_0_0,
	&GenInst_Word_t2165514892_0_0_0,
	&GenInst_WordAbstractBehaviour_t545947899_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t975906802_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t441991844_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2633332527_0_0_0,
	&GenInst_List_1_t1914133451_0_0_0,
	&GenInst_ILoadLevelEventHandler_t678501447_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0,
	&GenInst_Prop_t2165309157_0_0_0,
	&GenInst_Surface_t3094389719_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2990433664_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t38412473_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2061353102_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1189512043_0_0_0,
	&GenInst_PropAbstractBehaviour_t1293468098_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0,
	&GenInst_MeshFilter_t3839065225_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4075600195_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t865486027_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0,
	&GenInst_IEditDataSetBehaviour_t1071163298_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0,
	&GenInst_KeyValuePair_2_t731195302_0_0_0,
	&GenInst_Status_t835151357_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_Status_t835151357_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Status_t835151357_0_0_0_KeyValuePair_2_t731195302_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_ImageTarget_t3520455670_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3416499615_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2680889866_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1087282249_0_0_0,
	&GenInst_ITrackerEventHandler_t3566668545_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0,
	&GenInst_InitError_t432217960_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0,
	&GenInst_ITextRecoEventHandler_t155716687_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0,
	&GenInst_MeshRenderer_t2804666580_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0,
	&GenInst_Collider_t2939674232_0_0_0,
	&GenInst_WireframeBehaviour_t433318935_0_0_0,
	&GenInst_IEnumerable_1_t309087608_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0,
	&GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0,
	&GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0,
	&GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0,
	&GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3566161319_gp_0_0_0_0,
	&GenInst_Array_Sort_m1767877396_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0,
	&GenInst_Array_compare_m3718693973_gp_0_0_0_0,
	&GenInst_Array_qsort_m3270161954_gp_0_0_0_0,
	&GenInst_Array_Resize_m2347367271_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1326446122_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m308210168_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1181152586_gp_0_0_0_0,
	&GenInst_Array_Exists_m2312185345_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0,
	&GenInst_Array_Find_m3878993575_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3671273393_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0,
	&GenInst_IList_1_t2706020430_gp_0_0_0_0,
	&GenInst_ICollection_1_t1952910030_gp_0_0_0_0,
	&GenInst_Nullable_1_t1122404262_gp_0_0_0_0,
	&GenInst_Comparer_1_t1701613202_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3219634540_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0,
	&GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1912719906_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4247873286_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2536192150_0_0_0,
	&GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0,
	&GenInst_List_1_t951551555_gp_0_0_0_0,
	&GenInst_Enumerator_t2095969493_gp_0_0_0_0,
	&GenInst_Collection_1_t3473121937_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0,
	&GenInst_LinkedList_1_t2631682684_gp_0_0_0_0,
	&GenInst_Enumerator_t2473886460_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0,
	&GenInst_Stack_1_t4128415215_gp_0_0_0_0,
	&GenInst_Enumerator_t2313787817_gp_0_0_0_0,
	&GenInst_HashSet_1_t3763949723_gp_0_0_0_0,
	&GenInst_Enumerator_t1211810685_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3144359540_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0,
	&GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1340241468_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m482375811_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0,
	&GenInst_GUILayer_t2983897946_0_0_0,
	&GenInst_VideoBackgroundManagerAbstractBehaviour_t3710195425_0_0_0,
	&GenInst_ObjectTracker_t455954211_0_0_0,
	&GenInst_SmartTerrainTracker_t2067565974_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0,
	&GenInst_DeviceTracker_t2823650348_0_0_0,
	&GenInst_RotationalDeviceTracker_t3411245443_0_0_0,
	&GenInst_DistortionRenderingBehaviour_t3790169744_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0,
	&GenInst_DigitalEyewearAbstractBehaviour_t2786128077_0_0_0,
	&GenInst_MarkerTracker_t4028259784_0_0_0,
	&GenInst_Mesh_t4241756145_0_0_0,
	&GenInst_ReconstructionFromTarget_t41970945_0_0_0,
	&GenInst_TextTracker_t3721656949_0_0_0,
	&GenInst_BackgroundPlaneBehaviour_t1927425041_0_0_0,
	&GenInst_ReconstructionBehaviour_t3695833539_0_0_0,
	&GenInst_DigitalEyewearBehaviour_t77362895_0_0_0,
	&GenInst_VideoBackgroundManager_t2163551206_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0,
	&GenInst_VuforiaBehaviour_t1845338141_0_0_0,
	&GenInst_MaskOutBehaviour_t1204933533_0_0_0,
	&GenInst_VirtualButtonBehaviour_t2253448098_0_0_0,
	&GenInst_TurnOffBehaviour_t1278464685_0_0_0,
	&GenInst_ImageTargetBehaviour_t1735871187_0_0_0,
	&GenInst_MarkerBehaviour_t3170674893_0_0_0,
	&GenInst_MultiTargetBehaviour_t2222860085_0_0_0,
	&GenInst_CylinderTargetBehaviour_t1850077856_0_0_0,
	&GenInst_WordBehaviour_t2034767101_0_0_0,
	&GenInst_TextRecoBehaviour_t892056475_0_0_0,
	&GenInst_ObjectTargetBehaviour_t2508606743_0_0_0,
	&GenInst_VuMarkBehaviour_t2797714203_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t731195302_0_0_0_KeyValuePair_2_t731195302_0_0_0,
	&GenInst_KeyValuePair_2_t731195302_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Status_t835151357_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Status_t835151357_0_0_0_Status_t835151357_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0,
};
