#pragma once

#include <stdint.h>
#include <stdarg.h>

#include "UnityForwardDecls.h"
#include "UnityRendering.h"

// unity plugin functions
typedef	void	(*UnityPluginSetGraphicsDeviceFunc)(void* device, int deviceType, int eventType);
typedef	void	(*UnityPluginRenderMarkerFunc)(int marker);
typedef int     (*UnityPluginGetAudioEffectDefinitionsFunc)(struct UnityAudioEffectDefinition*** descptr);

// log handler function
#ifdef __cplusplus
	typedef bool (*LogEntryHandler)(LogType logType, const char* log, va_list list);
#endif

//
// these are functions referenced in trampoline and implemented in unity player lib
//

#ifdef __cplusplus
extern "C" {
#endif

// life cycle management

void	UnityParseCommandLine(int argc, char* argv[]);
void	UnityInitApplicationNoGraphics(const char* appPathName);
    void	UnityInitApplicationGraphics(void);
    void	UnityCleanup(void);
    void	UnityLoadApplication(void);
    void	UnityPlayerLoop(void);					// normal player loop
    void	UnityBatchPlayerLoop(void);				// batch mode like player loop, without rendering (usable for background processing)
void	UnitySetPlayerFocus(int focused);	// send OnApplicationFocus() message to scripts
void	UnityPause(int pause);
    int		UnityIsPaused(void);					// 0 if player is running, 1 if paused
    void	UnityWillPause(void);					// send the message that app will pause
    void	UnityWillResume(void);					// send the message that app will resume
    void	UnityInputProcess(void);
    void	UnityDeliverUIEvents(void);				// unity processing impacting UI will be called in there


// rendering

int		UnityGetRenderingAPIs(int capacity, int* outAPIs);
int		UnityHasRenderingAPIExtension(const char* extension);
    void	UnityFinishRendering(void);

// for Create* functions if surf is null we will actuially create new one, otherwise we update the one provided
// gles: one and only one of texid/rbid should be non-zero
// metal: resolveTex should be non-nil only if tex have AA
UnityRenderBuffer	UnityCreateExternalSurfaceGLES(UnityRenderBuffer surf, int isColor, unsigned texid, unsigned rbid, unsigned glesFormat, const UnityRenderBufferDesc* desc);
UnityRenderBuffer	UnityCreateExternalSurfaceMTL(UnityRenderBuffer surf, int isColor, MTLTextureRef tex, const UnityRenderBufferDesc* desc);
UnityRenderBuffer	UnityCreateExternalColorSurfaceMTL(UnityRenderBuffer surf, MTLTextureRef tex, MTLTextureRef resolveTex, const UnityRenderBufferDesc* desc);
UnityRenderBuffer	UnityCreateExternalDepthSurfaceMTL(UnityRenderBuffer surf, MTLTextureRef tex, MTLTextureRef stencilTex, const UnityRenderBufferDesc* desc);
// creates "dummy" surface - will indicate "missing" buffer (e.g. depth-only RT will have color as dummy)
UnityRenderBuffer	UnityCreateDummySurface(UnityRenderBuffer surf, int isColor, const UnityRenderBufferDesc* desc);

// disable rendering to render buffers (all Cameras that were rendering to one of buffers would be reset to use backbuffer)
void	UnityDisableRenderBuffers(UnityRenderBuffer color, UnityRenderBuffer depth);
// destroys render buffer
void	UnityDestroyExternalSurface(UnityRenderBuffer surf);
// sets current render target
void	UnitySetRenderTarget(UnityRenderBuffer color, UnityRenderBuffer depth);
// final blit to backbuffer
void	UnityBlitToBackbuffer(UnityRenderBuffer srcColor, UnityRenderBuffer dstColor, UnityRenderBuffer dstDepth);
// signal unity that we start new frame with given backbuffer
void	UnityStartFrame(UnityRenderBuffer color, UnityRenderBuffer depth);
// signal unity that we are about to end current frame
    void	UnityEndFrame(void);


// This must match the one in ApiEnumsGLES.h
typedef enum UnityFramebufferTarget
{
	kDrawFramebuffer = 0,
	kReadFramebuffer,
	kFramebufferTargetCount
}UnityFramebufferTarget;
void	UnityBindFramebuffer(UnityFramebufferTarget target, int fbo);
void	UnityRegisterFBO(UnityRenderBuffer color, UnityRenderBuffer depth, unsigned fbo);

// metal plugins support

// we expect that you or use current MTLCommandEncoder (to do some custom rendering)
// or if you need your own MTLCommandEncoder you should end unity one with UnityEndCurrentMTLCommandEncoder and you should end your own before returning to unity

// queries current in-flight MTLCommandEncoder (might be nil)
    MTLCommandEncoderRef	UnityCurrentMTLCommandEncoder(void);
// ends current in-flight MTLCommandEncoder
    void					UnityEndCurrentMTLCommandEncoder(void);
// queries texture from RenderBuffer (both color/depth are supported)
MTLTextureRef			UnityRenderBufferMTLTexture(UnityRenderBuffer buffer);
// queries AA resolve texture from color RenderBuffer (might be nil, e.g. not AA-ed RenderBuffer). Returns nil for depth RenderBuffer
MTLTextureRef			UnityRenderBufferAAResolvedMTLTexture(UnityRenderBuffer buffer);
// queries stencil texture from depth RenderBuffer (might be nil). Returns nil for color RenderBuffer
MTLTextureRef			UnityRenderBufferStencilMTLTexture(UnityRenderBuffer buffer);


// controling player internals

// TODO: needs some cleanup
void	UnitySetAudioSessionActive(int active);
    void	UnityGLInvalidateState(void);
    void	UnityReloadResources(void);
    int		UnityIsCaptureScreenshotRequested(void);
    void	UnityCaptureScreenshot(void);
void	UnitySendMessage(const char* obj, const char* method, const char* msg);

    EAGLContext*		UnityGetDataContextGLES(void);
    MTLCommandBufferRef	UnityCurrentMTLCommandBuffer(void);

#ifdef __cplusplus
	void	UnitySetLogEntryHandler(LogEntryHandler newHandler);
#endif


// plugins support

void	UnityRegisterRenderingPlugin(UnityPluginSetGraphicsDeviceFunc setDevice, UnityPluginRenderMarkerFunc renderMarker);
void	UnityRegisterAudioPlugin(UnityPluginGetAudioEffectDefinitionsFunc getAudioEffectDefinitions);


// resolution/orientation handling

void	UnityGetRenderingResolution(unsigned* w, unsigned* h);
void	UnityGetSystemResolution(unsigned* w, unsigned* h);

void	UnityRequestRenderingResolution(unsigned w, unsigned h);

int		UnityIsOrientationEnabled(unsigned /*ScreenOrientation*/ orientation);
int		UnityShouldAutorotate();
    int		UnityRequestedScreenOrientation(void); // returns ScreenOrientation

int		UnityReportResizeView(unsigned w, unsigned h, unsigned /*ScreenOrientation*/ contentOrientation);	// returns ScreenOrientation
void	UnityReportBackbufferChange(UnityRenderBuffer colorBB, UnityRenderBuffer depthBB);



// player settings

    int		UnityDisableDepthAndStencilBuffers(void);
    int		UnityUseAnimatedAutorotation(void);
int		UnityGetDesiredMSAASampleCount(int defaultSampleCount);
    int		UnityGetSRGBRequested(void);
    int		UnityGetShowActivityIndicatorOnLoading(void);
    int		UnityGetAccelerometerFrequency(void);
    int		UnityGetTargetFPS(void);
    int		UnityGetAppBackgroundBehavior(void);


// push notifications
#if !UNITY_TVOS
void	UnitySendLocalNotification(UILocalNotification* notification);
#endif
void	UnitySendRemoteNotification(NSDictionary* notification);
void	UnitySendDeviceToken(NSData* deviceToken);
void	UnitySendRemoteNotificationError(NSError* error);

// native events

    void	UnityADBannerViewWasClicked(void);
    void	UnityADBannerViewWasLoaded(void);
    void	UnityADBannerViewFailedToLoad(void);
    void	UnityADInterstitialADWasLoaded(void);
    void	UnityADInterstitialADWasViewed(void);
    void	UnityUpdateDisplayList(void);


// profiler

void*	UnityCreateProfilerCounter(const char*);
void	UnityDestroyProfilerCounter(void*);
void	UnityStartProfilerCounter(void*);
void	UnityEndProfilerCounter(void*);


// sensors

void	UnitySensorsSetGyroRotationRate(int idx, float x, float y, float z);
void	UnitySensorsSetGyroRotationRateUnbiased(int idx, float x, float y, float z);
void	UnitySensorsSetGravity(int idx, float x, float y, float z);
void	UnitySensorsSetUserAcceleration(int idx, float x, float y, float z);
void	UnitySensorsSetAttitude(int idx, float x, float y, float z, float w);
void	UnityDidAccelerate(float x, float y, float z, double timestamp);
void	UnitySetJoystickPosition (int joyNum, int axis, float pos);
int		UnityStringToKey(const char *name);
void	UnitySetKeyState (int key, int /*bool*/ state);

// WWW connection handling

void	UnityReportWWWStatusError(void* udata, int status, const char* error);

void	UnityReportWWWReceivedResponse(void* udata, int status, unsigned expectedDataLength, const char* respHeader);
void	UnityReportWWWReceivedData(void* udata, const void* buffer, unsigned totalRead, unsigned expectedTotal);
void	UnityReportWWWFinishedLoadingData(void* udata);
void	UnityReportWWWSentData(void* udata, unsigned totalWritten, unsigned expectedTotal);

// AVCapture

    void	UnityReportAVCapturePermission(void);
void	UnityDidCaptureVideoFrame(intptr_t tex, void* udata);

// logging override

#ifdef __cplusplus
} // extern "C"
#endif


// touches processing

#ifdef __cplusplus
extern "C" {
#endif

void	UnitySetViewTouchProcessing(UIView* view, int /*ViewTouchProcessing*/ processingPolicy);
void	UnityDropViewTouchProcessing(UIView* view);

void	UnitySendTouchesBegin(NSSet* touches, UIEvent* event);
void	UnitySendTouchesEnded(NSSet* touches, UIEvent* event);
void	UnitySendTouchesCancelled(NSSet* touches, UIEvent* event);
void	UnitySendTouchesMoved(NSSet* touches, UIEvent* event);

    void	UnityCancelTouches(void);

#ifdef __cplusplus
} // extern "C"
#endif


//
// these are functions referenced and implemented in trampoline
//

#ifdef __cplusplus
extern "C" {
#endif

// UnityAppController.mm
    UIViewController*		UnityGetGLViewController(void);
    UIView*					UnityGetGLView(void);
    UIWindow*				UnityGetMainWindow(void);
    enum ScreenOrientation	UnityCurrentOrientation(void);

// Unity/DisplayManager.mm
float					UnityScreenScaleFactor(UIScreen* screen);

#ifdef __cplusplus
} // extern "C"
#endif


//
// these are functions referenced in unity player lib and implemented in trampoline
//

#ifdef __cplusplus
extern "C" {
#endif

// iPhone_Sensors.mm
    void			UnityInitJoysticks(void);
    void			UnityCoreMotionStart(void);
    void			UnityCoreMotionStop(void);
int				UnityIsGyroEnabled(int idx);
    int				UnityIsGyroAvailable(void);
    void			UnityUpdateGyroData(void);
void			UnitySetGyroUpdateInterval(int idx, float interval);
float			UnityGetGyroUpdateInterval(int idx);
    void			UnityUpdateJoystickData(void);
    int				UnityGetJoystickCount(void);
void			UnityGetJoystickName(int idx, char* buffer, int maxLen);
void			UnityGetJoystickAxisName(int idx, int axis, char* buffer, int maxLen);
void			UnityGetNiceKeyname(int key, char* buffer, int maxLen);

// UnityAppController+Rendering.mm
    void			UnityInitMainScreenRenderingCallback(void);
    void			UnityGfxInitedCallback(void);
void			UnityPresentContextCallback(struct UnityFrameStats const* frameStats);
void			UnityFramerateChangeCallback(int targetFPS);
    int				UnitySelectedRenderingAPI(void);

    NSBundle*			UnityGetMetalBundle(void);
    MTLDeviceRef		UnityGetMetalDevice(void);
    MTLCommandQueueRef	UnityGetMetalCommandQueue(void);

    EAGLContext*		UnityGetDataContextEAGL(void);

    UnityRenderBuffer	UnityBackbufferColor(void);
    UnityRenderBuffer	UnityBackbufferDepth(void);

// UI/ActivityIndicator.mm
    void			UnityStartActivityIndicator(void);
    void			UnityStopActivityIndicator(void);

// UI/Keyboard.mm
void			UnityKeyboard_Create(unsigned keyboardType, int autocorrection, int multiline, int secure, int alert, const char* text, const char* placeholder);
    void			UnityKeyboard_Show(void);
    void			UnityKeyboard_Hide(void);
void			UnityKeyboard_GetRect(float* x, float* y, float* w, float* h);
void			UnityKeyboard_SetText(const char* text);
    NSString*		UnityKeyboard_GetText(void);
    int				UnityKeyboard_IsActive(void);
    int				UnityKeyboard_IsDone(void);
    int				UnityKeyboard_WasCanceled(void);
void			UnityKeyboard_SetInputHidden(int hidden);
    int				UnityKeyboard_IsInputHidden(void);

// UI/UnityViewControllerBase.mm
    void			UnityNotifyAutoOrientationChange(void);

// Unity/AVCapture.mm
int				UnityGetAVCapturePermission(int captureTypes);
void			UnityRequestAVCapturePermission(int captureTypes);

// Unity/CameraCapture.mm
void			UnityEnumVideoCaptureDevices(void* udata, void(*callback)(void* udata, const char* name, int frontFacing));
void*			UnityInitCameraCapture(int device, int w, int h, int fps, void* udata);
void			UnityStartCameraCapture(void* capture);
void			UnityPauseCameraCapture(void* capture);
void			UnityStopCameraCapture(void* capture);
void			UnityCameraCaptureExtents(void* capture, int* w, int* h);
void			UnityCameraCaptureReadToMemory(void* capture, void* dst, int w, int h);
int				UnityCameraCaptureVideoRotationDeg(void* capture);
int				UnityCameraCaptureVerticallyMirrored(void* capture);


// Unity/DeviceSettings.mm
    const char*		UnityDeviceUniqueIdentifier(void);
    const char*		UnityVendorIdentifier(void);
    const char*		UnityAdvertisingIdentifier(void);
    int				UnityAdvertisingTrackingEnabled(void);
    const char*		UnityDeviceName(void);
    const char*		UnitySystemName(void);
    const char*		UnitySystemVersion(void);
    const char*		UnityDeviceModel(void);
    int				UnityDeviceCPUCount(void);
    int				UnityDeviceGeneration(void);
    float			UnityDeviceDPI(void);
    const char*		UnitySystemLanguage(void);

// Unity/DisplayManager.mm
    EAGLContext*	UnityGetMainScreenContextGLES(void);
    EAGLContext*	UnityGetContextEAGL(void);
    void			UnityStartFrameRendering(void);

// Unity/Filesystem.mm
    const char*		UnityApplicationDir(void);
    const char*		UnityDocumentsDir(void);
    const char*		UnityLibraryDir(void);
    const char*		UnityCachesDir(void);
int				UnityUpdateNoBackupFlag(const char* path, int setFlag); // Returns 1 if successful, otherwise 0

// Unity/WWWConnection.mm
void*			UnityStartWWWConnectionGet(void* udata, const void* headerDict, const char* url);
void*			UnityStartWWWConnectionPost(void* udata, const void* headerDict, const char* url, const void* data, unsigned length);
void			UnityDestroyWWWConnection(void* connection);
void			UnityShouldCancelWWW(const void* connection);

//Apple TV Remote
    int			UnityGetAppleTVRemoteAllowExitToMenu(void);
void		UnitySetAppleTVRemoteAllowExitToMenu(int val);
    int			UnityGetAppleTVRemoteAllowRotation(void);
void		UnitySetAppleTVRemoteAllowRotation(int val);
    int			UnityGetAppleTVRemoteReportAbsoluteDpadValues(void);
void		UnitySetAppleTVRemoteReportAbsoluteDpadValues(int val);
    int			UnityGetAppleTVRemoteTouchesEnabled(void);
void		UnitySetAppleTVRemoteTouchesEnabled(int val);

#ifdef __cplusplus
} // extern "C"
#endif


#ifdef __OBJC__
	// This is basically a wrapper for [NSString UTF8String] with additional strdup.
	//
	// Apparently multiple calls on UTF8String will leak memory (NSData objects) that are collected
	// only when @autoreleasepool is exited. This function serves as documentation for this and as a
	// handy wrapper.
	inline char* AllocCString(NSString* value)
	{
		if(value == nil)
			return 0;

		const char* str = [value UTF8String];
		return str ? strdup(str) : 0;
	}
#endif
