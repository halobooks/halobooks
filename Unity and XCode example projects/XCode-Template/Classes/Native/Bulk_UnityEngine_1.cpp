﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t1844602099;
// UnityEngine.iOS.ADBannerView
struct ADBannerView_t2392257822;
// UnityEngine.iOS.ADBannerView/BannerFailedToLoadDelegate
struct BannerFailedToLoadDelegate_t2244784594;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate
struct BannerWasClickedDelegate_t3510840818;
// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate
struct BannerWasLoadedDelegate_t3604098244;
// UnityEngine.iOS.ADInterstitialAd
struct ADInterstitialAd_t3393793052;
// UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate
struct InterstitialWasLoadedDelegate_t3068494210;
// UnityEngine.iOS.ADInterstitialAd/InterstitialWasViewedDelegate
struct InterstitialWasViewedDelegate_t507319425;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t1344855248;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t1652317979;
// UnityEngine.Logger
struct Logger_t2997509588;
// UnityEngine.ILogHandler
struct ILogHandler_t2265139045;
// UnityEngine.Object
struct Object_t3071478659;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Type
struct Type_t;
// UnityEngine.RectOffset
struct RectOffset_t3056157787;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t779639188;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.RequireComponent
struct RequireComponent_t1687166108;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3731857623;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// UnityEngine.RPC
struct RPC_t3134615963;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2970544072;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t3165457172;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3197444790;
// UnityEngine.GUILayer
struct GUILayer_t2983897946;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t2216353654;
// UnityEngine.SerializeField
struct SerializeField_t3754825534;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2835496234;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1486273289;
// UnityEngine.SliderState
struct SliderState_t1233388262;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t3570684786;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2654168339;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1726768202;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1670395707;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t229750097;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2349069933;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t645920862;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3799088250;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3814920354;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2378268441;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1820874799;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1185876199;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t1307362368;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218;
// System.Diagnostics.StackTrace
struct StackTrace_t1047871261;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t759180893;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Experimental.Director.AnimatorControllerPlayable
struct AnimatorControllerPlayable_t3906681469;
// UnityEngine.TextEditor
struct TextEditor_t319394238;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Transform/Enumerator
struct Enumerator_t3875554846;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3134093121;
// System.Exception
struct Exception_t3991598821;
// UnityEngine.UnityException
struct UnityException_t3473321374;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt1844602099.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt1844602099MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments1587375252.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments1587375252MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec4294668057.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec4294668057MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd3393793052.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd3393793052MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter3068494210MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter3068494210.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters507319425MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters507319425.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier514511185.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier514511185MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit3375281836.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit3375281836MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1652317979.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1652317979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Logger2997509588.h"
#include "UnityEngine_UnityEngine_Logger2997509588MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_LogType4286006228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngine_Plane4206452690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_RPC3134615963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Reflection_Binder1074302268.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "UnityEngine_UnityEngine_SliderState1233388262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2349069933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "mscorlib_System_Action_1_gen2349069933.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen645920862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "mscorlib_System_Action_1_gen645920862.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121.h"
#include "mscorlib_System_Action_1_gen3814920354MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3906681469.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor319394238.h"
#include "UnityEngine_UnityEngine_TextEditor319394238MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_FilterMode1625068031.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692MethodDeclarations.h"
#include "mscorlib_System_AppDomain3575612635MethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120MethodDeclarations.h"
#include "mscorlib_System_AppDomain3575612635.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException3473321374.h"
#include "UnityEngine_UnityEngine_UnityException3473321374MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "UnityEngine_UnityEngine_UnityString3369712284.h"
#include "UnityEngine_UnityEngine_UserAuthorization2016802404.h"
#include "UnityEngine_UnityEngine_UserAuthorization2016802404MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t2983897946_m2891371969(__this, method) ((  GUILayer_t2983897946 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C"  void ExcludeFromDocsAttribute__ctor_m623077387 (ExcludeFromDocsAttribute_t1844602099 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawArguments
extern "C" void Internal_DrawArguments_t1587375252_marshal_pinvoke(const Internal_DrawArguments_t1587375252& unmarshaled, Internal_DrawArguments_t1587375252_marshaled_pinvoke& marshaled)
{
	marshaled.___target_0 = reinterpret_cast<intptr_t>((unmarshaled.get_target_0()).get_m_value_0());
	Rect_t4241904616_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	marshaled.___isHover_2 = unmarshaled.get_isHover_2();
	marshaled.___isActive_3 = unmarshaled.get_isActive_3();
	marshaled.___on_4 = unmarshaled.get_on_4();
	marshaled.___hasKeyboardFocus_5 = unmarshaled.get_hasKeyboardFocus_5();
}
extern "C" void Internal_DrawArguments_t1587375252_marshal_pinvoke_back(const Internal_DrawArguments_t1587375252_marshaled_pinvoke& marshaled, Internal_DrawArguments_t1587375252& unmarshaled)
{
	IntPtr_t unmarshaled_target_temp_0;
	memset(&unmarshaled_target_temp_0, 0, sizeof(unmarshaled_target_temp_0));
	IntPtr_t unmarshaled_target_temp_0_temp;
	unmarshaled_target_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___target_0));
	unmarshaled_target_temp_0 = unmarshaled_target_temp_0_temp;
	unmarshaled.set_target_0(unmarshaled_target_temp_0);
	Rect_t4241904616  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Rect_t4241904616_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	int32_t unmarshaled_isHover_temp_2 = 0;
	unmarshaled_isHover_temp_2 = marshaled.___isHover_2;
	unmarshaled.set_isHover_2(unmarshaled_isHover_temp_2);
	int32_t unmarshaled_isActive_temp_3 = 0;
	unmarshaled_isActive_temp_3 = marshaled.___isActive_3;
	unmarshaled.set_isActive_3(unmarshaled_isActive_temp_3);
	int32_t unmarshaled_on_temp_4 = 0;
	unmarshaled_on_temp_4 = marshaled.___on_4;
	unmarshaled.set_on_4(unmarshaled_on_temp_4);
	int32_t unmarshaled_hasKeyboardFocus_temp_5 = 0;
	unmarshaled_hasKeyboardFocus_temp_5 = marshaled.___hasKeyboardFocus_5;
	unmarshaled.set_hasKeyboardFocus_5(unmarshaled_hasKeyboardFocus_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawArguments
extern "C" void Internal_DrawArguments_t1587375252_marshal_pinvoke_cleanup(Internal_DrawArguments_t1587375252_marshaled_pinvoke& marshaled)
{
	Rect_t4241904616_marshal_pinvoke_cleanup(marshaled.___position_1);
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawArguments
extern "C" void Internal_DrawArguments_t1587375252_marshal_com(const Internal_DrawArguments_t1587375252& unmarshaled, Internal_DrawArguments_t1587375252_marshaled_com& marshaled)
{
	marshaled.___target_0 = reinterpret_cast<intptr_t>((unmarshaled.get_target_0()).get_m_value_0());
	Rect_t4241904616_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	marshaled.___isHover_2 = unmarshaled.get_isHover_2();
	marshaled.___isActive_3 = unmarshaled.get_isActive_3();
	marshaled.___on_4 = unmarshaled.get_on_4();
	marshaled.___hasKeyboardFocus_5 = unmarshaled.get_hasKeyboardFocus_5();
}
extern "C" void Internal_DrawArguments_t1587375252_marshal_com_back(const Internal_DrawArguments_t1587375252_marshaled_com& marshaled, Internal_DrawArguments_t1587375252& unmarshaled)
{
	IntPtr_t unmarshaled_target_temp_0;
	memset(&unmarshaled_target_temp_0, 0, sizeof(unmarshaled_target_temp_0));
	IntPtr_t unmarshaled_target_temp_0_temp;
	unmarshaled_target_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___target_0));
	unmarshaled_target_temp_0 = unmarshaled_target_temp_0_temp;
	unmarshaled.set_target_0(unmarshaled_target_temp_0);
	Rect_t4241904616  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Rect_t4241904616_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	int32_t unmarshaled_isHover_temp_2 = 0;
	unmarshaled_isHover_temp_2 = marshaled.___isHover_2;
	unmarshaled.set_isHover_2(unmarshaled_isHover_temp_2);
	int32_t unmarshaled_isActive_temp_3 = 0;
	unmarshaled_isActive_temp_3 = marshaled.___isActive_3;
	unmarshaled.set_isActive_3(unmarshaled_isActive_temp_3);
	int32_t unmarshaled_on_temp_4 = 0;
	unmarshaled_on_temp_4 = marshaled.___on_4;
	unmarshaled.set_on_4(unmarshaled_on_temp_4);
	int32_t unmarshaled_hasKeyboardFocus_temp_5 = 0;
	unmarshaled_hasKeyboardFocus_temp_5 = marshaled.___hasKeyboardFocus_5;
	unmarshaled.set_hasKeyboardFocus_5(unmarshaled_hasKeyboardFocus_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawArguments
extern "C" void Internal_DrawArguments_t1587375252_marshal_com_cleanup(Internal_DrawArguments_t1587375252_marshaled_com& marshaled)
{
	Rect_t4241904616_marshal_com_cleanup(marshaled.___position_1);
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawWithTextSelectionArguments
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_pinvoke(const Internal_DrawWithTextSelectionArguments_t4294668057& unmarshaled, Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_pinvoke& marshaled)
{
	marshaled.___target_0 = reinterpret_cast<intptr_t>((unmarshaled.get_target_0()).get_m_value_0());
	Rect_t4241904616_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	marshaled.___firstPos_2 = unmarshaled.get_firstPos_2();
	marshaled.___lastPos_3 = unmarshaled.get_lastPos_3();
	Color_t4194546905_marshal_pinvoke(unmarshaled.get_cursorColor_4(), marshaled.___cursorColor_4);
	Color_t4194546905_marshal_pinvoke(unmarshaled.get_selectionColor_5(), marshaled.___selectionColor_5);
	marshaled.___isHover_6 = unmarshaled.get_isHover_6();
	marshaled.___isActive_7 = unmarshaled.get_isActive_7();
	marshaled.___on_8 = unmarshaled.get_on_8();
	marshaled.___hasKeyboardFocus_9 = unmarshaled.get_hasKeyboardFocus_9();
	marshaled.___drawSelectionAsComposition_10 = unmarshaled.get_drawSelectionAsComposition_10();
}
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_pinvoke_back(const Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_pinvoke& marshaled, Internal_DrawWithTextSelectionArguments_t4294668057& unmarshaled)
{
	IntPtr_t unmarshaled_target_temp_0;
	memset(&unmarshaled_target_temp_0, 0, sizeof(unmarshaled_target_temp_0));
	IntPtr_t unmarshaled_target_temp_0_temp;
	unmarshaled_target_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___target_0));
	unmarshaled_target_temp_0 = unmarshaled_target_temp_0_temp;
	unmarshaled.set_target_0(unmarshaled_target_temp_0);
	Rect_t4241904616  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Rect_t4241904616_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	int32_t unmarshaled_firstPos_temp_2 = 0;
	unmarshaled_firstPos_temp_2 = marshaled.___firstPos_2;
	unmarshaled.set_firstPos_2(unmarshaled_firstPos_temp_2);
	int32_t unmarshaled_lastPos_temp_3 = 0;
	unmarshaled_lastPos_temp_3 = marshaled.___lastPos_3;
	unmarshaled.set_lastPos_3(unmarshaled_lastPos_temp_3);
	Color_t4194546905  unmarshaled_cursorColor_temp_4;
	memset(&unmarshaled_cursorColor_temp_4, 0, sizeof(unmarshaled_cursorColor_temp_4));
	Color_t4194546905_marshal_pinvoke_back(marshaled.___cursorColor_4, unmarshaled_cursorColor_temp_4);
	unmarshaled.set_cursorColor_4(unmarshaled_cursorColor_temp_4);
	Color_t4194546905  unmarshaled_selectionColor_temp_5;
	memset(&unmarshaled_selectionColor_temp_5, 0, sizeof(unmarshaled_selectionColor_temp_5));
	Color_t4194546905_marshal_pinvoke_back(marshaled.___selectionColor_5, unmarshaled_selectionColor_temp_5);
	unmarshaled.set_selectionColor_5(unmarshaled_selectionColor_temp_5);
	int32_t unmarshaled_isHover_temp_6 = 0;
	unmarshaled_isHover_temp_6 = marshaled.___isHover_6;
	unmarshaled.set_isHover_6(unmarshaled_isHover_temp_6);
	int32_t unmarshaled_isActive_temp_7 = 0;
	unmarshaled_isActive_temp_7 = marshaled.___isActive_7;
	unmarshaled.set_isActive_7(unmarshaled_isActive_temp_7);
	int32_t unmarshaled_on_temp_8 = 0;
	unmarshaled_on_temp_8 = marshaled.___on_8;
	unmarshaled.set_on_8(unmarshaled_on_temp_8);
	int32_t unmarshaled_hasKeyboardFocus_temp_9 = 0;
	unmarshaled_hasKeyboardFocus_temp_9 = marshaled.___hasKeyboardFocus_9;
	unmarshaled.set_hasKeyboardFocus_9(unmarshaled_hasKeyboardFocus_temp_9);
	int32_t unmarshaled_drawSelectionAsComposition_temp_10 = 0;
	unmarshaled_drawSelectionAsComposition_temp_10 = marshaled.___drawSelectionAsComposition_10;
	unmarshaled.set_drawSelectionAsComposition_10(unmarshaled_drawSelectionAsComposition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawWithTextSelectionArguments
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_pinvoke_cleanup(Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_pinvoke& marshaled)
{
	Rect_t4241904616_marshal_pinvoke_cleanup(marshaled.___position_1);
	Color_t4194546905_marshal_pinvoke_cleanup(marshaled.___cursorColor_4);
	Color_t4194546905_marshal_pinvoke_cleanup(marshaled.___selectionColor_5);
}
// Conversion methods for marshalling of: UnityEngine.Internal_DrawWithTextSelectionArguments
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_com(const Internal_DrawWithTextSelectionArguments_t4294668057& unmarshaled, Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_com& marshaled)
{
	marshaled.___target_0 = reinterpret_cast<intptr_t>((unmarshaled.get_target_0()).get_m_value_0());
	Rect_t4241904616_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	marshaled.___firstPos_2 = unmarshaled.get_firstPos_2();
	marshaled.___lastPos_3 = unmarshaled.get_lastPos_3();
	Color_t4194546905_marshal_com(unmarshaled.get_cursorColor_4(), marshaled.___cursorColor_4);
	Color_t4194546905_marshal_com(unmarshaled.get_selectionColor_5(), marshaled.___selectionColor_5);
	marshaled.___isHover_6 = unmarshaled.get_isHover_6();
	marshaled.___isActive_7 = unmarshaled.get_isActive_7();
	marshaled.___on_8 = unmarshaled.get_on_8();
	marshaled.___hasKeyboardFocus_9 = unmarshaled.get_hasKeyboardFocus_9();
	marshaled.___drawSelectionAsComposition_10 = unmarshaled.get_drawSelectionAsComposition_10();
}
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_com_back(const Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_com& marshaled, Internal_DrawWithTextSelectionArguments_t4294668057& unmarshaled)
{
	IntPtr_t unmarshaled_target_temp_0;
	memset(&unmarshaled_target_temp_0, 0, sizeof(unmarshaled_target_temp_0));
	IntPtr_t unmarshaled_target_temp_0_temp;
	unmarshaled_target_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___target_0));
	unmarshaled_target_temp_0 = unmarshaled_target_temp_0_temp;
	unmarshaled.set_target_0(unmarshaled_target_temp_0);
	Rect_t4241904616  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Rect_t4241904616_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	int32_t unmarshaled_firstPos_temp_2 = 0;
	unmarshaled_firstPos_temp_2 = marshaled.___firstPos_2;
	unmarshaled.set_firstPos_2(unmarshaled_firstPos_temp_2);
	int32_t unmarshaled_lastPos_temp_3 = 0;
	unmarshaled_lastPos_temp_3 = marshaled.___lastPos_3;
	unmarshaled.set_lastPos_3(unmarshaled_lastPos_temp_3);
	Color_t4194546905  unmarshaled_cursorColor_temp_4;
	memset(&unmarshaled_cursorColor_temp_4, 0, sizeof(unmarshaled_cursorColor_temp_4));
	Color_t4194546905_marshal_com_back(marshaled.___cursorColor_4, unmarshaled_cursorColor_temp_4);
	unmarshaled.set_cursorColor_4(unmarshaled_cursorColor_temp_4);
	Color_t4194546905  unmarshaled_selectionColor_temp_5;
	memset(&unmarshaled_selectionColor_temp_5, 0, sizeof(unmarshaled_selectionColor_temp_5));
	Color_t4194546905_marshal_com_back(marshaled.___selectionColor_5, unmarshaled_selectionColor_temp_5);
	unmarshaled.set_selectionColor_5(unmarshaled_selectionColor_temp_5);
	int32_t unmarshaled_isHover_temp_6 = 0;
	unmarshaled_isHover_temp_6 = marshaled.___isHover_6;
	unmarshaled.set_isHover_6(unmarshaled_isHover_temp_6);
	int32_t unmarshaled_isActive_temp_7 = 0;
	unmarshaled_isActive_temp_7 = marshaled.___isActive_7;
	unmarshaled.set_isActive_7(unmarshaled_isActive_temp_7);
	int32_t unmarshaled_on_temp_8 = 0;
	unmarshaled_on_temp_8 = marshaled.___on_8;
	unmarshaled.set_on_8(unmarshaled_on_temp_8);
	int32_t unmarshaled_hasKeyboardFocus_temp_9 = 0;
	unmarshaled_hasKeyboardFocus_temp_9 = marshaled.___hasKeyboardFocus_9;
	unmarshaled.set_hasKeyboardFocus_9(unmarshaled_hasKeyboardFocus_temp_9);
	int32_t unmarshaled_drawSelectionAsComposition_temp_10 = 0;
	unmarshaled_drawSelectionAsComposition_temp_10 = marshaled.___drawSelectionAsComposition_10;
	unmarshaled.set_drawSelectionAsComposition_10(unmarshaled_drawSelectionAsComposition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.Internal_DrawWithTextSelectionArguments
extern "C" void Internal_DrawWithTextSelectionArguments_t4294668057_marshal_com_cleanup(Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_com& marshaled)
{
	Rect_t4241904616_marshal_com_cleanup(marshaled.___position_1);
	Color_t4194546905_marshal_com_cleanup(marshaled.___cursorColor_4);
	Color_t4194546905_marshal_com_cleanup(marshaled.___selectionColor_5);
}
// System.Void UnityEngine.iOS.ADBannerView::.cctor()
extern "C"  void ADBannerView__cctor_m2737897525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)
extern "C"  void ADBannerView_Native_DestroyBanner_m3588853962 (Il2CppObject * __this /* static, unused */, IntPtr_t ___view0, const MethodInfo* method)
{
	typedef void (*ADBannerView_Native_DestroyBanner_m3588853962_ftn) (IntPtr_t);
	static ADBannerView_Native_DestroyBanner_m3588853962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADBannerView_Native_DestroyBanner_m3588853962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADBannerView::Native_DestroyBanner(System.IntPtr)");
	_il2cpp_icall_func(___view0);
}
// System.Void UnityEngine.iOS.ADBannerView::Finalize()
extern Il2CppClass* ADBannerView_t2392257822_il2cpp_TypeInfo_var;
extern const uint32_t ADBannerView_Finalize_m2140033770_MetadataUsageId;
extern "C"  void ADBannerView_Finalize_m2140033770 (ADBannerView_t2392257822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADBannerView_Finalize_m2140033770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = __this->get__bannerView_0();
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		ADBannerView_Native_DestroyBanner_m3588853962(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasClicked()
extern Il2CppClass* ADBannerView_t2392257822_il2cpp_TypeInfo_var;
extern const uint32_t ADBannerView_FireBannerWasClicked_m3637402988_MetadataUsageId;
extern "C"  void ADBannerView_FireBannerWasClicked_m3637402988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADBannerView_FireBannerWasClicked_m3637402988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerWasClickedDelegate_t3510840818 * L_0 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerWasClicked_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerWasClickedDelegate_t3510840818 * L_1 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerWasClicked_1();
		NullCheck(L_1);
		BannerWasClickedDelegate_Invoke_m1164690252(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::FireBannerWasLoaded()
extern Il2CppClass* ADBannerView_t2392257822_il2cpp_TypeInfo_var;
extern const uint32_t ADBannerView_FireBannerWasLoaded_m3273736514_MetadataUsageId;
extern "C"  void ADBannerView_FireBannerWasLoaded_m3273736514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADBannerView_FireBannerWasLoaded_m3273736514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t3604098244 * L_0 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerWasLoaded_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerWasLoadedDelegate_t3604098244 * L_1 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerWasLoaded_2();
		NullCheck(L_1);
		BannerWasLoadedDelegate_Invoke_m3949163900(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView::FireBannerFailedToLoad()
extern Il2CppClass* ADBannerView_t2392257822_il2cpp_TypeInfo_var;
extern const uint32_t ADBannerView_FireBannerFailedToLoad_m1790847436_MetadataUsageId;
extern "C"  void ADBannerView_FireBannerFailedToLoad_m1790847436 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADBannerView_FireBannerFailedToLoad_m1790847436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerFailedToLoadDelegate_t2244784594 * L_0 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerFailedToLoad_3();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADBannerView_t2392257822_il2cpp_TypeInfo_var);
		BannerFailedToLoadDelegate_t2244784594 * L_1 = ((ADBannerView_t2392257822_StaticFields*)ADBannerView_t2392257822_il2cpp_TypeInfo_var->static_fields)->get_onBannerFailedToLoad_3();
		NullCheck(L_1);
		BannerFailedToLoadDelegate_Invoke_m3262427884(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADBannerView/BannerFailedToLoadDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void BannerFailedToLoadDelegate__ctor_m2629571602 (BannerFailedToLoadDelegate_t2244784594 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerFailedToLoadDelegate::Invoke()
extern "C"  void BannerFailedToLoadDelegate_Invoke_m3262427884 (BannerFailedToLoadDelegate_t2244784594 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		BannerFailedToLoadDelegate_Invoke_m3262427884((BannerFailedToLoadDelegate_t2244784594 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594 (BannerFailedToLoadDelegate_t2244784594 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.iOS.ADBannerView/BannerFailedToLoadDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BannerFailedToLoadDelegate_BeginInvoke_m1073531935 (BannerFailedToLoadDelegate_t2244784594 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerFailedToLoadDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void BannerFailedToLoadDelegate_EndInvoke_m3595591458 (BannerFailedToLoadDelegate_t2244784594 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void BannerWasClickedDelegate__ctor_m1483041906 (BannerWasClickedDelegate_t3510840818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::Invoke()
extern "C"  void BannerWasClickedDelegate_Invoke_m1164690252 (BannerWasClickedDelegate_t3510840818 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		BannerWasClickedDelegate_Invoke_m1164690252((BannerWasClickedDelegate_t3510840818 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818 (BannerWasClickedDelegate_t3510840818 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BannerWasClickedDelegate_BeginInvoke_m2463719615 (BannerWasClickedDelegate_t3510840818 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void BannerWasClickedDelegate_EndInvoke_m2721376130 (BannerWasClickedDelegate_t3510840818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void BannerWasLoadedDelegate__ctor_m74826402 (BannerWasLoadedDelegate_t3604098244 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::Invoke()
extern "C"  void BannerWasLoadedDelegate_Invoke_m3949163900 (BannerWasLoadedDelegate_t3604098244 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		BannerWasLoadedDelegate_Invoke_m3949163900((BannerWasLoadedDelegate_t3604098244 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244 (BannerWasLoadedDelegate_t3604098244 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BannerWasLoadedDelegate_BeginInvoke_m2489699591 (BannerWasLoadedDelegate_t3604098244 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void BannerWasLoadedDelegate_EndInvoke_m3402094002 (BannerWasLoadedDelegate_t3604098244 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::.cctor()
extern "C"  void ADInterstitialAd__cctor_m1224608307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)
extern "C"  void ADInterstitialAd_Native_DestroyInterstitial_m1874896972 (Il2CppObject * __this /* static, unused */, IntPtr_t ___view0, const MethodInfo* method)
{
	typedef void (*ADInterstitialAd_Native_DestroyInterstitial_m1874896972_ftn) (IntPtr_t);
	static ADInterstitialAd_Native_DestroyInterstitial_m1874896972_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ADInterstitialAd_Native_DestroyInterstitial_m1874896972_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)");
	_il2cpp_icall_func(___view0);
}
// System.Void UnityEngine.iOS.ADInterstitialAd::Finalize()
extern Il2CppClass* ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var;
extern const uint32_t ADInterstitialAd_Finalize_m3863008616_MetadataUsageId;
extern "C"  void ADInterstitialAd_Finalize_m3863008616 (ADInterstitialAd_t3393793052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADInterstitialAd_Finalize_m3863008616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = __this->get_interstitialView_0();
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var);
		ADInterstitialAd_Native_DestroyInterstitial_m1874896972(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::FireInterstitialWasLoaded()
extern Il2CppClass* ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var;
extern const uint32_t ADInterstitialAd_FireInterstitialWasLoaded_m1452242756_MetadataUsageId;
extern "C"  void ADInterstitialAd_FireInterstitialWasLoaded_m1452242756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADInterstitialAd_FireInterstitialWasLoaded_m1452242756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var);
		InterstitialWasLoadedDelegate_t3068494210 * L_0 = ((ADInterstitialAd_t3393793052_StaticFields*)ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var->static_fields)->get_onInterstitialWasLoaded_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var);
		InterstitialWasLoadedDelegate_t3068494210 * L_1 = ((ADInterstitialAd_t3393793052_StaticFields*)ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var->static_fields)->get_onInterstitialWasLoaded_1();
		NullCheck(L_1);
		InterstitialWasLoadedDelegate_Invoke_m2428938746(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd::FireInterstitialWasViewed()
extern Il2CppClass* ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var;
extern const uint32_t ADInterstitialAd_FireInterstitialWasViewed_m802485635_MetadataUsageId;
extern "C"  void ADInterstitialAd_FireInterstitialWasViewed_m802485635 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ADInterstitialAd_FireInterstitialWasViewed_m802485635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var);
		InterstitialWasViewedDelegate_t507319425 * L_0 = ((ADInterstitialAd_t3393793052_StaticFields*)ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var->static_fields)->get_onInterstitialWasViewed_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var);
		InterstitialWasViewedDelegate_t507319425 * L_1 = ((ADInterstitialAd_t3393793052_StaticFields*)ADInterstitialAd_t3393793052_il2cpp_TypeInfo_var->static_fields)->get_onInterstitialWasViewed_2();
		NullCheck(L_1);
		InterstitialWasViewedDelegate_Invoke_m3038628409(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void InterstitialWasLoadedDelegate__ctor_m2439394720 (InterstitialWasLoadedDelegate_t3068494210 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::Invoke()
extern "C"  void InterstitialWasLoadedDelegate_Invoke_m2428938746 (InterstitialWasLoadedDelegate_t3068494210 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		InterstitialWasLoadedDelegate_Invoke_m2428938746((InterstitialWasLoadedDelegate_t3068494210 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210 (InterstitialWasLoadedDelegate_t3068494210 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InterstitialWasLoadedDelegate_BeginInvoke_m1425628105 (InterstitialWasLoadedDelegate_t3068494210 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void InterstitialWasLoadedDelegate_EndInvoke_m2339731376 (InterstitialWasLoadedDelegate_t3068494210 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasViewedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void InterstitialWasViewedDelegate__ctor_m2752017695 (InterstitialWasViewedDelegate_t507319425 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasViewedDelegate::Invoke()
extern "C"  void InterstitialWasViewedDelegate_Invoke_m3038628409 (InterstitialWasViewedDelegate_t507319425 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		InterstitialWasViewedDelegate_Invoke_m3038628409((InterstitialWasViewedDelegate_t507319425 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425 (InterstitialWasViewedDelegate_t507319425 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.iOS.ADInterstitialAd/InterstitialWasViewedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InterstitialWasViewedDelegate_BeginInvoke_m3141305002 (InterstitialWasViewedDelegate_t507319425 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.iOS.ADInterstitialAd/InterstitialWasViewedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void InterstitialWasViewedDelegate_EndInvoke_m971591343 (InterstitialWasViewedDelegate_t507319425 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.iOS.LocalNotification::.ctor()
extern "C"  void LocalNotification__ctor_m2361357092 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		LocalNotification_InitWrapper_m910277669(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern Il2CppClass* LocalNotification_t1344855248_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m4000496905_MetadataUsageId;
extern "C"  void LocalNotification__cctor_m4000496905 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m4000496905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m3805233578((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m386468226((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t1344855248_StaticFields*)LocalNotification_t1344855248_il2cpp_TypeInfo_var->static_fields)->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Double UnityEngine.iOS.LocalNotification::GetFireDate()
extern "C"  double LocalNotification_GetFireDate_m3226659871 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef double (*LocalNotification_GetFireDate_m3226659871_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_GetFireDate_m3226659871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_GetFireDate_m3226659871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::GetFireDate()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)
extern "C"  void LocalNotification_SetFireDate_m1553477818 (LocalNotification_t1344855248 * __this, double ___dt0, const MethodInfo* method)
{
	typedef void (*LocalNotification_SetFireDate_m1553477818_ftn) (LocalNotification_t1344855248 *, double);
	static LocalNotification_SetFireDate_m1553477818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_SetFireDate_m1553477818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)");
	_il2cpp_icall_func(__this, ___dt0);
}
// System.DateTime UnityEngine.iOS.LocalNotification::get_fireDate()
extern Il2CppClass* LocalNotification_t1344855248_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification_get_fireDate_m2185968852_MetadataUsageId;
extern "C"  DateTime_t4283661327  LocalNotification_get_fireDate_m2185968852 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification_get_fireDate_m2185968852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = LocalNotification_GetFireDate_m3226659871(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LocalNotification_t1344855248_il2cpp_TypeInfo_var);
		int64_t L_1 = ((LocalNotification_t1344855248_StaticFields*)LocalNotification_t1344855248_il2cpp_TypeInfo_var->static_fields)->get_m_NSReferenceDateTicks_1();
		DateTime_t4283661327  L_2;
		memset(&L_2, 0, sizeof(L_2));
		DateTime__ctor_m4059138700(&L_2, ((int64_t)((int64_t)(((int64_t)((int64_t)((double)((double)L_0*(double)(10000000.0))))))+(int64_t)L_1)), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::set_fireDate(System.DateTime)
extern Il2CppClass* LocalNotification_t1344855248_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification_set_fireDate_m3705017239_MetadataUsageId;
extern "C"  void LocalNotification_set_fireDate_m3705017239 (LocalNotification_t1344855248 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification_set_fireDate_m3705017239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t4283661327  L_0 = DateTime_ToUniversalTime_m691668206((&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		int64_t L_1 = DateTime_get_Ticks_m386468226((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LocalNotification_t1344855248_il2cpp_TypeInfo_var);
		int64_t L_2 = ((LocalNotification_t1344855248_StaticFields*)LocalNotification_t1344855248_il2cpp_TypeInfo_var->static_fields)->get_m_NSReferenceDateTicks_1();
		LocalNotification_SetFireDate_m1553477818(__this, ((double)((double)(((double)((double)((int64_t)((int64_t)L_1-(int64_t)L_2)))))/(double)(10000000.0))), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.iOS.LocalNotification::get_timeZone()
extern "C"  String_t* LocalNotification_get_timeZone_m2940284639 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_timeZone_m2940284639_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_timeZone_m2940284639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_timeZone_m2940284639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_timeZone()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_timeZone(System.String)
extern "C"  void LocalNotification_set_timeZone_m2670784108 (LocalNotification_t1344855248 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_timeZone_m2670784108_ftn) (LocalNotification_t1344855248 *, String_t*);
	static LocalNotification_set_timeZone_m2670784108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_timeZone_m2670784108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_timeZone(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.iOS.CalendarUnit UnityEngine.iOS.LocalNotification::get_repeatInterval()
extern "C"  int32_t LocalNotification_get_repeatInterval_m2938542676 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_repeatInterval_m2938542676_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_repeatInterval_m2938542676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_repeatInterval_m2938542676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_repeatInterval()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)
extern "C"  void LocalNotification_set_repeatInterval_m2284412131 (LocalNotification_t1344855248 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_repeatInterval_m2284412131_ftn) (LocalNotification_t1344855248 *, int32_t);
	static LocalNotification_set_repeatInterval_m2284412131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_repeatInterval_m2284412131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.iOS.CalendarIdentifier UnityEngine.iOS.LocalNotification::get_repeatCalendar()
extern "C"  int32_t LocalNotification_get_repeatCalendar_m916891826 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_repeatCalendar_m916891826_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_repeatCalendar_m916891826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_repeatCalendar_m916891826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_repeatCalendar()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)
extern "C"  void LocalNotification_set_repeatCalendar_m599340503 (LocalNotification_t1344855248 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_repeatCalendar_m599340503_ftn) (LocalNotification_t1344855248 *, int32_t);
	static LocalNotification_set_repeatCalendar_m599340503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_repeatCalendar_m599340503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertBody()
extern "C"  String_t* LocalNotification_get_alertBody_m3216658234 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertBody_m3216658234_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_alertBody_m3216658234_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertBody_m3216658234_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertBody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertBody(System.String)
extern "C"  void LocalNotification_set_alertBody_m3334455103 (LocalNotification_t1344855248 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertBody_m3334455103_ftn) (LocalNotification_t1344855248 *, String_t*);
	static LocalNotification_set_alertBody_m3334455103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertBody_m3334455103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertBody(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertAction()
extern "C"  String_t* LocalNotification_get_alertAction_m4064597262 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertAction_m4064597262_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_alertAction_m4064597262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertAction_m4064597262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertAction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertAction(System.String)
extern "C"  void LocalNotification_set_alertAction_m860676139 (LocalNotification_t1344855248 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertAction_m860676139_ftn) (LocalNotification_t1344855248 *, String_t*);
	static LocalNotification_set_alertAction_m860676139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertAction_m860676139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertAction(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.iOS.LocalNotification::get_hasAction()
extern "C"  bool LocalNotification_get_hasAction_m3802604239 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef bool (*LocalNotification_get_hasAction_m3802604239_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_hasAction_m3802604239_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_hasAction_m3802604239_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_hasAction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)
extern "C"  void LocalNotification_set_hasAction_m3331895596 (LocalNotification_t1344855248 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_hasAction_m3331895596_ftn) (LocalNotification_t1344855248 *, bool);
	static LocalNotification_set_hasAction_m3331895596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_hasAction_m3331895596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_alertLaunchImage()
extern "C"  String_t* LocalNotification_get_alertLaunchImage_m3685756786 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_alertLaunchImage_m3685756786_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_alertLaunchImage_m3685756786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_alertLaunchImage_m3685756786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_alertLaunchImage()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)
extern "C"  void LocalNotification_set_alertLaunchImage_m2289744825 (LocalNotification_t1344855248 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_alertLaunchImage_m2289744825_ftn) (LocalNotification_t1344855248 *, String_t*);
	static LocalNotification_set_alertLaunchImage_m2289744825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_alertLaunchImage_m2289744825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()
extern "C"  int32_t LocalNotification_get_applicationIconBadgeNumber_m598051564 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef int32_t (*LocalNotification_get_applicationIconBadgeNumber_m598051564_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_applicationIconBadgeNumber_m598051564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_applicationIconBadgeNumber_m598051564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)
extern "C"  void LocalNotification_set_applicationIconBadgeNumber_m4106546993 (LocalNotification_t1344855248 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_applicationIconBadgeNumber_m4106546993_ftn) (LocalNotification_t1344855248 *, int32_t);
	static LocalNotification_set_applicationIconBadgeNumber_m4106546993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_applicationIconBadgeNumber_m4106546993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_soundName()
extern "C"  String_t* LocalNotification_get_soundName_m2126669654 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_soundName_m2126669654_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_soundName_m2126669654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_soundName_m2126669654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_soundName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_soundName(System.String)
extern "C"  void LocalNotification_set_soundName_m870364323 (LocalNotification_t1344855248 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_soundName_m870364323_ftn) (LocalNotification_t1344855248 *, String_t*);
	static LocalNotification_set_soundName_m870364323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_soundName_m870364323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_soundName(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.iOS.LocalNotification::get_defaultSoundName()
extern "C"  String_t* LocalNotification_get_defaultSoundName_m2358493375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*LocalNotification_get_defaultSoundName_m2358493375_ftn) ();
	static LocalNotification_get_defaultSoundName_m2358493375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_defaultSoundName_m2358493375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_defaultSoundName()");
	return _il2cpp_icall_func();
}
// System.Collections.IDictionary UnityEngine.iOS.LocalNotification::get_userInfo()
extern "C"  Il2CppObject * LocalNotification_get_userInfo_m4128471802 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef Il2CppObject * (*LocalNotification_get_userInfo_m4128471802_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_get_userInfo_m4128471802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_get_userInfo_m4128471802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::get_userInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)
extern "C"  void LocalNotification_set_userInfo_m3850714959 (LocalNotification_t1344855248 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	typedef void (*LocalNotification_set_userInfo_m3850714959_ftn) (LocalNotification_t1344855248 *, Il2CppObject *);
	static LocalNotification_set_userInfo_m3850714959_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_set_userInfo_m3850714959_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m3155596668 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef void (*LocalNotification_Destroy_m3155596668_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_Destroy_m3155596668_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m3155596668_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m22293182 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m3155596668(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::InitWrapper()
extern "C"  void LocalNotification_InitWrapper_m910277669 (LocalNotification_t1344855248 * __this, const MethodInfo* method)
{
	typedef void (*LocalNotification_InitWrapper_m910277669_ftn) (LocalNotification_t1344855248 *);
	static LocalNotification_InitWrapper_m910277669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_InitWrapper_m910277669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::InitWrapper()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::.ctor()
extern "C"  void RemoteNotification__ctor_m56993883 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.iOS.RemoteNotification::get_alertBody()
extern "C"  String_t* RemoteNotification_get_alertBody_m3725360779 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef String_t* (*RemoteNotification_get_alertBody_m3725360779_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_get_alertBody_m3725360779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_alertBody_m3725360779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_alertBody()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.iOS.RemoteNotification::get_hasAction()
extern "C"  bool RemoteNotification_get_hasAction_m2262891258 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef bool (*RemoteNotification_get_hasAction_m2262891258_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_get_hasAction_m2262891258_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_hasAction_m2262891258_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_hasAction()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()
extern "C"  int32_t RemoteNotification_get_applicationIconBadgeNumber_m1135788693 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef int32_t (*RemoteNotification_get_applicationIconBadgeNumber_m1135788693_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_get_applicationIconBadgeNumber_m1135788693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_applicationIconBadgeNumber_m1135788693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.iOS.RemoteNotification::get_soundName()
extern "C"  String_t* RemoteNotification_get_soundName_m2635372199 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef String_t* (*RemoteNotification_get_soundName_m2635372199_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_get_soundName_m2635372199_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_soundName_m2635372199_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_soundName()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.IDictionary UnityEngine.iOS.RemoteNotification::get_userInfo()
extern "C"  Il2CppObject * RemoteNotification_get_userInfo_m4136631987 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef Il2CppObject * (*RemoteNotification_get_userInfo_m4136631987_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_get_userInfo_m4136631987_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_get_userInfo_m4136631987_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::get_userInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m570710259 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	typedef void (*RemoteNotification_Destroy_m570710259_ftn) (RemoteNotification_t1652317979 *);
	static RemoteNotification_Destroy_m570710259_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m570710259_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m1495193127 (RemoteNotification_t1652317979 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m570710259(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2655645489 (Keyframe_t4079056114 * __this, float ___time0, float ___value1, const MethodInfo* method)
{
	{
		float L_0 = ___time0;
		__this->set_m_Time_0(L_0);
		float L_1 = ___value1;
		__this->set_m_Value_1(L_1);
		__this->set_m_InTangent_2((0.0f));
		__this->set_m_OutTangent_3((0.0f));
		return;
	}
}
extern "C"  void Keyframe__ctor_m2655645489_AdjustorThunk (Il2CppObject * __this, float ___time0, float ___value1, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe__ctor_m2655645489(_thisAdjusted, ___time0, ___value1, method);
}
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Keyframe__ctor_m3412708539 (Keyframe_t4079056114 * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const MethodInfo* method)
{
	{
		float L_0 = ___time0;
		__this->set_m_Time_0(L_0);
		float L_1 = ___value1;
		__this->set_m_Value_1(L_1);
		float L_2 = ___inTangent2;
		__this->set_m_InTangent_2(L_2);
		float L_3 = ___outTangent3;
		__this->set_m_OutTangent_3(L_3);
		return;
	}
}
extern "C"  void Keyframe__ctor_m3412708539_AdjustorThunk (Il2CppObject * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe__ctor_m3412708539(_thisAdjusted, ___time0, ___value1, ___inTangent2, ___outTangent3, method);
}
// System.Single UnityEngine.Keyframe::get_time()
extern "C"  float Keyframe_get_time_m1367974951 (Keyframe_t4079056114 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Time_0();
		return L_0;
	}
}
extern "C"  float Keyframe_get_time_m1367974951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	return Keyframe_get_time_m1367974951(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_time(System.Single)
extern "C"  void Keyframe_set_time_m2674731900 (Keyframe_t4079056114 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_time_m2674731900_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe_set_time_m2674731900(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_value()
extern "C"  float Keyframe_get_value_m1003136441 (Keyframe_t4079056114 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Value_1();
		return L_0;
	}
}
extern "C"  float Keyframe_get_value_m1003136441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	return Keyframe_get_value_m1003136441(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_value(System.Single)
extern "C"  void Keyframe_set_value_m2735107322 (Keyframe_t4079056114 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Value_1(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_value_m2735107322_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe_set_value_m2735107322(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_inTangent()
extern "C"  float Keyframe_get_inTangent_m2236973672 (Keyframe_t4079056114 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_InTangent_2();
		return L_0;
	}
}
extern "C"  float Keyframe_get_inTangent_m2236973672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	return Keyframe_get_inTangent_m2236973672(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_inTangent(System.Single)
extern "C"  void Keyframe_set_inTangent_m1264152747 (Keyframe_t4079056114 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_InTangent_2(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_inTangent_m1264152747_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe_set_inTangent_m1264152747(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Keyframe::get_outTangent()
extern "C"  float Keyframe_get_outTangent_m2520753969 (Keyframe_t4079056114 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_OutTangent_3();
		return L_0;
	}
}
extern "C"  float Keyframe_get_outTangent_m2520753969_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	return Keyframe_get_outTangent_m2520753969(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_outTangent(System.Single)
extern "C"  void Keyframe_set_outTangent_m4071134898 (Keyframe_t4079056114 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_OutTangent_3(L_0);
		return;
	}
}
extern "C"  void Keyframe_set_outTangent_m4071134898_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe_set_outTangent_m4071134898(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.Keyframe::get_tangentMode()
extern "C"  int32_t Keyframe_get_tangentMode_m1245220572 (Keyframe_t4079056114 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
extern "C"  int32_t Keyframe_get_tangentMode_m1245220572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	return Keyframe_get_tangentMode_m1245220572(_thisAdjusted, method);
}
// System.Void UnityEngine.Keyframe::set_tangentMode(System.Int32)
extern "C"  void Keyframe_set_tangentMode_m2444824761 (Keyframe_t4079056114 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void Keyframe_set_tangentMode_m2444824761_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Keyframe_t4079056114 * _thisAdjusted = reinterpret_cast<Keyframe_t4079056114 *>(__this + 1);
	Keyframe_set_tangentMode_m2444824761(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: UnityEngine.Keyframe
extern "C" void Keyframe_t4079056114_marshal_pinvoke(const Keyframe_t4079056114& unmarshaled, Keyframe_t4079056114_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Time_0 = unmarshaled.get_m_Time_0();
	marshaled.___m_Value_1 = unmarshaled.get_m_Value_1();
	marshaled.___m_InTangent_2 = unmarshaled.get_m_InTangent_2();
	marshaled.___m_OutTangent_3 = unmarshaled.get_m_OutTangent_3();
}
extern "C" void Keyframe_t4079056114_marshal_pinvoke_back(const Keyframe_t4079056114_marshaled_pinvoke& marshaled, Keyframe_t4079056114& unmarshaled)
{
	float unmarshaled_m_Time_temp_0 = 0.0f;
	unmarshaled_m_Time_temp_0 = marshaled.___m_Time_0;
	unmarshaled.set_m_Time_0(unmarshaled_m_Time_temp_0);
	float unmarshaled_m_Value_temp_1 = 0.0f;
	unmarshaled_m_Value_temp_1 = marshaled.___m_Value_1;
	unmarshaled.set_m_Value_1(unmarshaled_m_Value_temp_1);
	float unmarshaled_m_InTangent_temp_2 = 0.0f;
	unmarshaled_m_InTangent_temp_2 = marshaled.___m_InTangent_2;
	unmarshaled.set_m_InTangent_2(unmarshaled_m_InTangent_temp_2);
	float unmarshaled_m_OutTangent_temp_3 = 0.0f;
	unmarshaled_m_OutTangent_temp_3 = marshaled.___m_OutTangent_3;
	unmarshaled.set_m_OutTangent_3(unmarshaled_m_OutTangent_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Keyframe
extern "C" void Keyframe_t4079056114_marshal_pinvoke_cleanup(Keyframe_t4079056114_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Keyframe
extern "C" void Keyframe_t4079056114_marshal_com(const Keyframe_t4079056114& unmarshaled, Keyframe_t4079056114_marshaled_com& marshaled)
{
	marshaled.___m_Time_0 = unmarshaled.get_m_Time_0();
	marshaled.___m_Value_1 = unmarshaled.get_m_Value_1();
	marshaled.___m_InTangent_2 = unmarshaled.get_m_InTangent_2();
	marshaled.___m_OutTangent_3 = unmarshaled.get_m_OutTangent_3();
}
extern "C" void Keyframe_t4079056114_marshal_com_back(const Keyframe_t4079056114_marshaled_com& marshaled, Keyframe_t4079056114& unmarshaled)
{
	float unmarshaled_m_Time_temp_0 = 0.0f;
	unmarshaled_m_Time_temp_0 = marshaled.___m_Time_0;
	unmarshaled.set_m_Time_0(unmarshaled_m_Time_temp_0);
	float unmarshaled_m_Value_temp_1 = 0.0f;
	unmarshaled_m_Value_temp_1 = marshaled.___m_Value_1;
	unmarshaled.set_m_Value_1(unmarshaled_m_Value_temp_1);
	float unmarshaled_m_InTangent_temp_2 = 0.0f;
	unmarshaled_m_InTangent_temp_2 = marshaled.___m_InTangent_2;
	unmarshaled.set_m_InTangent_2(unmarshaled_m_InTangent_temp_2);
	float unmarshaled_m_OutTangent_temp_3 = 0.0f;
	unmarshaled_m_OutTangent_temp_3 = marshaled.___m_OutTangent_3;
	unmarshaled.set_m_OutTangent_3(unmarshaled_m_OutTangent_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Keyframe
extern "C" void Keyframe_t4079056114_marshal_com_cleanup(Keyframe_t4079056114_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m654679389 (Logger_t2997509588 * __this, Il2CppObject * ___logHandler0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___logHandler0;
		Logger_set_logHandler_m2661029584(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m1919624760(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m3244716167(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  Il2CppObject * Logger_get_logHandler_m2576399569 (Logger_t2997509588 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m2661029584 (Logger_t2997509588 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m3803347955 (Logger_t2997509588 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m1919624760 (Logger_t2997509588 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m1512373196 (Logger_t2997509588 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m3244716167 (Logger_t2997509588 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m3362907658 (Logger_t2997509588 * __this, int32_t ___logType0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = Logger_get_logEnabled_m3803347955(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = ___logType0;
		int32_t L_2 = Logger_get_filterLogType_m1512373196(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_3 = ___logType0;
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)4))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B4_0;
	}

IL_001f:
	{
		return (bool)0;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern Il2CppCodeGenString* _stringLiteral2439591;
extern const uint32_t Logger_GetString_m3033796341_MetadataUsageId;
extern "C"  String_t* Logger_GetString_m3033796341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m3033796341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral2439591;
	}

IL_0016:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t2265139045_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral119816;
extern const uint32_t Logger_Log_m3875808634_MetadataUsageId;
extern "C"  void Logger_Log_m3875808634 (Logger_t2997509588 * __this, int32_t ___logType0, Il2CppObject * ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m3875808634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3362907658(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m2576399569(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m3033796341(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3071478659 *, String_t*, ObjectU5BU5D_t1108656482* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2265139045_il2cpp_TypeInfo_var, L_2, L_3, (Object_t3071478659 *)NULL, _stringLiteral119816, L_4);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t2265139045_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral119816;
extern const uint32_t Logger_Log_m3185878806_MetadataUsageId;
extern "C"  void Logger_Log_m3185878806 (Logger_t2997509588 * __this, int32_t ___logType0, Il2CppObject * ___message1, Object_t3071478659 * ___context2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m3185878806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3362907658(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m2576399569(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t3071478659 * L_4 = ___context2;
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_6 = ___message1;
		String_t* L_7 = Logger_GetString_m3033796341(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3071478659 *, String_t*, ObjectU5BU5D_t1108656482* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2265139045_il2cpp_TypeInfo_var, L_2, L_3, L_4, _stringLiteral119816, L_5);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* ILogHandler_t2265139045_il2cpp_TypeInfo_var;
extern const uint32_t Logger_LogFormat_m4050017187_MetadataUsageId;
extern "C"  void Logger_LogFormat_m4050017187 (Logger_t2997509588 * __this, int32_t ___logType0, Object_t3071478659 * ___context1, String_t* ___format2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m4050017187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3362907658(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m2576399569(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t3071478659 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t1108656482* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3071478659 *, String_t*, ObjectU5BU5D_t1108656482* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2265139045_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C"  void Material__ctor_m2685909642 (Material_t3870600107 * __this, Shader_t3191267369 * ___shader0, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Shader_t3191267369 * L_0 = ___shader0;
		Material_Internal_CreateWithShader_m701341915(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m2546967560 (Material_t3870600107 * __this, Material_t3870600107 * ___source0, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Material_t3870600107 * L_0 = ___source0;
		Material_Internal_CreateWithMaterial_m2349411671(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t Material_set_mainTexture_m3116438437_MetadataUsageId;
extern "C"  void Material_set_mainTexture_m3116438437 (Material_t3870600107 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_mainTexture_m3116438437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2526458961 * L_0 = ___value0;
		Material_SetTexture_m1833724755(__this, _stringLiteral558922319, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m1918430019 (Material_t3870600107 * __this, String_t* ___propertyName0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t4194546905  L_2 = ___color1;
		Material_SetColor_m54957808(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m54957808 (Material_t3870600107 * __this, int32_t ___nameID0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetColor_m3209011477(NULL /*static, unused*/, __this, L_0, (&___color1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColor_m3209011477 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___self0, int32_t ___nameID1, Color_t4194546905 * ___color2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m3209011477_ftn) (Material_t3870600107 *, int32_t, Color_t4194546905 *);
	static Material_INTERNAL_CALL_SetColor_m3209011477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m3209011477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___color2);
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1833724755 (Material_t3870600107 * __this, String_t* ___propertyName0, Texture_t2526458961 * ___texture1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2526458961 * L_2 = ___texture1;
		Material_SetTexture_m3847256752(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m3847256752 (Material_t3870600107 * __this, int32_t ___nameID0, Texture_t2526458961 * ___texture1, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m3847256752_ftn) (Material_t3870600107 *, int32_t, Texture_t2526458961 *);
	static Material_SetTexture_m3847256752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m3847256752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID0, ___texture1);
}
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m4241824642 (Material_t3870600107 * __this, int32_t ___pass0, const MethodInfo* method)
{
	typedef bool (*Material_SetPass_m4241824642_ftn) (Material_t3870600107 *, int32_t);
	static Material_SetPass_m4241824642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetPass_m4241824642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetPass(System.Int32)");
	return _il2cpp_icall_func(__this, ___pass0);
}
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern "C"  void Material_Internal_CreateWithShader_m701341915 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mono0, Shader_t3191267369 * ___shader1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithShader_m701341915_ftn) (Material_t3870600107 *, Shader_t3191267369 *);
	static Material_Internal_CreateWithShader_m701341915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithShader_m701341915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)");
	_il2cpp_icall_func(___mono0, ___shader1);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m2349411671 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mono0, Material_t3870600107 * ___source1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m2349411671_ftn) (Material_t3870600107 *, Material_t3870600107 *);
	static Material_Internal_CreateWithMaterial_m2349411671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m2349411671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono0, ___source1);
}
// System.Void UnityEngine.Mathf::.cctor()
extern Il2CppClass* MathfInternal_t4096243933_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m1875403730_MetadataUsageId;
extern "C"  void Mathf__cctor_m1875403730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m1875403730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Single UnityEngine.Mathf::Tan(System.Single)
extern "C"  float Mathf_Tan_m3075991205 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = tan((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Acos(System.Single)
extern "C"  float Mathf_Acos_m3432578408 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = acos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Atan(System.Single)
extern "C"  float Mathf_Atan_m153454606 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = atan((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern "C"  float Mathf_Atan2_m3138013817 (Il2CppObject * __this /* static, unused */, float ___y0, float ___x1, const MethodInfo* method)
{
	{
		float L_0 = ___y0;
		float L_1 = ___x1;
		double L_2 = atan2((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C"  float Mathf_Sqrt_m3592270478 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m3641135540 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m2322067385 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2413438171 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3923796455 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single[])
extern "C"  float Mathf_Max_m2571079968 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___values0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (0.0f);
	}

IL_0010:
	{
		SingleU5BU5D_t2316563989* L_2 = ___values0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		float L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		V_2 = 1;
		goto IL_002c;
	}

IL_001b:
	{
		SingleU5BU5D_t2316563989* L_5 = ___values0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		float L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		float L_9 = V_1;
		if ((!(((float)L_8) > ((float)L_9))))
		{
			goto IL_0028;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_10 = ___values0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		float L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
	}

IL_0028:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_001b;
		}
	}
	{
		float L_17 = V_1;
		return L_17;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m2911193737 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Ceil(System.Single)
extern "C"  float Mathf_Ceil_m3793305609 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C"  float Mathf_Round_m2677810712 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m3163545820 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m4040614993 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3872743893 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min1;
		___value0 = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value0;
		float L_4 = ___max2;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max2;
		___value0 = L_5;
	}

IL_0019:
	{
		float L_6 = ___value0;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2272733930 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Lerp_m3257777633_MetadataUsageId;
extern "C"  float Mathf_Lerp_m3257777633 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Lerp_m3257777633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Approximately_m1395529776_MetadataUsageId;
extern "C"  bool Mathf_Approximately_m1395529776 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1395529776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		float L_9 = Mathf_Max_m3923796455(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_pinvoke(const Mathf_t4203372500& unmarshaled, Mathf_t4203372500_marshaled_pinvoke& marshaled)
{
}
extern "C" void Mathf_t4203372500_marshal_pinvoke_back(const Mathf_t4203372500_marshaled_pinvoke& marshaled, Mathf_t4203372500& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_pinvoke_cleanup(Mathf_t4203372500_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_com(const Mathf_t4203372500& unmarshaled, Mathf_t4203372500_marshaled_com& marshaled)
{
}
extern "C" void Mathf_t4203372500_marshal_com_back(const Mathf_t4203372500_marshaled_com& marshaled, Mathf_t4203372500& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_com_cleanup(Mathf_t4203372500_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m2279862332 (Matrix4x4_t1651859333 * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = Matrix4x4_get_Item_m1280478331(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float Matrix4x4_get_Item_m2279862332_AdjustorThunk (Il2CppObject * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_Item_m2279862332(_thisAdjusted, ___row0, ___column1, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m2343951137 (Matrix4x4_t1651859333 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = ___value2;
		Matrix4x4_set_Item_m3979676448(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m2343951137_AdjustorThunk (Il2CppObject * __this, int32_t ___row0, int32_t ___column1, float ___value2, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	Matrix4x4_set_Item_m2343951137(_thisAdjusted, ___row0, ___column1, ___value2, method);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3642945605;
extern const uint32_t Matrix4x4_get_Item_m1280478331_MetadataUsageId;
extern "C"  float Matrix4x4_get_Item_m1280478331 (Matrix4x4_t1651859333 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_Item_m1280478331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = __this->get_m00_0();
		return L_2;
	}

IL_0054:
	{
		float L_3 = __this->get_m10_1();
		return L_3;
	}

IL_005b:
	{
		float L_4 = __this->get_m20_2();
		return L_4;
	}

IL_0062:
	{
		float L_5 = __this->get_m30_3();
		return L_5;
	}

IL_0069:
	{
		float L_6 = __this->get_m01_4();
		return L_6;
	}

IL_0070:
	{
		float L_7 = __this->get_m11_5();
		return L_7;
	}

IL_0077:
	{
		float L_8 = __this->get_m21_6();
		return L_8;
	}

IL_007e:
	{
		float L_9 = __this->get_m31_7();
		return L_9;
	}

IL_0085:
	{
		float L_10 = __this->get_m02_8();
		return L_10;
	}

IL_008c:
	{
		float L_11 = __this->get_m12_9();
		return L_11;
	}

IL_0093:
	{
		float L_12 = __this->get_m22_10();
		return L_12;
	}

IL_009a:
	{
		float L_13 = __this->get_m32_11();
		return L_13;
	}

IL_00a1:
	{
		float L_14 = __this->get_m03_12();
		return L_14;
	}

IL_00a8:
	{
		float L_15 = __this->get_m13_13();
		return L_15;
	}

IL_00af:
	{
		float L_16 = __this->get_m23_14();
		return L_16;
	}

IL_00b6:
	{
		float L_17 = __this->get_m33_15();
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t3456360697 * L_18 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_18, _stringLiteral3642945605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}
}
extern "C"  float Matrix4x4_get_Item_m1280478331_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_Item_m1280478331(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3642945605;
extern const uint32_t Matrix4x4_set_Item_m3979676448_MetadataUsageId;
extern "C"  void Matrix4x4_set_Item_m3979676448 (Matrix4x4_t1651859333 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_set_Item_m3979676448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value1;
		__this->set_m00_0(L_2);
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value1;
		__this->set_m10_1(L_3);
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value1;
		__this->set_m20_2(L_4);
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value1;
		__this->set_m30_3(L_5);
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value1;
		__this->set_m01_4(L_6);
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value1;
		__this->set_m11_5(L_7);
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value1;
		__this->set_m21_6(L_8);
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value1;
		__this->set_m31_7(L_9);
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value1;
		__this->set_m02_8(L_10);
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value1;
		__this->set_m12_9(L_11);
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value1;
		__this->set_m22_10(L_12);
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value1;
		__this->set_m32_11(L_13);
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value1;
		__this->set_m03_12(L_14);
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value1;
		__this->set_m13_13(L_15);
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value1;
		__this->set_m23_14(L_16);
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value1;
		__this->set_m33_15(L_17);
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t3456360697 * L_18 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_18, _stringLiteral3642945605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0118:
	{
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m3979676448_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	Matrix4x4_set_Item_m3979676448(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m4083477721 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector4_t4282066567  L_0 = Matrix4x4_GetColumn_m786071102(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3402333527((&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_2 = Matrix4x4_GetColumn_m786071102(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m3402333527((&V_1), /*hidden argument*/NULL);
		Vector4_t4282066567  L_4 = Matrix4x4_GetColumn_m786071102(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m3402333527((&V_2), /*hidden argument*/NULL);
		Vector4_t4282066567  L_6 = Matrix4x4_GetColumn_m786071102(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m3402333527((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m4083477721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_GetHashCode_m4083477721(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_Equals_m3534208385_MetadataUsageId;
extern "C"  bool Matrix4x4_Equals_m3534208385 (Matrix4x4_t1651859333 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m3534208385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t4282066567  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Matrix4x4_t1651859333_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Matrix4x4_t1651859333 *)((Matrix4x4_t1651859333 *)UnBox (L_1, Matrix4x4_t1651859333_il2cpp_TypeInfo_var))));
		Vector4_t4282066567  L_2 = Matrix4x4_GetColumn_m786071102(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t4282066567  L_3 = Matrix4x4_GetColumn_m786071102((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t4282066567  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m3270185343((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_7 = Matrix4x4_GetColumn_m786071102(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t4282066567  L_8 = Matrix4x4_GetColumn_m786071102((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t4282066567  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m3270185343((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_12 = Matrix4x4_GetColumn_m786071102(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t4282066567  L_13 = Matrix4x4_GetColumn_m786071102((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t4282066567  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m3270185343((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_17 = Matrix4x4_GetColumn_m786071102(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t4282066567  L_18 = Matrix4x4_GetColumn_m786071102((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t4282066567  L_19 = L_18;
		Il2CppObject * L_20 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m3270185343((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Matrix4x4_Equals_m3534208385_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_Equals_m3534208385(_thisAdjusted, ___other0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1651859333  Matrix4x4_Inverse_m1483646919 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___m0, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_INTERNAL_CALL_Inverse_m362567117(NULL /*static, unused*/, (&___m0), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_Inverse_m362567117 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___m0, Matrix4x4_t1651859333 * ___value1, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn) (Matrix4x4_t1651859333 *, Matrix4x4_t1651859333 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___m0, ___value1);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_inverse_m2596073482 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t1651859333  L_0 = Matrix4x4_Inverse_m1483646919(NULL /*static, unused*/, (*(Matrix4x4_t1651859333 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_inverse_m2596073482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_inverse_m2596073482(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t4282066567  Matrix4x4_GetColumn_m786071102 (Matrix4x4_t1651859333 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		float L_1 = Matrix4x4_get_Item_m2279862332(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i0;
		float L_3 = Matrix4x4_get_Item_m2279862332(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i0;
		float L_5 = Matrix4x4_get_Item_m2279862332(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i0;
		float L_7 = Matrix4x4_get_Item_m2279862332(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t4282066567  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m2441427762(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Vector4_t4282066567  Matrix4x4_GetColumn_m786071102_AdjustorThunk (Il2CppObject * __this, int32_t ___i0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_GetColumn_m786071102(_thisAdjusted, ___i0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_get_zero_m1808471152_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_zero_m1808471152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_zero_m1808471152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m00_0((0.0f));
		(&V_0)->set_m01_4((0.0f));
		(&V_0)->set_m02_8((0.0f));
		(&V_0)->set_m03_12((0.0f));
		(&V_0)->set_m10_1((0.0f));
		(&V_0)->set_m11_5((0.0f));
		(&V_0)->set_m12_9((0.0f));
		(&V_0)->set_m13_13((0.0f));
		(&V_0)->set_m20_2((0.0f));
		(&V_0)->set_m21_6((0.0f));
		(&V_0)->set_m22_10((0.0f));
		(&V_0)->set_m23_14((0.0f));
		(&V_0)->set_m30_3((0.0f));
		(&V_0)->set_m31_7((0.0f));
		(&V_0)->set_m32_11((0.0f));
		(&V_0)->set_m33_15((0.0f));
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_get_identity_m3946683782_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_identity_m3946683782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m3946683782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m00_0((1.0f));
		(&V_0)->set_m01_4((0.0f));
		(&V_0)->set_m02_8((0.0f));
		(&V_0)->set_m03_12((0.0f));
		(&V_0)->set_m10_1((0.0f));
		(&V_0)->set_m11_5((1.0f));
		(&V_0)->set_m12_9((0.0f));
		(&V_0)->set_m13_13((0.0f));
		(&V_0)->set_m20_2((0.0f));
		(&V_0)->set_m21_6((0.0f));
		(&V_0)->set_m22_10((1.0f));
		(&V_0)->set_m23_14((0.0f));
		(&V_0)->set_m30_3((0.0f));
		(&V_0)->set_m31_7((0.0f));
		(&V_0)->set_m32_11((0.0f));
		(&V_0)->set_m33_15((1.0f));
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t1651859333  Matrix4x4_TRS_m3596398659 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___q1, Vector3_t4282066566  ___s2, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_INTERNAL_CALL_TRS_m1769439979(NULL /*static, unused*/, (&___pos0), (&___q1), (&___s2), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m1769439979 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___pos0, Quaternion_t1553702882 * ___q1, Vector3_t4282066566 * ___s2, Matrix4x4_t1651859333 * ___value3, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *, Vector3_t4282066566 *, Matrix4x4_t1651859333 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___pos0, ___q1, ___s2, ___value3);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4122729682;
extern const uint32_t Matrix4x4_ToString_m294134723_MetadataUsageId;
extern "C"  String_t* Matrix4x4_ToString_m294134723 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m294134723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		Il2CppObject * L_19 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_19);
		ObjectU5BU5D_t1108656482* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		Il2CppObject * L_23 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_23);
		ObjectU5BU5D_t1108656482* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		Il2CppObject * L_31 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_31);
		ObjectU5BU5D_t1108656482* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		Il2CppObject * L_35 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_35);
		ObjectU5BU5D_t1108656482* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_39);
		ObjectU5BU5D_t1108656482* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		Il2CppObject * L_51 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)L_51);
		ObjectU5BU5D_t1108656482* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		Il2CppObject * L_59 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)L_59);
		ObjectU5BU5D_t1108656482* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		Il2CppObject * L_63 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_63);
		String_t* L_64 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral4122729682, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m294134723_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_ToString_m294134723(_thisAdjusted, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  Matrix4x4_t1651859333  Matrix4x4_Ortho_m3444991721 (Il2CppObject * __this /* static, unused */, float ___left0, float ___right1, float ___bottom2, float ___top3, float ___zNear4, float ___zFar5, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___left0;
		float L_1 = ___right1;
		float L_2 = ___bottom2;
		float L_3 = ___top3;
		float L_4 = ___zNear4;
		float L_5 = ___zFar5;
		Matrix4x4_INTERNAL_CALL_Ortho_m2253237141(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_Ortho_m2253237141 (Il2CppObject * __this /* static, unused */, float ___left0, float ___right1, float ___bottom2, float ___top3, float ___zNear4, float ___zFar5, Matrix4x4_t1651859333 * ___value6, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_Ortho_m2253237141_ftn) (float, float, float, float, float, float, Matrix4x4_t1651859333 *);
	static Matrix4x4_INTERNAL_CALL_Ortho_m2253237141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Ortho_m2253237141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___left0, ___right1, ___bottom2, ___top3, ___zNear4, ___zFar5, ___value6);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_op_Multiply_m4108203689_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_op_Multiply_m4108203689 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___lhs0, Matrix4x4_t1651859333  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_op_Multiply_m4108203689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = (&___lhs0)->get_m00_0();
		float L_1 = (&___rhs1)->get_m00_0();
		float L_2 = (&___lhs0)->get_m01_4();
		float L_3 = (&___rhs1)->get_m10_1();
		float L_4 = (&___lhs0)->get_m02_8();
		float L_5 = (&___rhs1)->get_m20_2();
		float L_6 = (&___lhs0)->get_m03_12();
		float L_7 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m00_0(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7)))));
		float L_8 = (&___lhs0)->get_m00_0();
		float L_9 = (&___rhs1)->get_m01_4();
		float L_10 = (&___lhs0)->get_m01_4();
		float L_11 = (&___rhs1)->get_m11_5();
		float L_12 = (&___lhs0)->get_m02_8();
		float L_13 = (&___rhs1)->get_m21_6();
		float L_14 = (&___lhs0)->get_m03_12();
		float L_15 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m01_4(((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15)))));
		float L_16 = (&___lhs0)->get_m00_0();
		float L_17 = (&___rhs1)->get_m02_8();
		float L_18 = (&___lhs0)->get_m01_4();
		float L_19 = (&___rhs1)->get_m12_9();
		float L_20 = (&___lhs0)->get_m02_8();
		float L_21 = (&___rhs1)->get_m22_10();
		float L_22 = (&___lhs0)->get_m03_12();
		float L_23 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m02_8(((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = (&___lhs0)->get_m00_0();
		float L_25 = (&___rhs1)->get_m03_12();
		float L_26 = (&___lhs0)->get_m01_4();
		float L_27 = (&___rhs1)->get_m13_13();
		float L_28 = (&___lhs0)->get_m02_8();
		float L_29 = (&___rhs1)->get_m23_14();
		float L_30 = (&___lhs0)->get_m03_12();
		float L_31 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m03_12(((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31)))));
		float L_32 = (&___lhs0)->get_m10_1();
		float L_33 = (&___rhs1)->get_m00_0();
		float L_34 = (&___lhs0)->get_m11_5();
		float L_35 = (&___rhs1)->get_m10_1();
		float L_36 = (&___lhs0)->get_m12_9();
		float L_37 = (&___rhs1)->get_m20_2();
		float L_38 = (&___lhs0)->get_m13_13();
		float L_39 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m10_1(((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39)))));
		float L_40 = (&___lhs0)->get_m10_1();
		float L_41 = (&___rhs1)->get_m01_4();
		float L_42 = (&___lhs0)->get_m11_5();
		float L_43 = (&___rhs1)->get_m11_5();
		float L_44 = (&___lhs0)->get_m12_9();
		float L_45 = (&___rhs1)->get_m21_6();
		float L_46 = (&___lhs0)->get_m13_13();
		float L_47 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m11_5(((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47)))));
		float L_48 = (&___lhs0)->get_m10_1();
		float L_49 = (&___rhs1)->get_m02_8();
		float L_50 = (&___lhs0)->get_m11_5();
		float L_51 = (&___rhs1)->get_m12_9();
		float L_52 = (&___lhs0)->get_m12_9();
		float L_53 = (&___rhs1)->get_m22_10();
		float L_54 = (&___lhs0)->get_m13_13();
		float L_55 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m12_9(((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55)))));
		float L_56 = (&___lhs0)->get_m10_1();
		float L_57 = (&___rhs1)->get_m03_12();
		float L_58 = (&___lhs0)->get_m11_5();
		float L_59 = (&___rhs1)->get_m13_13();
		float L_60 = (&___lhs0)->get_m12_9();
		float L_61 = (&___rhs1)->get_m23_14();
		float L_62 = (&___lhs0)->get_m13_13();
		float L_63 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m13_13(((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63)))));
		float L_64 = (&___lhs0)->get_m20_2();
		float L_65 = (&___rhs1)->get_m00_0();
		float L_66 = (&___lhs0)->get_m21_6();
		float L_67 = (&___rhs1)->get_m10_1();
		float L_68 = (&___lhs0)->get_m22_10();
		float L_69 = (&___rhs1)->get_m20_2();
		float L_70 = (&___lhs0)->get_m23_14();
		float L_71 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m20_2(((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71)))));
		float L_72 = (&___lhs0)->get_m20_2();
		float L_73 = (&___rhs1)->get_m01_4();
		float L_74 = (&___lhs0)->get_m21_6();
		float L_75 = (&___rhs1)->get_m11_5();
		float L_76 = (&___lhs0)->get_m22_10();
		float L_77 = (&___rhs1)->get_m21_6();
		float L_78 = (&___lhs0)->get_m23_14();
		float L_79 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m21_6(((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79)))));
		float L_80 = (&___lhs0)->get_m20_2();
		float L_81 = (&___rhs1)->get_m02_8();
		float L_82 = (&___lhs0)->get_m21_6();
		float L_83 = (&___rhs1)->get_m12_9();
		float L_84 = (&___lhs0)->get_m22_10();
		float L_85 = (&___rhs1)->get_m22_10();
		float L_86 = (&___lhs0)->get_m23_14();
		float L_87 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m22_10(((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87)))));
		float L_88 = (&___lhs0)->get_m20_2();
		float L_89 = (&___rhs1)->get_m03_12();
		float L_90 = (&___lhs0)->get_m21_6();
		float L_91 = (&___rhs1)->get_m13_13();
		float L_92 = (&___lhs0)->get_m22_10();
		float L_93 = (&___rhs1)->get_m23_14();
		float L_94 = (&___lhs0)->get_m23_14();
		float L_95 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m23_14(((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95)))));
		float L_96 = (&___lhs0)->get_m30_3();
		float L_97 = (&___rhs1)->get_m00_0();
		float L_98 = (&___lhs0)->get_m31_7();
		float L_99 = (&___rhs1)->get_m10_1();
		float L_100 = (&___lhs0)->get_m32_11();
		float L_101 = (&___rhs1)->get_m20_2();
		float L_102 = (&___lhs0)->get_m33_15();
		float L_103 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m30_3(((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103)))));
		float L_104 = (&___lhs0)->get_m30_3();
		float L_105 = (&___rhs1)->get_m01_4();
		float L_106 = (&___lhs0)->get_m31_7();
		float L_107 = (&___rhs1)->get_m11_5();
		float L_108 = (&___lhs0)->get_m32_11();
		float L_109 = (&___rhs1)->get_m21_6();
		float L_110 = (&___lhs0)->get_m33_15();
		float L_111 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m31_7(((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111)))));
		float L_112 = (&___lhs0)->get_m30_3();
		float L_113 = (&___rhs1)->get_m02_8();
		float L_114 = (&___lhs0)->get_m31_7();
		float L_115 = (&___rhs1)->get_m12_9();
		float L_116 = (&___lhs0)->get_m32_11();
		float L_117 = (&___rhs1)->get_m22_10();
		float L_118 = (&___lhs0)->get_m33_15();
		float L_119 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m32_11(((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119)))));
		float L_120 = (&___lhs0)->get_m30_3();
		float L_121 = (&___rhs1)->get_m03_12();
		float L_122 = (&___lhs0)->get_m31_7();
		float L_123 = (&___rhs1)->get_m13_13();
		float L_124 = (&___lhs0)->get_m32_11();
		float L_125 = (&___rhs1)->get_m23_14();
		float L_126 = (&___lhs0)->get_m33_15();
		float L_127 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m33_15(((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127)))));
		Matrix4x4_t1651859333  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Matrix4x4_op_Multiply_m314399977 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___lhs0, Vector4_t4282066567  ___v1, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_m00_0();
		float L_1 = (&___v1)->get_x_1();
		float L_2 = (&___lhs0)->get_m01_4();
		float L_3 = (&___v1)->get_y_2();
		float L_4 = (&___lhs0)->get_m02_8();
		float L_5 = (&___v1)->get_z_3();
		float L_6 = (&___lhs0)->get_m03_12();
		float L_7 = (&___v1)->get_w_4();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7)))));
		float L_8 = (&___lhs0)->get_m10_1();
		float L_9 = (&___v1)->get_x_1();
		float L_10 = (&___lhs0)->get_m11_5();
		float L_11 = (&___v1)->get_y_2();
		float L_12 = (&___lhs0)->get_m12_9();
		float L_13 = (&___v1)->get_z_3();
		float L_14 = (&___lhs0)->get_m13_13();
		float L_15 = (&___v1)->get_w_4();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15)))));
		float L_16 = (&___lhs0)->get_m20_2();
		float L_17 = (&___v1)->get_x_1();
		float L_18 = (&___lhs0)->get_m21_6();
		float L_19 = (&___v1)->get_y_2();
		float L_20 = (&___lhs0)->get_m22_10();
		float L_21 = (&___v1)->get_z_3();
		float L_22 = (&___lhs0)->get_m23_14();
		float L_23 = (&___v1)->get_w_4();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = (&___lhs0)->get_m30_3();
		float L_25 = (&___v1)->get_x_1();
		float L_26 = (&___lhs0)->get_m31_7();
		float L_27 = (&___v1)->get_y_2();
		float L_28 = (&___lhs0)->get_m32_11();
		float L_29 = (&___v1)->get_z_3();
		float L_30 = (&___lhs0)->get_m33_15();
		float L_31 = (&___v1)->get_w_4();
		(&V_0)->set_w_4(((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31)))));
		Vector4_t4282066567  L_32 = V_0;
		return L_32;
	}
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke(const Matrix4x4_t1651859333& unmarshaled, Matrix4x4_t1651859333_marshaled_pinvoke& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke_back(const Matrix4x4_t1651859333_marshaled_pinvoke& marshaled, Matrix4x4_t1651859333& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke_cleanup(Matrix4x4_t1651859333_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_com(const Matrix4x4_t1651859333& unmarshaled, Matrix4x4_t1651859333_marshaled_com& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t1651859333_marshal_com_back(const Matrix4x4_t1651859333_marshaled_com& marshaled, Matrix4x4_t1651859333& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_com_cleanup(Matrix4x4_t1651859333_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2684203808 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m3749730360(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m3749730360 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mono0, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m3749730360_ftn) (Mesh_t4241756145 *);
	static Mesh_Internal_Create_m3749730360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m3749730360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C"  void Mesh_Clear_m1789068674 (Mesh_t4241756145 * __this, bool ___keepVertexLayout0, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m1789068674_ftn) (Mesh_t4241756145 *, bool);
	static Mesh_Clear_m1789068674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m1789068674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m90337099 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		Mesh_Clear_m1789068674(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t215400611* Mesh_get_vertices_m3685486174 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t215400611* (*Mesh_get_vertices_m3685486174_ftn) (Mesh_t4241756145 *);
	static Mesh_get_vertices_m3685486174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertices_m3685486174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertices()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m2628866109 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_vertices_m2628866109_ftn) (Mesh_t4241756145 *, Vector3U5BU5D_t215400611*);
	static Mesh_set_vertices_m2628866109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_vertices_m2628866109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C"  void Mesh_set_normals_m3763698282 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_normals_m3763698282_ftn) (Mesh_t4241756145 *, Vector3U5BU5D_t215400611*);
	static Mesh_set_normals_m3763698282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_normals_m3763698282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv_m558008935 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t4024180168* (*Mesh_get_uv_m558008935_ftn) (Mesh_t4241756145 *);
	static Mesh_get_uv_m558008935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv_m558008935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv_m498907190 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_uv_m498907190_ftn) (Mesh_t4241756145 *, Vector2U5BU5D_t4024180168*);
	static Mesh_set_uv_m498907190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_uv_m498907190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern "C"  Bounds_t2711641849  Mesh_get_bounds_m1625335237 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Mesh_INTERNAL_get_bounds_m739771994(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t2711641849  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Mesh_INTERNAL_get_bounds_m739771994 (Mesh_t4241756145 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_INTERNAL_get_bounds_m739771994_ftn) (Mesh_t4241756145 *, Bounds_t2711641849 *);
	static Mesh_INTERNAL_get_bounds_m739771994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_INTERNAL_get_bounds_m739771994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1806625021 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateNormals_m1806625021_ftn) (Mesh_t4241756145 *);
	static Mesh_RecalculateNormals_m1806625021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateNormals_m1806625021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateNormals()");
	_il2cpp_icall_func(__this);
}
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t3230847821* Mesh_get_triangles_m2145908418 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3230847821* (*Mesh_get_triangles_m2145908418_ftn) (Mesh_t4241756145 *);
	static Mesh_get_triangles_m2145908418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_triangles_m2145908418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_triangles()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m2341339867 (Mesh_t4241756145 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_triangles_m2341339867_ftn) (Mesh_t4241756145 *, Int32U5BU5D_t3230847821*);
	static Mesh_set_triangles_m2341339867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_triangles_m2341339867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_triangles(System.Int32[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Mesh::get_vertexCount()
extern "C"  int32_t Mesh_get_vertexCount_m538235264 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef int32_t (*Mesh_get_vertexCount_m538235264_ftn) (Mesh_t4241756145 *);
	static Mesh_get_vertexCount_m538235264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertexCount_m538235264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshCollider_set_sharedMesh_m3062685539 (MeshCollider_t2667653125 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshCollider_set_sharedMesh_m3062685539_ftn) (MeshCollider_t2667653125 *, Mesh_t4241756145 *);
	static MeshCollider_set_sharedMesh_m3062685539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_sharedMesh_m3062685539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t4241756145 * MeshFilter_get_mesh_m484001117 (MeshFilter_t3839065225 * __this, const MethodInfo* method)
{
	typedef Mesh_t4241756145 * (*MeshFilter_get_mesh_m484001117_ftn) (MeshFilter_t3839065225 *);
	static MeshFilter_get_mesh_m484001117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m484001117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_mesh_m1404580322 (MeshFilter_t3839065225 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_mesh_m1404580322_ftn) (MeshFilter_t3839065225 *, Mesh_t4241756145 *);
	static MeshFilter_set_mesh_m1404580322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m1404580322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t4241756145 * MeshFilter_get_sharedMesh_m2700148450 (MeshFilter_t3839065225 * __this, const MethodInfo* method)
{
	typedef Mesh_t4241756145 * (*MeshFilter_get_sharedMesh_m2700148450_ftn) (MeshFilter_t3839065225 *);
	static MeshFilter_get_sharedMesh_m2700148450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_sharedMesh_m2700148450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_sharedMesh_m793190055 (MeshFilter_t3839065225 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_sharedMesh_m793190055_ftn) (MeshFilter_t3839065225 *, Mesh_t4241756145 *);
	static MeshFilter_set_sharedMesh_m793190055_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_sharedMesh_m793190055_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2022291967 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m1624944828(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m972795186 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3154030143 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m2825545578 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m2825545578_ftn) (MonoBehaviour_t667441552 *, String_t*, float);
	static MonoBehaviour_Invoke_m2825545578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m2825545578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m1115468640 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m1115468640_ftn) (MonoBehaviour_t667441552 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m1115468640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m1115468640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m3230208631 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m972795186(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2461959659 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2461959659_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2461959659_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2461959659_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1460913732 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1460913732_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_IsInvoking_m1460913732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1460913732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m3881121150 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3154030143(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2135303124 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t3621161934 * L_1 = MonoBehaviour_StartCoroutine_Auto_m1831125106(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_Auto_m1831125106 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2964903975 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_m2964903975_ftn) (MonoBehaviour_t667441552 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m2964903975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m2964903975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2272783641 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t3621161934 * L_2 = MonoBehaviour_StartCoroutine_m2964903975(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m2790918991 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m2790918991_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_StopCoroutine_m2790918991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m2790918991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1340700766 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1762309278 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t3621161934 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1074098068(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1074098068 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn) (MonoBehaviour_t667441552 *, Coroutine_t3621161934 *);
	static MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1437893335 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1437893335_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_StopAllCoroutines_m1437893335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1437893335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m1497342762_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m1497342762 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m1497342762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m4058409766 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m4058409766_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_get_useGUILayout_m4058409766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m4058409766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m589898551 (MonoBehaviour_t667441552 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m589898551_ftn) (MonoBehaviour_t667441552 *, bool);
	static MonoBehaviour_set_useGUILayout_m589898551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m589898551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_m_TimeStamp_0();
		return L_0;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m3597535726(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = __this->get_m_Sender_1();
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2720861079(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral907697851;
extern const uint32_t NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId;
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NetworkViewID_t3400394436  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3400394436  L_1 = NetworkViewID_get_unassigned_m2302461037(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m776200489(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral907697851, /*hidden argument*/NULL);
		NetworkView_t3656680617 * L_3 = NetworkMessageInfo_NullNetworkView_m516412409(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0026:
	{
		NetworkViewID_t3400394436  L_4 = __this->get_m_ViewID_2();
		NetworkView_t3656680617 * L_5 = NetworkView_Find_m2926682619(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m221874487(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkMessageInfo_NullNetworkView_m516412409_ftn) (NetworkMessageInfo_t3807997963 *);
	static NetworkMessageInfo_NullNetworkView_m516412409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m516412409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m516412409(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_pinvoke(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_back(const NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_pinvoke_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_pinvoke_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_cleanup(NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke_cleanup(marshaled.___m_ViewID_2);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_com(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_back(const NetworkMessageInfo_t3807997963_marshaled_com& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_com_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_com_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_cleanup(NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	NetworkPlayer_t3231273765_marshal_com_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com_cleanup(marshaled.___m_ViewID_2);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2814584925;
extern const uint32_t NetworkPlayer__ctor_m978047135_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m978047135 (NetworkPlayer_t3231273765 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m978047135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2814584925, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m978047135_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	NetworkPlayer__ctor_m978047135(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2705922677 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m2646691040 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m2646691040_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m2646691040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m2646691040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m4147820797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m152642746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m152642746_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m152642746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m152642746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m3142644809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m4290647008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m1155344837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m82698821 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m82698821_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m82698821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m82698821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m668021995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m3396943446(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2832997817(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t3231273765_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1380117345_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1380117345 (NetworkPlayer_t3231273765 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1380117345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkPlayer_t3231273765 *)((NetworkPlayer_t3231273765 *)UnBox (L_1, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_0)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool NetworkPlayer_Equals_m1380117345_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_Equals_m1380117345(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m3142644809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2705922677(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m206939407(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m4260923428 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m4290647008(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m2646691040(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m4260923428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_port_m4260923428(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m668021995(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m82698821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_guid_m1193250345(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m4115863651 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m1286526384(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m4115863651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_ToString_m4115863651(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m4147820797(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m420672594(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m152642746(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m2392166863(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t3231273765  NetworkPlayer_get_unassigned_m865556847 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m1190866952 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3921017795 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_back(const NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_com_back(const NetworkPlayer_t3231273765_marshaled_com& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com_cleanup(NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t3656680617 * NetworkView_Find_m2926682619 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method)
{
	{
		NetworkView_t3656680617 * L_0 = NetworkView_INTERNAL_CALL_Find_m752431216(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3656680617 * NetworkView_INTERNAL_CALL_Find_m752431216 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkView_INTERNAL_CALL_Find_m752431216_ftn) (NetworkViewID_t3400394436 *);
	static NetworkView_INTERNAL_CALL_Find_m752431216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m752431216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3400394436  NetworkViewID_get_unassigned_m2302461037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_INTERNAL_get_unassigned_m3953385100(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m3953385100 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m619939661 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m874421061 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn) (NetworkViewID_t3400394436 *, NetworkPlayer_t3231273765 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m679444096 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m2541220858 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___lhs0, NetworkViewID_t3400394436 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn) (NetworkViewID_t3400394436 *, NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_GetHashCode_m2420635706(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3400394436_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m2612049122_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m2612049122 (NetworkViewID_t3400394436 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m2612049122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3400394436_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkViewID_t3400394436 *)((NetworkViewID_t3400394436 *)UnBox (L_1, NetworkViewID_t3400394436_il2cpp_TypeInfo_var))));
		NetworkViewID_t3400394436  L_2 = V_0;
		bool L_3 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool NetworkViewID_Equals_m2612049122_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_Equals_m2612049122(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m2754407673 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m619939661(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m2754407673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_isMine_m2754407673(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_Internal_GetOwner_m874421061(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_owner_m576069262(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3783538050 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m679444096(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3783538050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_ToString_m3783538050(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m776200489 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m4224856356 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_back(const NetworkViewID_t3400394436_marshaled_pinvoke& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_cleanup(NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_com_back(const NetworkViewID_t3400394436_marshaled_com& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com_cleanup(NetworkViewID_t3400394436_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m570634126 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t3071478659 * Object_Internal_CloneSingle_m3129073756 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_Internal_CloneSingle_m3129073756_ftn) (Object_t3071478659 *);
	static Object_Internal_CloneSingle_m3129073756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m3129073756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t3071478659 * Object_Internal_InstantiateSingle_m3047563925 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566  ___pos1, Quaternion_t1553702882  ___rot2, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___data0;
		Object_t3071478659 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3071478659 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566 * ___pos1, Quaternion_t1553702882 * ___rot2, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn) (Object_t3071478659 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m2260435093 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m2260435093_ftn) (Object_t3071478659 *, float);
	static Object_Destroy_m2260435093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m2260435093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m176400816 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m1826427014 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m1826427014_ftn) (Object_t3071478659 *, bool);
	static Object_DestroyImmediate_m1826427014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m1826427014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m349958679 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t3071478659 * L_0 = ___obj0;
		bool L_1 = V_0;
		Object_DestroyImmediate_m1826427014(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfType_m975740280 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfType_m975740280_ftn) (Type_t *);
	static Object_FindObjectsOfType_m975740280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m975740280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3709440845 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m3709440845_ftn) (Object_t3071478659 *);
	static Object_get_name_m3709440845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m3709440845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m1123518500 (Object_t3071478659 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m1123518500_ftn) (Object_t3071478659 *, String_t*);
	static Object_set_name_m1123518500_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m1123518500_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m4064482788 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m4064482788_ftn) (Object_t3071478659 *);
	static Object_DontDestroyOnLoad_m4064482788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m4064482788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m1893459363 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m1893459363_ftn) (Object_t3071478659 *);
	static Object_get_hideFlags_m1893459363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m1893459363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m41317712 (Object_t3071478659 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m41317712_ftn) (Object_t3071478659 *, int32_t);
	static Object_set_hideFlags_m41317712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m41317712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m3324336244 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m3324336244_ftn) (Object_t3071478659 *, float);
	static Object_DestroyObject_m3324336244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m3324336244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C"  void Object_DestroyObject_m3900253135 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		Object_DestroyObject_m3324336244(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindSceneObjectsOfType_m2168852346 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindSceneObjectsOfType_m2168852346_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m2168852346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m2168852346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfTypeIncludingAssets_m1276784656 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m2155033093 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m2155033093_ftn) (Object_t3071478659 *);
	static Object_ToString_m2155033093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m2155033093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m3762691104 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m3762691104_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m3762691104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m3762691104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m1697651929_MetadataUsageId;
extern "C"  bool Object_Equals_m1697651929 (Object_t3071478659 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m1697651929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, __this, ((Object_t3071478659 *)IsInstClass(L_0, Object_t3071478659_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m1758859581 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m200424466(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m2625210252 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___lhs0, Object_t3071478659 * ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t3071478659 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t3071478659 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t3071478659 * L_5 = ___lhs0;
		bool L_6 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t3071478659 * L_8 = ___rhs1;
		bool L_9 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t3071478659 * L_10 = ___lhs0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_m_InstanceID_0();
		Object_t3071478659 * L_12 = ___rhs1;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_m_InstanceID_0();
		return (bool)((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m434626365_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m434626365 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m434626365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m1583421857(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m200424466 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_InstanceID_0();
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m1583421857 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_1();
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_m2255090103_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m2255090103 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2255090103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3473406, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Vector3_t4282066566  L_2 = ___position1;
		Quaternion_t1553702882  L_3 = ___rotation2;
		Object_t3071478659 * L_4 = Object_Internal_InstantiateSingle_m3047563925(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_m3040600263_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m3040600263 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3040600263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3473406, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Object_t3071478659 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m264735768_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m264735768 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m264735768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t3071478659 * Object_FindObjectOfType_m3820159265 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	ObjectU5BU5D_t1015136018* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ObjectU5BU5D_t1015136018* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1015136018* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1015136018* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		Object_t3071478659 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}

IL_0014:
	{
		return (Object_t3071478659 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2106766291 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___exists0, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___exists0;
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3964590952 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1296218211 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_pinvoke& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_pinvoke_back(const Object_t3071478659_marshaled_pinvoke& marshaled, Object_t3071478659& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke_cleanup(Object_t3071478659_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_com& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_com_back(const Object_t3071478659_marshaled_com& marshaled, Object_t3071478659& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com_cleanup(Object_t3071478659_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2201046863 (Plane_t4206452690 * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___inNormal0;
		Vector3_t4282066566  L_1 = Vector3_Normalize_m3047997355(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t4282066566  L_2 = ___inNormal0;
		Vector3_t4282066566  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2201046863_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	Plane__ctor_m2201046863(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_normal_m3534129213(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m2612484153 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_1();
		return L_0;
	}
}
extern "C"  float Plane_get_distance_m2612484153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_distance_m2612484153(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2829769106_MetadataUsageId;
extern "C"  bool Plane_Raycast_m2829769106 (Plane_t4206452690 * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2829769106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t4282066566  L_0 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m2612484153(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		return (bool)0;
	}

IL_0047:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		return (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
extern "C"  bool Plane_Raycast_m2829769106_AdjustorThunk (Il2CppObject * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_Raycast_m2829769106(_thisAdjusted, ___ray0, ___enter1, method);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_pinvoke_back(const Plane_t4206452690_marshaled_pinvoke& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke_cleanup(Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Normal_0);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_com_back(const Plane_t4206452690_marshaled_com& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com_cleanup(Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Normal_0);
}
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m2055981962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m2055981962_ftn) ();
	static QualitySettings_get_antiAliasing_m2055981962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m2055981962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	return _il2cpp_icall_func();
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m2993616266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m2993616266_ftn) ();
	static QualitySettings_get_activeColorSpace_m2993616266_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m2993616266_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1100844011 (Quaternion_t1553702882 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m1100844011_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion__ctor_m1100844011(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t1553702882  Quaternion_get_identity_m1743882806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m1100844011(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_AngleAxis_m644124247 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566  ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m1562314763(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m1562314763 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566 * ___axis1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn) (float, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m791330738 (Quaternion_t1553702882 * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		Quaternion_Internal_ToAxisAngleRad_m3414012038(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m791330738_AdjustorThunk (Il2CppObject * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_ToAngleAxis_m791330738(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m2869326048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, Vector3_t4282066566  ___upwards1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1501255504 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___forward0, Vector3_t4282066566 * ___upwards1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_Inverse_m3542515566 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Inverse_m4175627710(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m4175627710 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Quaternion_ToString_m1793285860_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m1793285860 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m1793285860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m1793285860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_ToString_m1793285860(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1940911101 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___euler0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_2 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Internal_FromEulerRad_m3681319598 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___euler0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m3414012038 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3823258238 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m3823258238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_GetHashCode_m3823258238(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3843409946_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3843409946 (Quaternion_t1553702882 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3843409946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t1553702882_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Quaternion_t1553702882 *)((Quaternion_t1553702882 *)UnBox (L_1, Quaternion_t1553702882_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Quaternion_Equals_m3843409946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_Equals_m3843409946(_thisAdjusted, ___other0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_op_Multiply_m3077481361 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_w_4();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_x_1();
		float L_3 = (&___rhs1)->get_w_4();
		float L_4 = (&___lhs0)->get_y_2();
		float L_5 = (&___rhs1)->get_z_3();
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_y_2();
		float L_8 = (&___lhs0)->get_w_4();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_w_4();
		float L_12 = (&___lhs0)->get_z_3();
		float L_13 = (&___rhs1)->get_x_1();
		float L_14 = (&___lhs0)->get_x_1();
		float L_15 = (&___rhs1)->get_z_3();
		float L_16 = (&___lhs0)->get_w_4();
		float L_17 = (&___rhs1)->get_z_3();
		float L_18 = (&___lhs0)->get_z_3();
		float L_19 = (&___rhs1)->get_w_4();
		float L_20 = (&___lhs0)->get_x_1();
		float L_21 = (&___rhs1)->get_y_2();
		float L_22 = (&___lhs0)->get_y_2();
		float L_23 = (&___rhs1)->get_x_1();
		float L_24 = (&___lhs0)->get_w_4();
		float L_25 = (&___rhs1)->get_w_4();
		float L_26 = (&___lhs0)->get_x_1();
		float L_27 = (&___rhs1)->get_x_1();
		float L_28 = (&___lhs0)->get_y_2();
		float L_29 = (&___rhs1)->get_y_2();
		float L_30 = (&___lhs0)->get_z_3();
		float L_31 = (&___rhs1)->get_z_3();
		Quaternion_t1553702882  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m1100844011(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Quaternion_op_Multiply_m3771288979 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, Vector3_t4282066566  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = (&___rotation0)->get_x_1();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_2();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_3();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_1();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_2();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_3();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_1();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_1();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_2();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_4();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_4();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_4();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t4282066566  L_48 = V_12;
		return L_48;
	}
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_pinvoke_back(const Quaternion_t1553702882_marshaled_pinvoke& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke_cleanup(Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_com_back(const Quaternion_t1553702882_marshaled_com& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com_cleanup(Quaternion_t1553702882_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_origin_m3064983562(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_direction_m3201866877(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822 (Ray_t3134616544 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_GetPoint_m1171104822(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray_ToString_m1391619614_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m1391619614 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1391619614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566  L_1 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566  L_5 = __this->get_m_Direction_1();
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m1391619614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_ToString_m1391619614(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_pinvoke_back(const Ray_t3134616544_marshaled_pinvoke& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke_cleanup(Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_com_back(const Ray_t3134616544_marshaled_com& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com_cleanup(Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3291325233 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3291325233_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3291325233(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3858381006 (Rect_t4241904616 * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3858381006_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3858381006(_thisAdjusted, ___source0, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m982385354 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_x_m982385354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_x_m982385354(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m577970569 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m577970569_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_x_m577970569(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m982386315 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_y_m982386315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_y_m982386315(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m67436392 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m67436392_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_y_m67436392(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_position_m2933356232(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_center_m610643572(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m2824209432 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		return L_0;
	}
}
extern "C"  float Rect_get_width_m2824209432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_width_m2824209432(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3771513595 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m3771513595_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_width_m3771513595(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m2154960823 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		return L_0;
	}
}
extern "C"  float Rect_get_height_m2154960823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_height_m2154960823(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3398820332 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3398820332_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_height_m3398820332(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_size_m136480416(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m371109962 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_xMin_m371109962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMin_m371109962(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m399739113 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_yMin_m399739113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMin_m399739113(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m370881244 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_xMax_m370881244_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMax_m370881244(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m399510395 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_yMax_m399510395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMax_m399510395(_thisAdjusted, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2721079081;
extern const uint32_t Rect_ToString_m2093687658_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2093687658 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2093687658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2721079081, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2093687658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_ToString_m2093687658(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3556594010 (Rect_t4241904616 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594010_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594010(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3556594041 (Rect_t4241904616 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594041_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594041(_thisAdjusted, ___point0, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m89026168 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m65342520((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m65342520((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m65342520((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m65342520((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Rect_GetHashCode_m89026168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_GetHashCode_m89026168(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m1091722644_MetadataUsageId;
extern "C"  bool Rect_Equals_m1091722644 (Rect_t4241904616 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m1091722644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t4241904616_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Rect_t4241904616 *)((Rect_t4241904616 *)UnBox (L_1, Rect_t4241904616_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m2110115959((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m2110115959((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m2110115959((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m2110115959((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Rect_Equals_m1091722644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Equals_m1091722644(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m2236552616 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1552341101 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_pinvoke& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_pinvoke_back(const Rect_t4241904616_marshaled_pinvoke& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke_cleanup(Rect_t4241904616_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_com& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_com_back(const Rect_t4241904616_marshaled_com& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com_cleanup(Rect_t4241904616_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2395783478 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m358348983 (RectOffset_t3056157787 * __this, GUIStyle_t2990928826 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3353955934 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m3353955934_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Init_m3353955934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3353955934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m2914212664 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m2914212664_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Cleanup_m2914212664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m2914212664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m4104523390 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4104523390_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_left_m4104523390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4104523390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m901965251 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m901965251_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_left_m901965251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m901965251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3831383975 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m3831383975_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_right_m3831383975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m3831383975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m2119805444 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m2119805444_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_right_m2119805444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m2119805444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m140097312 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m140097312_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_top_m140097312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m140097312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3043172093 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3043172093_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_top_m3043172093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3043172093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2106858018 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2106858018_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_bottom_m2106858018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2106858018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m3840454247 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m3840454247_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_bottom_m3840454247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m3840454247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1186440923 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1186440923_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_horizontal_m1186440923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1186440923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3650431789 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3650431789_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_vertical_m3650431789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3650431789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m3416542060 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t2990928826 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m2914212664(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2126166178;
extern const uint32_t RectOffset_ToString_m2231965149_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m2231965149 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m2231965149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m4104523390(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m3831383975(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m140097312(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2106858018(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2126166178, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_pinvoke_back(const RectOffset_t3056157787_marshaled_pinvoke& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke_cleanup(RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_com_back(const RectOffset_t3056157787_marshaled_com& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com_cleanup(RectOffset_t3056157787_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m2261331528 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t779639188 * L_1 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t972643934 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3880635155(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m3710908308 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3880635155 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3880635155((ReapplyDrivenProperties_t779639188 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m1851329218 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m3686159268 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.IntPtr UnityEngine.RenderBuffer::GetNativeRenderBufferPtr()
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2564329965 (RenderBuffer_t3529837690 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_BufferPtr_1();
		return L_0;
	}
}
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2564329965_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690 * _thisAdjusted = reinterpret_cast<RenderBuffer_t3529837690 *>(__this + 1);
	return RenderBuffer_GetNativeRenderBufferPtr_m2564329965(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke(const RenderBuffer_t3529837690& unmarshaled, RenderBuffer_t3529837690_marshaled_pinvoke& marshaled)
{
	marshaled.___m_RenderTextureInstanceID_0 = unmarshaled.get_m_RenderTextureInstanceID_0();
	marshaled.___m_BufferPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_BufferPtr_1()).get_m_value_0());
}
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke_back(const RenderBuffer_t3529837690_marshaled_pinvoke& marshaled, RenderBuffer_t3529837690& unmarshaled)
{
	int32_t unmarshaled_m_RenderTextureInstanceID_temp_0 = 0;
	unmarshaled_m_RenderTextureInstanceID_temp_0 = marshaled.___m_RenderTextureInstanceID_0;
	unmarshaled.set_m_RenderTextureInstanceID_0(unmarshaled_m_RenderTextureInstanceID_temp_0);
	IntPtr_t unmarshaled_m_BufferPtr_temp_1;
	memset(&unmarshaled_m_BufferPtr_temp_1, 0, sizeof(unmarshaled_m_BufferPtr_temp_1));
	IntPtr_t unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled_m_BufferPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_BufferPtr_1));
	unmarshaled_m_BufferPtr_temp_1 = unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled.set_m_BufferPtr_1(unmarshaled_m_BufferPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke_cleanup(RenderBuffer_t3529837690_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_com(const RenderBuffer_t3529837690& unmarshaled, RenderBuffer_t3529837690_marshaled_com& marshaled)
{
	marshaled.___m_RenderTextureInstanceID_0 = unmarshaled.get_m_RenderTextureInstanceID_0();
	marshaled.___m_BufferPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_BufferPtr_1()).get_m_value_0());
}
extern "C" void RenderBuffer_t3529837690_marshal_com_back(const RenderBuffer_t3529837690_marshaled_com& marshaled, RenderBuffer_t3529837690& unmarshaled)
{
	int32_t unmarshaled_m_RenderTextureInstanceID_temp_0 = 0;
	unmarshaled_m_RenderTextureInstanceID_temp_0 = marshaled.___m_RenderTextureInstanceID_0;
	unmarshaled.set_m_RenderTextureInstanceID_0(unmarshaled_m_RenderTextureInstanceID_temp_0);
	IntPtr_t unmarshaled_m_BufferPtr_temp_1;
	memset(&unmarshaled_m_BufferPtr_temp_1, 0, sizeof(unmarshaled_m_BufferPtr_temp_1));
	IntPtr_t unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled_m_BufferPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_BufferPtr_1));
	unmarshaled_m_BufferPtr_temp_1 = unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled.set_m_BufferPtr_1(unmarshaled_m_BufferPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_com_cleanup(RenderBuffer_t3529837690_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2514140131 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2514140131_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_enabled_m2514140131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2514140131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t3870600107 * Renderer_get_material_m2720864603 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_material_m2720864603_ftn) (Renderer_t3076687687 *);
	static Renderer_get_material_m2720864603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2720864603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1012580896 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1012580896_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_material_m1012580896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1012580896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1064371045 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m1064371045_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_sharedMaterial_m1064371045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m1064371045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_materials_m3755041148 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_materials_m3755041148_ftn) (Renderer_t3076687687 *);
	static Renderer_get_materials_m3755041148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m3755041148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m1255100914 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m1255100914_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_sharedMaterials_m1255100914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m1255100914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture__ctor_m2927948204 (RenderTexture_t1963041563 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1444121965(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		RenderTexture_set_width_m2449777612(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTexture_set_height_m4249514117(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m68043273(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m2539201833(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m2486231130(NULL /*static, unused*/, __this, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1444121965 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m800385162 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*RenderTexture_GetTemporary_m800385162_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m800385162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m800385162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width0, ___height1, ___depthBuffer2, ___format3, ___readWrite4, ___antiAliasing5);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m52998487 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m3045961066 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___temp0, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m3045961066_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_ReleaseTemporary_m3045961066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m3045961066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1030655936 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m1030655936_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetWidth_m1030655936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m1030655936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m3535686411 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m3535686411_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetWidth_m3535686411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m3535686411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m1940011033 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m1940011033_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetHeight_m1940011033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m1940011033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m4218891754 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m4218891754_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetHeight_m4218891754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m4218891754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m2486231130 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1498578543 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m1030655936(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m2449777612 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m3535686411(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4010076224 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m1940011033(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m4249514117 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m4218891754(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m68043273 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m68043273_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_depth_m68043273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m68043273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2539201833 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m2539201833_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_format_m2539201833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m2539201833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_generateMips(System.Boolean)
extern "C"  void RenderTexture_set_generateMips_m3082423096 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_generateMips_m3082423096_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_generateMips_m3082423096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_generateMips_m3082423096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_generateMips(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C"  void RenderTexture_set_antiAliasing_m3431921842 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m3431921842_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_antiAliasing_m3431921842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m3431921842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C"  bool RenderTexture_Create_m703203206 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m2783419455(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m2783419455 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C"  void RenderTexture_Release_m2820287481 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m2325488906(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2325488906 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C"  bool RenderTexture_IsCreated_m599737014 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m885429165(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m885429165 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t3529837690  RenderTexture_get_colorBuffer_m1260375694 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderTexture_GetColorBuffer_m4145048604(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3529837690  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t3529837690  RenderTexture_get_depthBuffer_m990382574 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderTexture_GetDepthBuffer_m63945916(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3529837690  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m4145048604 (RenderTexture_t1963041563 * __this, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetColorBuffer_m4145048604_ftn) (RenderTexture_t1963041563 *, RenderBuffer_t3529837690 *);
	static RenderTexture_GetColorBuffer_m4145048604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetColorBuffer_m4145048604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m63945916 (RenderTexture_t1963041563 * __this, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetDepthBuffer_m63945916_ftn) (RenderTexture_t1963041563 *, RenderBuffer_t3529837690 *);
	static RenderTexture_GetDepthBuffer_m63945916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetDepthBuffer_m63945916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m1002947377 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m1002947377_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_set_active_m1002947377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m1002947377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m2023271172 (RequireComponent_t1687166108 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m2863879896 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t3071478659 * ResourceRequest_get_asset_m670320982 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_back(const ResourceRequest_t3731857623_marshaled_pinvoke& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_cleanup(ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_com_back(const ResourceRequest_t3731857623_marshaled_com& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com_cleanup(ResourceRequest_t3731857623_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2187391845_MetadataUsageId;
extern "C"  Object_t3071478659 * Resources_Load_m2187391845 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2187391845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t3071478659 * Resources_Load_m3601699608 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_Load_m3601699608_ftn) (String_t*, Type_t *);
	static Resources_Load_m3601699608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m3601699608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t3699081103 * Resources_UnloadUnusedAssets_m3831138427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*Resources_UnloadUnusedAssets_m3831138427_ftn) ();
	static Resources_UnloadUnusedAssets_m3831138427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m3831138427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m281827604 (RPC_t3134615963 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3080333084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m3080333084_ftn) ();
	static Screen_get_width_m3080333084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m3080333084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1504859443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1504859443_ftn) ();
	static Screen_get_height_m1504859443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1504859443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
extern "C"  bool Screen_get_autorotateToPortrait_m187839762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortrait_m187839762_ftn) ();
	static Screen_get_autorotateToPortrait_m187839762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortrait_m187839762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortrait()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
extern "C"  void Screen_set_autorotateToPortrait_m3130934167 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortrait_m3130934167_ftn) (bool);
	static Screen_set_autorotateToPortrait_m3130934167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortrait_m3130934167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
extern "C"  bool Screen_get_autorotateToPortraitUpsideDown_m631759142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn) ();
	static Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortraitUpsideDown()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
extern "C"  void Screen_set_autorotateToPortraitUpsideDown_m493773611 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn) (bool);
	static Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
extern "C"  bool Screen_get_autorotateToLandscapeLeft_m3574433901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeLeft_m3574433901_ftn) ();
	static Screen_get_autorotateToLandscapeLeft_m3574433901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeLeft_m3574433901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeLeft()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeLeft_m3928884054 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeLeft_m3928884054_ftn) (bool);
	static Screen_set_autorotateToLandscapeLeft_m3928884054_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeLeft_m3928884054_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
extern "C"  bool Screen_get_autorotateToLandscapeRight_m283511704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeRight_m283511704_ftn) ();
	static Screen_get_autorotateToLandscapeRight_m283511704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeRight_m283511704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeRight()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeRight_m158995741 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeRight_m158995741_ftn) (bool);
	static Screen_set_autorotateToLandscapeRight_m158995741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeRight_m158995741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m1193220576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_orientation_m1193220576_ftn) ();
	static Screen_get_orientation_m1193220576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m1193220576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m931760051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_orientation_m931760051_ftn) (int32_t);
	static Screen_set_orientation_m931760051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m931760051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m4188281051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m4188281051_ftn) (int32_t);
	static Screen_set_sleepTimeout_m4188281051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m4188281051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1827087273 (ScriptableObject_t2970544072 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m2334361070(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2334361070 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2970544072 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn) (ScriptableObject_t2970544072 *);
	static ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m750914562 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstance_m750914562_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m750914562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m750914562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m3255479417 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2970544072 * L_1 = ScriptableObject_CreateInstanceFromType_m3795352533(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstanceFromType_m3795352533 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstanceFromType_m3795352533_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3795352533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3795352533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_back(const ScriptableObject_t2970544072_marshaled_pinvoke& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_cleanup(ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_com& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_com_back(const ScriptableObject_t2970544072_marshaled_com& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com_cleanup(ScriptableObject_t2970544072_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m940065582 (RequiredByNativeCodeAttribute_t3165457172 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3320039756 (UsedByNativeCodeAttribute_t3197444790 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m2731695210_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m2731695210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m2731695210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t3209134097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t3209134097  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t3209134097  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t3209134097  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t3209134097  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t3209134097  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t3209134097  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t3209134097  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t3452915852* L_0 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t3209134097  L_1 = V_0;
		(*(HitInfo_t3209134097 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t3452915852* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t3209134097  L_3 = V_1;
		(*(HitInfo_t3209134097 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t3452915852* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_5 = V_2;
		(*(HitInfo_t3209134097 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t3452915852* L_6 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3209134097  L_7 = V_3;
		(*(HitInfo_t3209134097 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t3209134097  L_9 = V_4;
		(*(HitInfo_t3209134097 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t3452915852* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t3209134097  L_11 = V_5;
		(*(HitInfo_t3209134097 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t3452915852* L_12 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t3209134097  L_13 = V_6;
		(*(HitInfo_t3209134097 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t3452915852* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t3209134097  L_15 = V_7;
		(*(HitInfo_t3209134097 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t3452915852* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t3209134097  L_17 = V_8;
		(*(HitInfo_t3209134097 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m2590456785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m4104134333 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t2727095145 * V_3 = NULL;
	CameraU5BU5D_t2716570836* V_4 = NULL;
	int32_t V_5 = 0;
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	GUILayer_t2983897946 * V_7 = NULL;
	GUIElement_t3775428101 * V_8 = NULL;
	Ray_t3134616544  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t3674682005 * V_12 = NULL;
	GameObject_t3674682005 * V_13 = NULL;
	int32_t V_14 = 0;
	HitInfo_t3209134097  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m3993434431(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_2 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_3 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t2716570836*)SZArrayNew(CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_6 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m3771867787(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_7 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t3209134097  L_9 = V_15;
		(*(HitInfo_t3209134097 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_12 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_14 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t2716570836* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Camera_t2727095145 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = L_18;
		Camera_t2727095145 * L_19 = V_3;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t2727095145 * L_22 = V_3;
		NullCheck(L_22);
		RenderTexture_t1963041563 * L_23 = Camera_get_targetTexture_m1468336738(L_22, /*hidden argument*/NULL);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t2727095145 * L_25 = V_3;
		NullCheck(L_25);
		Rect_t4241904616  L_26 = Camera_get_pixelRect_m936851539(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t4282066566  L_27 = V_0;
		bool L_28 = Rect_Contains_m3556594041((&V_6), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t2727095145 * L_29 = V_3;
		NullCheck(L_29);
		GUILayer_t2983897946 * L_30 = Component_GetComponent_TisGUILayer_t2983897946_m2891371969(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var);
		V_7 = L_30;
		GUILayer_t2983897946 * L_31 = V_7;
		bool L_32 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t2983897946 * L_33 = V_7;
		Vector3_t4282066566  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3775428101 * L_35 = GUILayer_HitTest_m3356120918(L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		GUIElement_t3775428101 * L_36 = V_8;
		bool L_37 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_38 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		GUIElement_t3775428101 * L_39 = V_8;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t3452915852* L_41 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		Camera_t2727095145 * L_42 = V_3;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_44 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_0145:
	{
		Camera_t2727095145 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m3669132771(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t2727095145 * L_47 = V_3;
		Vector3_t4282066566  L_48 = V_0;
		NullCheck(L_47);
		Ray_t3134616544  L_49 = Camera_ScreenPointToRay_m1733083890(L_47, L_48, /*hidden argument*/NULL);
		V_9 = L_49;
		Vector3_t4282066566  L_50 = Ray_get_direction_m3201866877((&V_9), /*hidden argument*/NULL);
		V_16 = L_50;
		float L_51 = (&V_16)->get_z_3();
		V_10 = L_51;
		float L_52 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t2727095145 * L_54 = V_3;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m388706726(L_54, /*hidden argument*/NULL);
		Camera_t2727095145 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m4074655061(L_56, /*hidden argument*/NULL);
		float L_58 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B23_0 = L_59;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t2727095145 * L_60 = V_3;
		Ray_t3134616544  L_61 = V_9;
		float L_62 = V_11;
		Camera_t2727095145 * L_63 = V_3;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m1045975289(L_63, /*hidden argument*/NULL);
		Camera_t2727095145 * L_65 = V_3;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m3669132771(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t3674682005 * L_67 = Camera_RaycastTry_m569221064(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_12 = L_67;
		GameObject_t3674682005 * L_68 = V_12;
		bool L_69 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_68, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_70 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		GameObject_t3674682005 * L_71 = V_12;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t3452915852* L_72 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		Camera_t2727095145 * L_73 = V_3;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t2727095145 * L_74 = V_3;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m192466552(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t2727095145 * L_76 = V_3;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m192466552(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_78 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_79 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_022a:
	{
		Camera_t2727095145 * L_80 = V_3;
		Ray_t3134616544  L_81 = V_9;
		float L_82 = V_11;
		Camera_t2727095145 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m1045975289(L_83, /*hidden argument*/NULL);
		Camera_t2727095145 * L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m3669132771(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t3674682005 * L_87 = Camera_RaycastTry2D_m3256311322(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_13 = L_87;
		GameObject_t3674682005 * L_88 = V_13;
		bool L_89 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_88, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_90 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 2);
		GameObject_t3674682005 * L_91 = V_13;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t3452915852* L_92 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 2);
		Camera_t2727095145 * L_93 = V_3;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t2727095145 * L_94 = V_3;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m192466552(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t2727095145 * L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m192466552(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_98 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_99 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 2);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_02b2:
	{
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_101 = V_5;
		CameraU5BU5D_t2716570836* L_102 = V_4;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_103 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_104 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		SendMouseEvents_SendEvents_m3877180750(NULL /*static, unused*/, L_103, (*(HitInfo_t3209134097 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_14;
		V_14 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_107 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_108 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2222972840;
extern Il2CppCodeGenString* _stringLiteral2438936645;
extern Il2CppCodeGenString* _stringLiteral288346913;
extern Il2CppCodeGenString* _stringLiteral2222975034;
extern Il2CppCodeGenString* _stringLiteral2223306714;
extern Il2CppCodeGenString* _stringLiteral2223010852;
extern Il2CppCodeGenString* _stringLiteral193571986;
extern const uint32_t SendMouseEvents_SendEvents_m3877180750_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m3877180750 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3209134097  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3877180750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t3209134097  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_5 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t3209134097  L_7 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m2569183060(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral2222972840, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_11 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t3209134097  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_15 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_14, (*(HitInfo_t3209134097 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_18 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m2569183060(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2438936645, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_20 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m2569183060(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral288346913, /*hidden argument*/NULL);
		HitInfoU5BU5D_t3452915852* L_22 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_24 = V_2;
		(*(HitInfo_t3209134097 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_25 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_28 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m2569183060(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral2222975034, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t3209134097  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_31 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_30, (*(HitInfo_t3209134097 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t3209134097  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_36 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_39 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m2569183060(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2223010852, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t3209134097  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral193571986, /*hidden argument*/NULL);
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t3209134097  L_45 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2569183060 (HitInfo_t3209134097 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m423373689(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m2569183060_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t3209134097 * _thisAdjusted = reinterpret_cast<HitInfo_t3209134097 *>(__this + 1);
	HitInfo_SendMessage_m2569183060(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m4083757090 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___lhs0, HitInfo_t3209134097  ___rhs1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t3674682005 * L_1 = (&___rhs1)->get_target_0();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t2727095145 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t2727095145 * L_4 = (&___rhs1)->get_camera_1();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m1943931337 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___exists0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___exists0)->get_target_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t2727095145 * L_2 = (&___exists0)->get_camera_1();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_pinvoke_back(const HitInfo_t3209134097_marshaled_pinvoke& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke_cleanup(HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_com_back(const HitInfo_t3209134097_marshaled_com& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com_cleanup(HitInfo_t3209134097_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m2703462003 (FormerlySerializedAsAttribute_t2216353654 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m2935479929 (FormerlySerializedAsAttribute_t2216353654 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_oldName_0();
		return L_0;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m4068807987 (SerializeField_t3754825534 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m889466149 (SerializePrivateVariables_t2835496234 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1859371625;
extern Il2CppCodeGenString* _stringLiteral2712312467;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m3556339879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_3, _stringLiteral1859371625, _stringLiteral2712312467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m3691215301 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		V_0 = (ObjectU5BU5D_t1108656482*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0013:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t1108656482* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t1074302268 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, ParameterModifierU5BU5D_t3896472559*, CultureInfo_t1065375142 *, StringU5BU5D_t4054002952* >::Invoke(71 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1074302268 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3896472559*)(ParameterModifierU5BU5D_t3896472559*)NULL, (CultureInfo_t1065375142 *)NULL, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL);
		return L_8;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t3191267369 * Shader_Find_m4048047578 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef Shader_t3191267369 * (*Shader_Find_m4048047578_ftn) (String_t*);
	static Shader_Find_m4048047578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m4048047578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3019342011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m3019342011_ftn) (String_t*);
	static Shader_PropertyToID_m3019342011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3019342011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m2764338918 (SharedBetweenAnimatorsAttribute_t1486273289 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m3732503849 (SliderState_t1233388262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m573039859 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3189060351_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1721872494_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m102270234_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m102270234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m102270234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_friends_10(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_users_11(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t3189060351 * L_0 = (List_1_t3189060351 *)il2cpp_codegen_object_new(List_1_t3189060351_il2cpp_TypeInfo_var);
		List_1__ctor_m1721872494(L_0, /*hidden argument*/List_1__ctor_m1721872494_MethodInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_14(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_FriendsCallback_1(L_0);
		GameCenterPlatform_Internal_LoadFriends_m1921936862(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m582381960(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m582381960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m582381960_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m582381960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m582381960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m2780967960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m2780967960_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m2780967960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m2780967960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m1252299660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m1252299660_ftn) ();
	static GameCenterPlatform_Internal_UserName_m1252299660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m1252299660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m385481212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m385481212_ftn) ();
	static GameCenterPlatform_Internal_UserID_m385481212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m385481212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m4169738944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m4169738944_ftn) ();
	static GameCenterPlatform_Internal_Underage_m4169738944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m4169738944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3884108195 * GameCenterPlatform_Internal_UserImage_m3175776130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*GameCenterPlatform_Internal_UserImage_m3175776130_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m3175776130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m3175776130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m1921936862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m817891229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m2511520970 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id0, ___progress1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m408601947 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m408601947_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m408601947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m408601947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score0, ___category1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m523283944 (Il2CppObject * __this /* static, unused */, String_t* ___category0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m523283944_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m523283944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m523283944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m4218820079 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___userIds0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn) (StringU5BU5D_t4054002952*);
	static GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m165059209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m878609996 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_12(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m165059209(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3791866548 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m3158758843 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m3174496109 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t2242891083  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_1 = ___number1;
		AchievementDescription_t2116066607 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m3125480712((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t2116066607 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2873143867;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m3728674360 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2873143867, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		AchievementDescription_t2116066607 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3884108195 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1092175896(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral48254646;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral48254646, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		AchievementDescriptionU5BU5D_t759444790* L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_3);
		Action_1_Invoke_m2766540343(L_3, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)L_4, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B3_0 = NULL;
	Action_1_t872614854 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t872614854 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m3594021162(G_B4_1, (bool)G_B4_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_1_t872614854 *)NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1761222218 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m3378244042 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m4228294663 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B5_0 = NULL;
	Action_1_t872614854 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t872614854 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2378268441* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		NullCheck(L_1);
		LocalUser_SetFriends_m3475409220(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		int32_t L_5 = ___result0;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3594021162(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3379715660;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m2031411110 (Il2CppObject * __this /* static, unused */, GcAchievementDataU5BU5D_t1726768202* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t912418020* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1726768202* L_1 = ___result0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3379715660, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1726768202* L_2 = ___result0;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t912418020* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_5 = ___result0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t344600729 * L_7 = GcAchievementData_ToAchievement_m3239514930(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t344600729 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_10 = ___result0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_11 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		AchievementU5BU5D_t912418020* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m222677977(L_11, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)L_12, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m165794409 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2797312324 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053 (Il2CppObject * __this /* static, unused */, GcScoreDataU5BU5D_t1670395707* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___result0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___result0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___result0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_10 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m50340758(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t1307362368_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m1634439374 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t1307362368 * L_1 = (LocalUser_t1307362368 *)il2cpp_codegen_object_new(LocalUser_t1307362368_il2cpp_TypeInfo_var);
		LocalUser__ctor_m1052633066(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_13(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m2095754825(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral48, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2583301917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m653377406(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m1252299660(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m914181770(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m385481212(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m1515238170(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_7 = GameCenterPlatform_Internal_Underage_m4169738944(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m2968368872(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_8 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		Texture2D_t3884108195 * L_9 = GameCenterPlatform_Internal_UserImage_m3175776130(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m1928130753(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m232801667 (GameCenterPlatform_t3570684786 * __this, Action_1_t229750097 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t229750097 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m2766540343(L_1, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t229750097 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementDescriptionLoaderCallback_2(L_2);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m4110499833 (GameCenterPlatform_t3570684786 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ProgressCallback_4(L_2);
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		GameCenterPlatform_Internal_ReportProgress_m2511520970(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m2745782249 (GameCenterPlatform_t3570684786 * __this, Action_1_t2349069933 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t2349069933 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m222677977(L_1, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t2349069933 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementLoaderCallback_3(L_2);
		GameCenterPlatform_Internal_LoadAchievements_m817891229(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m1009544586 (GameCenterPlatform_t3570684786 * __this, int64_t ___score0, String_t* ___board1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreCallback_5(L_2);
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		GameCenterPlatform_Internal_ReportScore_m408601947(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m3562614827 (GameCenterPlatform_t3570684786 * __this, String_t* ___category0, Action_1_t645920862 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t645920862 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m50340758(L_1, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t645920862 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreLoaderCallback_6(L_2);
		String_t* L_3 = ___category0;
		GameCenterPlatform_Internal_LoadScores_m523283944(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t1820874799_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1416233310_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2883394111 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	GcLeaderboard_t1820874799 * V_1 = NULL;
	Range_t1533311935  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_LeaderboardCallback_7(L_2);
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t1185876199 *)CastclassClass(L_3, Leaderboard_t1185876199_il2cpp_TypeInfo_var));
		Leaderboard_t1185876199 * L_4 = V_0;
		GcLeaderboard_t1820874799 * L_5 = (GcLeaderboard_t1820874799 *)il2cpp_codegen_object_new(GcLeaderboard_t1820874799_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m4042810199(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t3189060351 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		GcLeaderboard_t1820874799 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1416233310(L_6, L_7, /*hidden argument*/List_1_Add_m1416233310_MethodInfo_var);
		Leaderboard_t1185876199 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t4054002952* L_9 = Leaderboard_GetUserFilter_m3119905721(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t1820874799 * L_10 = V_1;
		Il2CppObject * L_11 = ___board0;
		NullCheck(L_11);
		String_t* L_12 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_11);
		Il2CppObject * L_13 = ___board0;
		NullCheck(L_13);
		int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t1185876199 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t4054002952* L_16 = Leaderboard_GetUserFilter_m3119905721(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t1820874799 * L_17 = V_1;
		Il2CppObject * L_18 = ___board0;
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_18);
		Il2CppObject * L_20 = ___board0;
		NullCheck(L_20);
		Range_t1533311935  L_21 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = (&V_2)->get_from_0();
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		Range_t1533311935  L_24 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = (&V_3)->get_count_1();
		Il2CppObject * L_26 = ___board0;
		NullCheck(L_26);
		int32_t L_27 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_26);
		Il2CppObject * L_28 = ___board0;
		NullCheck(L_28);
		int32_t L_29 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m1783152707(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3208733121_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m173797613_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3622080807_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4196185109_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2084830155 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t1820874799 * V_0 = NULL;
	Enumerator_t3208733121  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		List_1_t3189060351 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		NullCheck(L_1);
		Enumerator_t3208733121  L_2 = List_1_GetEnumerator_m173797613(L_1, /*hidden argument*/List_1_GetEnumerator_m173797613_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t1820874799 * L_3 = Enumerator_get_Current_m3622080807((&V_1), /*hidden argument*/Enumerator_get_Current_m3622080807_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t1820874799 * L_4 = V_0;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m100384368(L_4, ((Leaderboard_t1185876199 *)CastclassClass(L_5, Leaderboard_t1185876199_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t1820874799 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m294711596(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m4196185109((&V_1), /*hidden argument*/Enumerator_MoveNext_m4196185109_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t3208733121  L_10 = V_1;
		Enumerator_t3208733121  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3208733121_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral708567292;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4096949980 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m1634439374(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral708567292, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m2437339590 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m302984165 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m4212910653 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m847940592_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m847940592 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m847940592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3066589434 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_users_11();
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m981272890 (GameCenterPlatform_t3570684786 * __this, StringU5BU5D_t4054002952* ___userIds0, Action_1_t3814920354 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3814920354 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3814920354 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_UsersCallback_8(L_2);
		StringU5BU5D_t4054002952* L_3 = ___userIds0;
		GameCenterPlatform_Internal_LoadUsers_m4218820079(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral183309550;
extern Il2CppCodeGenString* _stringLiteral2135574971;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m3650098397 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, Texture2D_t3884108195 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral183309550, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_3 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t2378268441** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_7)), L_8);
		int32_t L_9 = L_8;
		UserProfile_t2280656072 * L_10 = ((*((UserProfileU5BU5D_t2378268441**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3884108195 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m1928130753(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2135574971, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2546851889 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2378268441**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m2295049883 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	{
		Leaderboard_t1185876199 * L_0 = (Leaderboard_t1185876199 *)il2cpp_codegen_object_new(Leaderboard_t1185876199_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m596857571(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t1185876199 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m1828880347 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t344600729 * V_0 = NULL;
	{
		Achievement_t344600729 * L_0 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m3345265521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t344600729 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m1285257317 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m3239514930_MetadataUsageId;
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930 (GcAchievementData_t3481375915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m3239514930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t344600729 * L_6 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m377036415(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t3481375915 * _thisAdjusted = reinterpret_cast<GcAchievementData_t3481375915 *>(__this + 1);
	return GcAchievementData_ToAchievement_m3239514930(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_back(const GcAchievementData_t3481375915_marshaled_pinvoke& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_cleanup(GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_com_back(const GcAchievementData_t3481375915_marshaled_com& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com_cleanup(GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern Il2CppClass* AchievementDescription_t2116066607_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId;
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712 (GcAchievementDescriptionData_t2242891083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Texture2D_t3884108195 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Texture2D_t3884108195 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	Texture2D_t3884108195 * G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	String_t* G_B3_5 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		String_t* L_1 = __this->get_m_Title_1();
		Texture2D_t3884108195 * L_2 = __this->get_m_Image_2();
		String_t* L_3 = __this->get_m_AchievedDescription_3();
		String_t* L_4 = __this->get_m_UnachievedDescription_4();
		int32_t L_5 = __this->get_m_Hidden_5();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = __this->get_m_Points_6();
		AchievementDescription_t2116066607 * L_7 = (AchievementDescription_t2116066607 *)il2cpp_codegen_object_new(AchievementDescription_t2116066607_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m3032164909(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, (bool)G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementDescriptionData_t2242891083 * _thisAdjusted = reinterpret_cast<GcAchievementDescriptionData_t2242891083 *>(__this + 1);
	return GcAchievementDescriptionData_ToAchievementDescription_m3125480712(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_back(const GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_back(const GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  void GcLeaderboard__ctor_m4042810199 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_t1185876199 * L_0 = ___board0;
		__this->set_m_GenericLeaderboard_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C"  void GcLeaderboard_Finalize_m4015840938 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m301614325(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  bool GcLeaderboard_Contains_m100384368 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		Leaderboard_t1185876199 * L_1 = ___board0;
		return (bool)((((Il2CppObject*)(Leaderboard_t1185876199 *)L_0) == ((Il2CppObject*)(Leaderboard_t1185876199 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const uint32_t GcLeaderboard_SetScores_m1734279820_MetadataUsageId;
extern "C"  void GcLeaderboard_SetScores_m1734279820 (GcLeaderboard_t1820874799 * __this, GcScoreDataU5BU5D_t1670395707* ___scoreDatas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcLeaderboard_SetScores_m1734279820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___scoreDatas0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___scoreDatas0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___scoreDatas0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t1185876199 * L_10 = __this->get_m_GenericLeaderboard_1();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m1463319879(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C"  void GcLeaderboard_SetLocalScore_m3970132532 (GcLeaderboard_t1820874799 * __this, GcScoreData_t2181296590  ___scoreData0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		Score_t3396031228 * L_2 = GcScoreData_ToScore_m2728389301((&___scoreData0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m700491556(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C"  void GcLeaderboard_SetMaxRange_m3160374921 (GcLeaderboard_t1820874799 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		uint32_t L_2 = ___maxRange0;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m3779908734(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C"  void GcLeaderboard_SetTitle_m3781988000 (GcLeaderboard_t1820874799 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		Leaderboard_SetTitle_m771163339(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GcLeaderboard_Internal_LoadScores_m1783152707 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___from1, int32_t ___count2, int32_t ___playerScope3, int32_t ___timeScope4, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m1783152707_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m1783152707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m1783152707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category0, ___from1, ___count2, ___playerScope3, ___timeScope4);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C"  void GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___timeScope1, StringU5BU5D_t4054002952* ___userIDs2, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, StringU5BU5D_t4054002952*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category0, ___timeScope1, ___userIDs2);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C"  bool GcLeaderboard_Loading_m294711596 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m294711596_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Loading_m294711596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m294711596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C"  void GcLeaderboard_Dispose_m301614325 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m301614325_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Dispose_m301614325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m301614325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_back(const GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_cleanup(GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_com_back(const GcLeaderboard_t1820874799_marshaled_com& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com_cleanup(GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern const uint32_t GcScoreData_ToScore_m2728389301_MetadataUsageId;
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301 (GcScoreData_t2181296590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcScoreData_ToScore_m2728389301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_Category_0();
		int32_t L_1 = __this->get_m_ValueHigh_2();
		int32_t L_2 = __this->get_m_ValueLow_1();
		String_t* L_3 = __this->get_m_PlayerID_5();
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_Date_3();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = __this->get_m_FormattedValue_4();
		int32_t L_7 = __this->get_m_Rank_6();
		Score_t3396031228 * L_8 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m3768037481(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcScoreData_t2181296590 * _thisAdjusted = reinterpret_cast<GcScoreData_t2181296590 *>(__this + 1);
	return GcScoreData_ToScore_m2728389301(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_back(const GcScoreData_t2181296590_marshaled_pinvoke& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_cleanup(GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_com& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_com_back(const GcScoreData_t2181296590_marshaled_com& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_bstring_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_bstring_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com_cleanup(GcScoreData_t2181296590_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern Il2CppClass* UserProfile_t2280656072_il2cpp_TypeInfo_var;
extern const uint32_t GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId;
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721 (GcUserProfileData_t657441114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = __this->get_userName_0();
		String_t* L_1 = __this->get_userID_1();
		int32_t L_2 = __this->get_isFriend_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t3884108195 * L_3 = __this->get_image_3();
		UserProfile_t2280656072 * L_4 = (UserProfile_t2280656072 *)il2cpp_codegen_object_new(UserProfile_t2280656072_il2cpp_TypeInfo_var);
		UserProfile__ctor_m2682768015(L_4, G_B3_2, G_B3_1, (bool)G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	return GcUserProfileData_ToUserProfile_m1509702721(_thisAdjusted, method);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596528006;
extern const uint32_t GcUserProfileData_AddToArray_m3757655355_MetadataUsageId;
extern "C"  void GcUserProfileData_AddToArray_m3757655355 (GcUserProfileData_t657441114 * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_AddToArray_m3757655355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___number1;
		UserProfile_t2280656072 * L_5 = GcUserProfileData_ToUserProfile_m1509702721(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t2378268441**)L_3)), L_5);
		((*((UserProfileU5BU5D_t2378268441**)L_3)))->SetAt(static_cast<il2cpp_array_size_t>(L_4), (UserProfile_t2280656072 *)L_5);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1596528006, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
extern "C"  void GcUserProfileData_AddToArray_m3757655355_AdjustorThunk (Il2CppObject * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	GcUserProfileData_AddToArray_m3757655355(_thisAdjusted, ___array0, ___number1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_back(const GcUserProfileData_t657441114_marshaled_pinvoke& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_cleanup(GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_com_back(const GcUserProfileData_t657441114_marshaled_com& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com_cleanup(GcUserProfileData_t657441114_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C"  void Achievement__ctor_m377036415 (Achievement_t344600729 * __this, String_t* ___id0, double ___percentCompleted1, bool ___completed2, bool ___hidden3, DateTime_t4283661327  ___lastReportedDate4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percentCompleted1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___completed2;
		__this->set_m_Completed_0(L_2);
		bool L_3 = ___hidden3;
		__this->set_m_Hidden_1(L_3);
		DateTime_t4283661327  L_4 = ___lastReportedDate4;
		__this->set_m_LastReportedDate_2(L_4);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t Achievement__ctor_m2960680429_MetadataUsageId;
extern "C"  void Achievement__ctor_m2960680429 (Achievement_t344600729 * __this, String_t* ___id0, double ___percent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m2960680429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percent1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_Hidden_1((bool)0);
		__this->set_m_Completed_0((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = ((DateTime_t4283661327_StaticFields*)DateTime_t4283661327_il2cpp_TypeInfo_var->static_fields)->get_MinValue_3();
		__this->set_m_LastReportedDate_2(L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral4010126410;
extern const uint32_t Achievement__ctor_m3345265521_MetadataUsageId;
extern "C"  void Achievement__ctor_m3345265521 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m3345265521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m2960680429(__this, _stringLiteral4010126410, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t Achievement_ToString_m2974157186_MetadataUsageId;
extern "C"  String_t* Achievement_ToString_m2974157186 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement_ToString_m2974157186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		String_t* L_1 = Achievement_get_id_m1680539866(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		double L_4 = Achievement_get_percentCompleted_m2492759109(__this, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral32179);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		bool L_9 = Achievement_get_completed_m3689853355(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral32179);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		bool L_14 = Achievement_get_hidden_m2954555244(__this, /*hidden argument*/NULL);
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral32179);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		DateTime_t4283661327  L_19 = Achievement_get_lastReportedDate_m445111052(__this, /*hidden argument*/NULL);
		DateTime_t4283661327  L_20 = L_19;
		Il2CppObject * L_21 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3016520001(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C"  String_t* Achievement_get_id_m1680539866 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C"  void Achievement_set_id_m3123429815 (Achievement_t344600729 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C"  double Achievement_get_percentCompleted_m2492759109 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CpercentCompletedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C"  void Achievement_set_percentCompleted_m1005987436 (Achievement_t344600729 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpercentCompletedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C"  bool Achievement_get_completed_m3689853355 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Completed_0();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C"  bool Achievement_get_hidden_m2954555244 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_1();
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C"  DateTime_t4283661327  Achievement_get_lastReportedDate_m445111052 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_m_LastReportedDate_2();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C"  void AchievementDescription__ctor_m3032164909 (AchievementDescription_t2116066607 * __this, String_t* ___id0, String_t* ___title1, Texture2D_t3884108195 * ___image2, String_t* ___achievedDescription3, String_t* ___unachievedDescription4, bool ___hidden5, int32_t ___points6, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		AchievementDescription_set_id_m2215766207(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___title1;
		__this->set_m_Title_0(L_1);
		Texture2D_t3884108195 * L_2 = ___image2;
		__this->set_m_Image_1(L_2);
		String_t* L_3 = ___achievedDescription3;
		__this->set_m_AchievedDescription_2(L_3);
		String_t* L_4 = ___unachievedDescription4;
		__this->set_m_UnachievedDescription_3(L_4);
		bool L_5 = ___hidden5;
		__this->set_m_Hidden_4(L_5);
		int32_t L_6 = ___points6;
		__this->set_m_Points_5(L_6);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t AchievementDescription_ToString_m1633092820_MetadataUsageId;
extern "C"  String_t* AchievementDescription_ToString_m1633092820 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AchievementDescription_ToString_m1633092820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		String_t* L_1 = AchievementDescription_get_id_m3162941612(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = AchievementDescription_get_title_m1294089737(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		String_t* L_7 = AchievementDescription_get_achievedDescription_m4248956218(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral32179);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = AchievementDescription_get_unachievedDescription_m3429874753(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral32179);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		int32_t L_13 = AchievementDescription_get_points_m4158769271(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral32179);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		bool L_18 = AchievementDescription_get_hidden_m734162136(__this, /*hidden argument*/NULL);
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C"  void AchievementDescription_SetImage_m1092175896 (AchievementDescription_t2116066607 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_1(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C"  String_t* AchievementDescription_get_id_m3162941612 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C"  void AchievementDescription_set_id_m2215766207 (AchievementDescription_t2116066607 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C"  String_t* AchievementDescription_get_title_m1294089737 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Title_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C"  String_t* AchievementDescription_get_achievedDescription_m4248956218 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_AchievedDescription_2();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C"  String_t* AchievementDescription_get_unachievedDescription_m3429874753 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UnachievedDescription_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C"  bool AchievementDescription_get_hidden_m734162136 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_4();
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C"  int32_t AchievementDescription_get_points_m4158769271 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Points_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3624438231;
extern const uint32_t Leaderboard__ctor_m596857571_MetadataUsageId;
extern "C"  void Leaderboard__ctor_m596857571 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard__ctor_m596857571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_set_id_m2022535593(__this, _stringLiteral3624438231, /*hidden argument*/NULL);
		Range_t1533311935  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Range__ctor_m872630735(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		Leaderboard_set_range_m2830170470(__this, L_0, /*hidden argument*/NULL);
		Leaderboard_set_userScope_m3914830286(__this, 0, /*hidden argument*/NULL);
		Leaderboard_set_timeScope_m3669793618(__this, 2, /*hidden argument*/NULL);
		__this->set_m_Loading_0((bool)0);
		Score_t3396031228 * L_1 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m113497156(L_1, _stringLiteral3624438231, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->set_m_LocalUserScore_1(L_1);
		__this->set_m_MaxRange_2(0);
		__this->set_m_Scores_3((IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Title_4(_stringLiteral3624438231);
		__this->set_m_UserIDs_5(((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UserScope_t1608660171_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeScope_t1305796361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69499590;
extern Il2CppCodeGenString* _stringLiteral1411513442;
extern Il2CppCodeGenString* _stringLiteral1472989374;
extern Il2CppCodeGenString* _stringLiteral3534372753;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral2271991173;
extern Il2CppCodeGenString* _stringLiteral778717479;
extern Il2CppCodeGenString* _stringLiteral4163059537;
extern Il2CppCodeGenString* _stringLiteral978617427;
extern Il2CppCodeGenString* _stringLiteral1548524389;
extern const uint32_t Leaderboard_ToString_m114482384_MetadataUsageId;
extern "C"  String_t* Leaderboard_ToString_m114482384 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_ToString_m114482384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Range_t1533311935  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Range_t1533311935  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral69499590);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral69499590);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		String_t* L_2 = Leaderboard_get_id_m2379239336(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1411513442);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1411513442);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = __this->get_m_Title_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1472989374);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1472989374);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		bool L_8 = __this->get_m_Loading_0();
		bool L_9 = L_8;
		Il2CppObject * L_10 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral3534372753);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3534372753);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Range_t1533311935  L_13 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&V_0)->get_from_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral44);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		Range_t1533311935  L_19 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = (&V_1)->get_count_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral2271991173);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2271991173);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		uint32_t L_25 = __this->get_m_MaxRange_2();
		uint32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral778717479);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral778717479);
		ObjectU5BU5D_t1108656482* L_29 = L_28;
		IScoreU5BU5D_t250104726* L_30 = __this->get_m_Scores_3();
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))));
		Il2CppObject * L_32 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_32);
		ObjectU5BU5D_t1108656482* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral4163059537);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral4163059537);
		ObjectU5BU5D_t1108656482* L_34 = L_33;
		int32_t L_35 = Leaderboard_get_userScope_m3547770469(__this, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(UserScope_t1608660171_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral978617427);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral978617427);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		int32_t L_40 = Leaderboard_get_timeScope_m2356081377(__this, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(TimeScope_t1305796361_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_42);
		ObjectU5BU5D_t1108656482* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral1548524389);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1548524389);
		ObjectU5BU5D_t1108656482* L_44 = L_43;
		StringU5BU5D_t4054002952* L_45 = __this->get_m_UserIDs_5();
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))));
		Il2CppObject * L_47 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C"  void Leaderboard_SetLocalUserScore_m700491556 (Leaderboard_t1185876199 * __this, Il2CppObject * ___score0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___score0;
		__this->set_m_LocalUserScore_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C"  void Leaderboard_SetMaxRange_m3779908734 (Leaderboard_t1185876199 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange0;
		__this->set_m_MaxRange_2(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C"  void Leaderboard_SetScores_m1463319879 (Leaderboard_t1185876199 * __this, IScoreU5BU5D_t250104726* ___scores0, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t250104726* L_0 = ___scores0;
		__this->set_m_Scores_3(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C"  void Leaderboard_SetTitle_m771163339 (Leaderboard_t1185876199 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_m_Title_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C"  StringU5BU5D_t4054002952* Leaderboard_GetUserFilter_m3119905721 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_m_UserIDs_5();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C"  String_t* Leaderboard_get_id_m2379239336 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C"  void Leaderboard_set_id_m2022535593 (Leaderboard_t1185876199 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C"  int32_t Leaderboard_get_userScope_m3547770469 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CuserScopeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void Leaderboard_set_userScope_m3914830286 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuserScopeU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C"  Range_t1533311935  Leaderboard_get_range_m234965965 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = __this->get_U3CrangeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void Leaderboard_set_range_m2830170470 (Leaderboard_t1185876199 * __this, Range_t1533311935  ___value0, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C"  int32_t Leaderboard_get_timeScope_m2356081377 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CtimeScopeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void Leaderboard_set_timeScope_m3669793618 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CtimeScopeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t LocalUser__ctor_m1052633066_MetadataUsageId;
extern "C"  void LocalUser__ctor_m1052633066 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalUser__ctor_m1052633066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m1280449570(__this, /*hidden argument*/NULL);
		__this->set_m_Friends_5((IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Authenticated_6((bool)0);
		__this->set_m_Underage_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C"  void LocalUser_SetFriends_m3475409220 (LocalUser_t1307362368 * __this, IUserProfileU5BU5D_t3419104218* ___friends0, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t3419104218* L_0 = ___friends0;
		__this->set_m_Friends_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C"  void LocalUser_SetAuthenticated_m653377406 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Authenticated_6(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C"  void LocalUser_SetUnderage_m2968368872 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Underage_7(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C"  bool LocalUser_get_authenticated_m3657159816 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Authenticated_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t Score__ctor_m113497156_MetadataUsageId;
extern "C"  void Score__ctor_m113497156 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score__ctor_m113497156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int64_t L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Score__ctor_m3768037481(__this, L_0, L_1, _stringLiteral48, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C"  void Score__ctor_m3768037481 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, String_t* ___userID2, DateTime_t4283661327  ___date3, String_t* ___formattedValue4, int32_t ___rank5, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID0;
		Score_set_leaderboardID_m3646875387(__this, L_0, /*hidden argument*/NULL);
		int64_t L_1 = ___value1;
		Score_set_value_m2229956626(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___userID2;
		__this->set_m_UserID_2(L_2);
		DateTime_t4283661327  L_3 = ___date3;
		__this->set_m_Date_0(L_3);
		String_t* L_4 = ___formattedValue4;
		__this->set_m_FormattedValue_1(L_4);
		int32_t L_5 = ___rank5;
		__this->set_m_Rank_3(L_5);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2642717173;
extern Il2CppCodeGenString* _stringLiteral1871350441;
extern Il2CppCodeGenString* _stringLiteral188508522;
extern Il2CppCodeGenString* _stringLiteral3481064940;
extern Il2CppCodeGenString* _stringLiteral1272038458;
extern const uint32_t Score_ToString_m3380639973_MetadataUsageId;
extern "C"  String_t* Score_ToString_m3380639973 (Score_t3396031228 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score_ToString_m3380639973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2642717173);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2642717173);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		int32_t L_2 = __this->get_m_Rank_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1871350441);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1871350441);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int64_t L_7 = Score_get_value_m3381234835(__this, /*hidden argument*/NULL);
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral188508522);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral188508522);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		String_t* L_12 = Score_get_leaderboardID_m1097777176(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_12);
		ObjectU5BU5D_t1108656482* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral3481064940);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3481064940);
		ObjectU5BU5D_t1108656482* L_14 = L_13;
		String_t* L_15 = __this->get_m_UserID_2();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral1272038458);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral1272038458);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		DateTime_t4283661327  L_18 = __this->get_m_Date_0();
		DateTime_t4283661327  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C"  String_t* Score_get_leaderboardID_m1097777176 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CleaderboardIDU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C"  void Score_set_leaderboardID_m3646875387 (Score_t3396031228 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CleaderboardIDU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C"  int64_t Score_get_value_m3381234835 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CvalueU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C"  void Score_set_value_m2229956626 (Score_t3396031228 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3378765435;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t UserProfile__ctor_m1280449570_MetadataUsageId;
extern "C"  void UserProfile__ctor_m1280449570 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile__ctor_m1280449570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_m_UserName_0(_stringLiteral3378765435);
		__this->set_m_ID_1(_stringLiteral48);
		__this->set_m_IsFriend_2((bool)0);
		__this->set_m_State_3(3);
		Texture2D_t3884108195 * L_0 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C"  void UserProfile__ctor_m2682768015 (UserProfile_t2280656072 * __this, String_t* ___name0, String_t* ___id1, bool ___friend2, int32_t ___state3, Texture2D_t3884108195 * ___image4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		String_t* L_1 = ___id1;
		__this->set_m_ID_1(L_1);
		bool L_2 = ___friend2;
		__this->set_m_IsFriend_2(L_2);
		int32_t L_3 = ___state3;
		__this->set_m_State_3(L_3);
		Texture2D_t3884108195 * L_4 = ___image4;
		__this->set_m_Image_4(L_4);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* UserState_t1609153288_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t UserProfile_ToString_m2563774257_MetadataUsageId;
extern "C"  String_t* UserProfile_ToString_m2563774257 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_ToString_m2563774257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = UserProfile_get_id_m2095754825(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = UserProfile_get_userName_m3149753764(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		bool L_7 = UserProfile_get_isFriend_m1712941209(__this, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral32179);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		int32_t L_12 = UserProfile_get_state_m1340538601(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(UserState_t1609153288_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C"  void UserProfile_SetUserName_m914181770 (UserProfile_t2280656072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C"  void UserProfile_SetUserID_m1515238170 (UserProfile_t2280656072 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		__this->set_m_ID_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C"  void UserProfile_SetImage_m1928130753 (UserProfile_t2280656072 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C"  String_t* UserProfile_get_userName_m3149753764 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UserName_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C"  String_t* UserProfile_get_id_m2095754825 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ID_1();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C"  bool UserProfile_get_isFriend_m1712941209 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsFriend_2();
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C"  int32_t UserProfile_get_state_m1340538601 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_State_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C"  void Range__ctor_m872630735 (Range_t1533311935 * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue0;
		__this->set_from_0(L_0);
		int32_t L_1 = ___valueCount1;
		__this->set_count_1(L_1);
		return;
	}
}
extern "C"  void Range__ctor_m872630735_AdjustorThunk (Il2CppObject * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	Range_t1533311935 * _thisAdjusted = reinterpret_cast<Range_t1533311935 *>(__this + 1);
	Range__ctor_m872630735(_thisAdjusted, ___fromValue0, ___valueCount1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_pinvoke(const Range_t1533311935& unmarshaled, Range_t1533311935_marshaled_pinvoke& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t1533311935_marshal_pinvoke_back(const Range_t1533311935_marshaled_pinvoke& marshaled, Range_t1533311935& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_pinvoke_cleanup(Range_t1533311935_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_com(const Range_t1533311935& unmarshaled, Range_t1533311935_marshaled_com& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t1533311935_marshal_com_back(const Range_t1533311935_marshaled_com& marshaled, Range_t1533311935& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_com_cleanup(Range_t1533311935_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility__cctor_m1486031939_MetadataUsageId;
extern "C"  void StackTraceUtility__cctor_m1486031939 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m1486031939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_SetProjectFolder_m3541316899_MetadataUsageId;
extern "C"  void StackTraceUtility_SetProjectFolder_m3541316899 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m3541316899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern Il2CppClass* StackTrace_t1047871261_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m235366505_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m235366505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m235366505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1047871261 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		StackTrace_t1047871261 * L_0 = (StackTrace_t1047871261 *)il2cpp_codegen_object_new(StackTrace_t1047871261_il2cpp_TypeInfo_var);
		StackTrace__ctor_m449371190(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1047871261 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m3996939365(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m1382284457(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4118889292;
extern Il2CppCodeGenString* _stringLiteral108037399;
extern Il2CppCodeGenString* _stringLiteral4222249919;
extern Il2CppCodeGenString* _stringLiteral235002098;
extern Il2CppCodeGenString* _stringLiteral2264296596;
extern Il2CppCodeGenString* _stringLiteral4231872978;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m295158192_MetadataUsageId;
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m295158192 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m295158192_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1500793453(L_1, _stringLiteral4118889292, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1500793453(L_3, _stringLiteral108037399, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1500793453(L_5, _stringLiteral4222249919, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1500793453(L_7, _stringLiteral235002098, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1500793453(L_9, _stringLiteral2264296596, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1500793453(L_11, _stringLiteral4231872978, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return (bool)G_B7_0;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTrace_t1047871261_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3176792989;
extern Il2CppCodeGenString* _stringLiteral4051978937;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral1830;
extern Il2CppCodeGenString* _stringLiteral3343166721;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m803331050_MetadataUsageId;
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m803331050 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m803331050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t1047871261 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, _stringLiteral3176792989, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t3991598821 *)IsInstClass(L_2, Exception_t3991598821_il2cpp_TypeInfo_var));
		Exception_t3991598821 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t928607144 * L_4 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_4, _stringLiteral4051978937, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		Exception_t3991598821 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t3991598821 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t243639308 * L_10 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)L_12);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t3991598821 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t3991598821 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1825781833(NULL /*static, unused*/, L_19, _stringLiteral10, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t3991598821 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = Exception_GetType_m913902486(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_25;
		Exception_t3991598821 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t3991598821 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m1030489823(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m2979997331(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m138640077(NULL /*static, unused*/, L_33, _stringLiteral1830, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m138640077(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message1;
		String_t* L_39 = V_3;
		*((Il2CppObject **)(L_38)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_38), (Il2CppObject *)L_39);
		Exception_t3991598821 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t3991598821 * L_41 = Exception_get_InnerException_m1427945535(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral3343166721, L_42, _stringLiteral10, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t3991598821 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t3991598821 * L_46 = Exception_get_InnerException_m1427945535(L_45, /*hidden argument*/NULL);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t3991598821 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t243639308 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m138640077(NULL /*static, unused*/, L_49, _stringLiteral10, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m3898090075(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t1047871261 * L_51 = (StackTrace_t1047871261 *)il2cpp_codegen_object_new(StackTrace_t1047871261_il2cpp_TypeInfo_var);
		StackTrace__ctor_m449371190(L_51, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t243639308 * L_52 = V_1;
		StackTrace_t1047871261 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m3996939365(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m3898090075(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace2;
		StringBuilder_t243639308 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m350379841(L_56, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_55)) = (Il2CppObject *)L_57;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_55), (Il2CppObject *)L_57);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3162923440;
extern Il2CppCodeGenString* _stringLiteral3880156305;
extern Il2CppCodeGenString* _stringLiteral994875;
extern Il2CppCodeGenString* _stringLiteral1444255587;
extern Il2CppCodeGenString* _stringLiteral3385676414;
extern Il2CppCodeGenString* _stringLiteral2053922246;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral96845;
extern Il2CppCodeGenString* _stringLiteral89059;
extern Il2CppCodeGenString* _stringLiteral1627007362;
extern Il2CppCodeGenString* _stringLiteral30610331;
extern Il2CppCodeGenString* _stringLiteral30841157;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m556856210_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m556856210 (Il2CppObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m556856210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t3324145743* L_3 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t4054002952* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_7 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t4054002952* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t4054002952* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m1030489823(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_16 = V_2;
		StringU5BU5D_t4054002952* L_17 = V_0;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t4054002952* L_18 = V_0;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_4 = L_21;
		String_t* L_22 = V_4;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m2979997331(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m3015341861(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_26 = V_4;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m1500793453(L_26, _stringLiteral3162923440, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_29 = V_4;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m1500793453(L_29, _stringLiteral3880156305, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_32 = V_3;
		StringU5BU5D_t4054002952* L_33 = V_0;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_34 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m295158192(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t4054002952* L_36 = V_0;
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m295158192(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_41 = V_4;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m1476794331(L_41, _stringLiteral994875, /*hidden argument*/NULL);
		V_5 = L_42;
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_44 = V_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m675079568(L_44, 0, L_45, /*hidden argument*/NULL);
		V_4 = L_46;
	}

IL_00fa:
	{
		String_t* L_47 = V_4;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m1476794331(L_47, _stringLiteral1444255587, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_49 = V_4;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m1476794331(L_49, _stringLiteral3385676414, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m1476794331(L_51, _stringLiteral2053922246, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_54 = V_4;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m1500793453(L_54, _stringLiteral91, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_56 = V_4;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m2265568550(L_56, _stringLiteral93, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_58 = V_4;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m1500793453(L_58, _stringLiteral96845, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_60 = V_4;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m242090629(L_60, 0, 3, /*hidden argument*/NULL);
		V_4 = L_61;
	}

IL_0188:
	{
		String_t* L_62 = V_4;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m1476794331(L_62, _stringLiteral89059, /*hidden argument*/NULL);
		V_6 = L_63;
		V_7 = (-1);
		int32_t L_64 = V_6;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_65 = V_4;
		int32_t L_66 = V_6;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m1991631068(L_65, _stringLiteral93, L_66, /*hidden argument*/NULL);
		V_7 = L_67;
	}

IL_01b1:
	{
		int32_t L_68 = V_6;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_69 = V_7;
		int32_t L_70 = V_6;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_71 = V_4;
		int32_t L_72 = V_6;
		int32_t L_73 = V_7;
		int32_t L_74 = V_6;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m242090629(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_75;
	}

IL_01d4:
	{
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_76);
		String_t* L_78 = String_Replace_m2915759397(L_76, _stringLiteral1627007362, L_77, /*hidden argument*/NULL);
		V_4 = L_78;
		String_t* L_79 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_80 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_79);
		String_t* L_82 = String_Replace_m2915759397(L_79, L_80, L_81, /*hidden argument*/NULL);
		V_4 = L_82;
		String_t* L_83 = V_4;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m3369701083(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_84;
		String_t* L_85 = V_4;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m2747144337(L_85, _stringLiteral30610331, /*hidden argument*/NULL);
		V_8 = L_86;
		int32_t L_87 = V_8;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m242090629(L_88, L_89, 5, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		int32_t L_92 = V_8;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m3926397187(L_91, L_92, _stringLiteral30841157, /*hidden argument*/NULL);
		V_4 = L_93;
		String_t* L_94 = V_4;
		String_t* L_95 = V_4;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m2979997331(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m3926397187(L_94, L_96, _stringLiteral41, /*hidden argument*/NULL);
		V_4 = L_97;
	}

IL_024e:
	{
		StringBuilder_t243639308 * L_98 = V_1;
		String_t* L_99 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m138640077(NULL /*static, unused*/, L_99, _stringLiteral10, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m3898090075(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_101 = V_3;
		V_3 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_102 = V_3;
		StringU5BU5D_t4054002952* L_103 = V_0;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_103)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t243639308 * L_104 = V_1;
		NullCheck(L_104);
		String_t* L_105 = StringBuilder_ToString_m350379841(L_104, /*hidden argument*/NULL);
		return L_105;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral65906227;
extern Il2CppCodeGenString* _stringLiteral3328621047;
extern Il2CppCodeGenString* _stringLiteral2281497008;
extern Il2CppCodeGenString* _stringLiteral3703338489;
extern Il2CppCodeGenString* _stringLiteral1970626406;
extern Il2CppCodeGenString* _stringLiteral3747019304;
extern Il2CppCodeGenString* _stringLiteral30841157;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m3996939365_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m3996939365 (Il2CppObject * __this /* static, unused */, StackTrace_t1047871261 * ___stackTrace0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m3996939365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t1034942277 * V_2 = NULL;
	MethodBase_t318515428 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t2015293532* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	int32_t G_B24_0 = 0;
	int32_t G_B26_0 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0257;
	}

IL_0012:
	{
		StackTrace_t1047871261 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1034942277 * L_3 = VirtFuncInvoker1< StackFrame_t1034942277 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1034942277 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t318515428 * L_5 = VirtFuncInvoker0< MethodBase_t318515428 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t318515428 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0253;
	}

IL_002c:
	{
		MethodBase_t318515428 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0253;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t243639308 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral46, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t243639308 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3898090075(L_21, _stringLiteral58, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_22 = V_0;
		MethodBase_t318515428 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m3898090075(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3898090075(L_25, _stringLiteral40, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t318515428 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t2015293532* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t243639308 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3898090075(L_29, _stringLiteral1396, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = (bool)0;
	}

IL_00d2:
	{
		StringBuilder_t243639308 * L_30 = V_0;
		ParameterInfoU5BU5D_t2015293532* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		ParameterInfo_t2235474049 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t2015293532* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t243639308 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m3898090075(L_40, _stringLiteral41, /*hidden argument*/NULL);
		StackFrame_t1034942277 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_0247;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_45, _stringLiteral65906227, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_48, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01c4;
		}
	}

IL_0140:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_51, _stringLiteral2281497008, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_54, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_01c4;
		}
	}

IL_016c:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_57, _stringLiteral3703338489, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0198;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_60, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_01c4;
		}
	}

IL_0198:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_63, _stringLiteral1970626406, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01c1;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_66, _stringLiteral3747019304, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)(L_67));
		goto IL_01c2;
	}

IL_01c1:
	{
		G_B24_0 = 0;
	}

IL_01c2:
	{
		G_B26_0 = G_B24_0;
		goto IL_01c5;
	}

IL_01c4:
	{
		G_B26_0 = 1;
	}

IL_01c5:
	{
		V_10 = (bool)G_B26_0;
		bool L_68 = V_10;
		if (L_68)
		{
			goto IL_0247;
		}
	}
	{
		StringBuilder_t243639308 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m3898090075(L_69, _stringLiteral30841157, /*hidden argument*/NULL);
		String_t* L_70 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_71 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_70);
		bool L_72 = String_StartsWith_m1500793453(L_70, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_0210;
		}
	}
	{
		String_t* L_73 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_74 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_74);
		int32_t L_75 = String_get_Length_m2979997331(L_74, /*hidden argument*/NULL);
		String_t* L_76 = V_9;
		NullCheck(L_76);
		int32_t L_77 = String_get_Length_m2979997331(L_76, /*hidden argument*/NULL);
		String_t* L_78 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_78);
		int32_t L_79 = String_get_Length_m2979997331(L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_80 = String_Substring_m675079568(L_73, L_75, ((int32_t)((int32_t)L_77-(int32_t)L_79)), /*hidden argument*/NULL);
		V_9 = L_80;
	}

IL_0210:
	{
		StringBuilder_t243639308 * L_81 = V_0;
		String_t* L_82 = V_9;
		NullCheck(L_81);
		StringBuilder_Append_m3898090075(L_81, L_82, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_83 = V_0;
		NullCheck(L_83);
		StringBuilder_Append_m3898090075(L_83, _stringLiteral58, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_84 = V_0;
		StackFrame_t1034942277 * L_85 = V_2;
		NullCheck(L_85);
		int32_t L_86 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_85);
		V_11 = L_86;
		String_t* L_87 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		NullCheck(L_84);
		StringBuilder_Append_m3898090075(L_84, L_87, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_88 = V_0;
		NullCheck(L_88);
		StringBuilder_Append_m3898090075(L_88, _stringLiteral41, /*hidden argument*/NULL);
	}

IL_0247:
	{
		StringBuilder_t243639308 * L_89 = V_0;
		NullCheck(L_89);
		StringBuilder_Append_m3898090075(L_89, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_0253:
	{
		int32_t L_90 = V_1;
		V_1 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_0257:
	{
		int32_t L_91 = V_1;
		StackTrace_t1047871261 * L_92 = ___stackTrace0;
		NullCheck(L_92);
		int32_t L_93 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_92);
		if ((((int32_t)L_91) < ((int32_t)L_93)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t243639308 * L_94 = V_0;
		NullCheck(L_94);
		String_t* L_95 = StringBuilder_ToString_m350379841(L_94, /*hidden argument*/NULL);
		return L_95;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C"  void StateMachineBehaviour__ctor_m3149540562 (StateMachineBehaviour_t759180893 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateEnter_m2858660338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m2829799871 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateExit_m922939338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMove_m834394999 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateIK_m2478904358 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m3756430466 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m4198306608 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateEnter_m1932811376 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m2812313539 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateExit_m2956750232 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMove_m2938949899 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateIK_m3126912700 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m2392556512 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t3906681469 * ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m2976228338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t3906681469 * ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor__ctor_m1029296947_MetadataUsageId;
extern "C"  void TextEditor__ctor_m1029296947 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m1029296947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_0 = GUIStyle_get_none_m2767659632(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_style_0(L_0);
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_1(L_1);
		GUIContent_t2094828418 * L_2 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m923375087(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_2(L_2);
		__this->set_m_iAltCursorPos_3((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m516856734 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m1143336192 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m1143336192_ftn) (Texture_t2526458961 *);
	static Texture_Internal_GetWidth_m1143336192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m1143336192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m1065663213 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m1065663213_ftn) (Texture_t2526458961 *);
	static Texture_Internal_GetHeight_m1065663213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m1065663213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m1557399609 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m1143336192(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4188032821;
extern const uint32_t Texture_set_width_m3343740566_MetadataUsageId;
extern "C"  void Texture_set_width_m3343740566 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_width_m3343740566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t3991598821 * L_0 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral4188032821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m1538561974 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m1065663213(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4188032821;
extern const uint32_t Texture_set_height_m1897594619_MetadataUsageId;
extern "C"  void Texture_set_height_m1897594619 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_height_m1897594619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t3991598821 * L_0 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral4188032821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3842701708 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Texture_set_filterMode_m3842701708_ftn) (Texture_t2526458961 *, int32_t);
	static Texture_set_filterMode_m3842701708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m3842701708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
extern "C"  void Texture_set_anisoLevel_m1894923584 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Texture_set_anisoLevel_m1894923584_ftn) (Texture_t2526458961 *, int32_t);
	static Texture_set_anisoLevel_m1894923584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_anisoLevel_m1894923584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_anisoLevel(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m3720633937 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Texture_set_wrapMode_m3720633937_ftn) (Texture_t2526458961 *, int32_t);
	static Texture_set_wrapMode_m3720633937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m3720633937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.IntPtr UnityEngine.Texture::GetNativeTexturePtr()
extern "C"  IntPtr_t Texture_GetNativeTexturePtr_m2052913073 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	typedef IntPtr_t (*Texture_GetNativeTexturePtr_m2052913073_ftn) (Texture_t2526458961 *);
	static Texture_GetNativeTexturePtr_m2052913073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_GetNativeTexturePtr_m2052913073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::GetNativeTexturePtr()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m1883511258_MetadataUsageId;
extern "C"  void Texture2D__ctor_m1883511258 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m1883511258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m457592211(NULL /*static, unused*/, __this, L_0, L_1, 5, (bool)1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m3705883154_MetadataUsageId;
extern "C"  void Texture2D__ctor_m3705883154 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m3705883154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m457592211(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (bool)0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D__ctor_m467773817 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, IntPtr_t ___nativeTex5, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		bool L_4 = ___linear4;
		IntPtr_t L_5 = ___nativeTex5;
		Texture2D_Internal_Create_m457592211(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m457592211 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, IntPtr_t ___nativeTex6, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m457592211_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m457592211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m457592211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D_CreateExternalTexture_m1858440074_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * Texture2D_CreateExternalTexture_m1858440074 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, IntPtr_t ___nativeTex5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D_CreateExternalTexture_m1858440074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		bool L_4 = ___linear4;
		IntPtr_t L_5 = ___nativeTex5;
		Texture2D_t3884108195 * L_6 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m467773817(L_6, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern "C"  int32_t Texture2D_get_format_m863889216 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture2D_get_format_m863889216_ftn) (Texture2D_t3884108195 *);
	static Texture2D_get_format_m863889216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_format_m863889216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_format()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m1289331147 (Texture2D_t3884108195 * __this, ColorU5BU5D_t2441545636* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t2441545636* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m2710961388(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m2710961388 (Texture2D_t3884108195 * __this, ColorU5BU5D_t2441545636* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel1;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel1;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t2441545636* L_8 = ___colors0;
		int32_t L_9 = ___miplevel1;
		Texture2D_SetPixels_m3304189612(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m3304189612 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t2441545636* ___colors4, int32_t ___miplevel5, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m3304189612_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t2441545636*, int32_t);
	static Texture2D_SetPixels_m3304189612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m3304189612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2090735797 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	typedef void (*Texture2D_SetAllPixels32_m2090735797_ftn) (Texture2D_t3884108195 *, Color32U5BU5D_t2960766953*, int32_t);
	static Texture2D_SetAllPixels32_m2090735797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m2090735797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m1350049485 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t2960766953* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m4259722026(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m4259722026 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	{
		Color32U5BU5D_t2960766953* L_0 = ___colors0;
		int32_t L_1 = ___miplevel1;
		Texture2D_SetAllPixels32_m2090735797(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m1759701362 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		ColorU5BU5D_t2441545636* L_1 = Texture2D_GetPixels_m3014919107(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m3014919107 (Texture2D_t3884108195 * __this, int32_t ___miplevel0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel0;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___miplevel0;
		ColorU5BU5D_t2441545636* L_9 = Texture2D_GetPixels_m871852803(__this, 0, 0, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m871852803 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const MethodInfo* method)
{
	typedef ColorU5BU5D_t2441545636* (*Texture2D_GetPixels_m871852803_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, int32_t, int32_t);
	static Texture2D_GetPixels_m871852803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels_m871852803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)");
	return _il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___miplevel4);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern "C"  Color32U5BU5D_t2960766953* Texture2D_GetPixels32_m327121251 (Texture2D_t3884108195 * __this, int32_t ___miplevel0, const MethodInfo* method)
{
	typedef Color32U5BU5D_t2960766953* (*Texture2D_GetPixels32_m327121251_ftn) (Texture2D_t3884108195 *, int32_t);
	static Texture2D_GetPixels32_m327121251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels32_m327121251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels32(System.Int32)");
	return _il2cpp_icall_func(__this, ___miplevel0);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C"  Color32U5BU5D_t2960766953* Texture2D_GetPixels32_m4076457746 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Color32U5BU5D_t2960766953* L_1 = Texture2D_GetPixels32_m327121251(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m2754532430 (Texture2D_t3884108195 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m2754532430_ftn) (Texture2D_t3884108195 *, bool, bool);
	static Texture2D_Apply_m2754532430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m2754532430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m1364130776 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m2754532430(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  bool Texture2D_Resize_m1334821900 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___hasMipMap3, const MethodInfo* method)
{
	typedef bool (*Texture2D_Resize_m1334821900_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, bool);
	static Texture2D_Resize_m1334821900_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Resize_m1334821900_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)");
	return _il2cpp_icall_func(__this, ___width0, ___height1, ___format2, ___hasMipMap3);
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern "C"  void Texture2D_ReadPixels_m2828402557 (Texture2D_t3884108195 * __this, Rect_t4241904616  ___source0, int32_t ___destX1, int32_t ___destY2, bool ___recalculateMipMaps3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___destX1;
		int32_t L_1 = ___destY2;
		bool L_2 = ___recalculateMipMaps3;
		Texture2D_INTERNAL_CALL_ReadPixels_m388307610(NULL /*static, unused*/, __this, (&___source0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern "C"  void Texture2D_INTERNAL_CALL_ReadPixels_m388307610 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___self0, Rect_t4241904616 * ___source1, int32_t ___destX2, int32_t ___destY3, bool ___recalculateMipMaps4, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn) (Texture2D_t3884108195 *, Rect_t4241904616 *, int32_t, int32_t, bool);
	static Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___self0, ___source1, ___destX2, ___destY3, ___recalculateMipMaps4);
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2741110510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m2741110510_ftn) ();
	static Time_get_deltaTime_m2741110510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m2741110510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m3434184975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m3434184975_ftn) ();
	static Time_get_frameCount_m3434184975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m3434184975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m2972554983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m2972554983_ftn) ();
	static Time_get_realtimeSinceStartup_m2972554983_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m2972554983_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t4282066566  Transform_get_position_m2211398607 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_position_m1705230066(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3111394108 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m1126232166(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m1705230066 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m1705230066_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_position_m1705230066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m1705230066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1126232166 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m1126232166_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_position_m1126232166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m1126232166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t4282066566  Transform_get_localPosition_m668140784 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localPosition_m2703574131(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3504330903 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m221305727(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m2703574131 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m2703574131_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_localPosition_m2703574131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m2703574131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m221305727 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m221305727_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_localPosition_m221305727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m221305727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t4282066566  Transform_get_right_m2070836824 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t4282066566  Transform_get_forward_m877665793 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t1553702882  Transform_get_rotation_m11483428 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_rotation_m2389720173(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m1525803229 (Transform_t1659122786 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m2051942009(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m2389720173 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m2389720173_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_get_rotation_m2389720173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m2389720173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2051942009 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m2051942009_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_set_rotation_m2051942009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m2051942009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t1553702882  Transform_get_localRotation_m3343229381 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localRotation_m1409235788(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m3719981474 (Transform_t1659122786 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m2898114752(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m1409235788 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m1409235788_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_get_localRotation_m1409235788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m1409235788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m2898114752 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m2898114752_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_set_localRotation_m2898114752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m2898114752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t4282066566  Transform_get_localScale_m3886572677 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localScale_m1534477480(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m310756934 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m3463244060(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m1534477480 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m1534477480_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_localScale_m1534477480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m1534477480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m3463244060 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m3463244060_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_localScale_m3463244060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m3463244060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t1659122786 * Transform_get_parent_m2236876972 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Transform_get_parentInternal_m3763475785(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral509676363;
extern const uint32_t Transform_set_parent_m3231272063_MetadataUsageId;
extern "C"  void Transform_set_parent_m3231272063 (Transform_t1659122786 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_parent_m3231272063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t972643934 *)IsInstSealed(__this, RectTransform_t972643934_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, _stringLiteral509676363, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t1659122786 * L_0 = ___value0;
		Transform_set_parentInternal_m1254352386(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t1659122786 * Transform_get_parentInternal_m3763475785 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_get_parentInternal_m3763475785_ftn) (Transform_t1659122786 *);
	static Transform_get_parentInternal_m3763475785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m3763475785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m1254352386 (Transform_t1659122786 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m1254352386_ftn) (Transform_t1659122786 *, Transform_t1659122786 *);
	static Transform_set_parentInternal_m1254352386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m1254352386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t1651859333  Transform_get_localToWorldMatrix_m3571020210 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localToWorldMatrix_m3747334677(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m3747334677 (Transform_t1659122786 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn) (Transform_t1659122786 *, Matrix4x4_t1651859333 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_TransformPoint_m437395512 (Transform_t1659122786 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_TransformPoint_m1435510371(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m1435510371 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_InverseTransformPoint_m1626812000 (Transform_t1659122786 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C"  Transform_t1659122786 * Transform_get_root_m1064615716 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_get_root_m1064615716_ftn) (Transform_t1659122786 *);
	static Transform_get_root_m1064615716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m1064615716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2107810675 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m2107810675_ftn) (Transform_t1659122786 *);
	static Transform_get_childCount_m2107810675_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m2107810675_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t1659122786 * Transform_Find_m3950449392 (Transform_t1659122786 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_Find_m3950449392_ftn) (Transform_t1659122786 *, String_t*);
	static Transform_Find_m3950449392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m3950449392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t4282066566  Transform_get_lossyScale_m3749612506 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_lossyScale_m2289856381(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_lossyScale_m2289856381 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m2289856381_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_lossyScale_m2289856381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m2289856381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C"  Transform_t1659122786 * Transform_FindChild_m2149912886 (Transform_t1659122786 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Transform_t1659122786 * L_1 = Transform_Find_m3950449392(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern Il2CppClass* Enumerator_t3875554846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m688365631_MetadataUsageId;
extern "C"  Il2CppObject * Transform_GetEnumerator_m688365631 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m688365631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3875554846 * L_0 = (Enumerator_t3875554846 *)il2cpp_codegen_object_new(Enumerator_t3875554846_il2cpp_TypeInfo_var);
		Enumerator__ctor_m856668951(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t1659122786 * Transform_GetChild_m4040462992 (Transform_t1659122786 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_GetChild_m4040462992_ftn) (Transform_t1659122786 *, int32_t);
	static Transform_GetChild_m4040462992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m4040462992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m856668951 (Enumerator_t3875554846 * __this, Transform_t1659122786 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1838268607 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t1659122786 * L_2 = Transform_GetChild_m4040462992(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1095530856 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t1659122786 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2107810675(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m258411155 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern Il2CppClass* UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MethodInfo_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m143130254_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m143130254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m143130254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t3575612635 * L_0 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MethodInfo_var);
		UnhandledExceptionEventHandler_t2544755120 * L_2 = (UnhandledExceptionEventHandler_t2544755120 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m2560140041(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m1729490677(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2454016344;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m4033241923 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3134093121 * ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t3134093121 * L_0 = ___args1;
		NullCheck(L_0);
		Il2CppObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m3487900308(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t3991598821 *)IsInstClass(L_1, Exception_t3991598821_il2cpp_TypeInfo_var));
		Exception_t3991598821 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t3991598821 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m3142367935(NULL /*static, unused*/, _stringLiteral2454016344, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3111784619;
extern const uint32_t UnhandledExceptionHandler_PrintException_m3142367935_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_PrintException_m3142367935 (Il2CppObject * __this /* static, unused */, String_t* ___title0, Exception_t3991598821 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m3142367935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title0;
		Exception_t3991598821 * L_1 = ___e1;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Exception_t3991598821 * L_4 = ___e1;
		NullCheck(L_4);
		Exception_t3991598821 * L_5 = Exception_get_InnerException_m1427945535(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Exception_t3991598821 * L_6 = ___e1;
		NullCheck(L_6);
		Exception_t3991598821 * L_7 = Exception_get_InnerException_m1427945535(L_6, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m3142367935(NULL /*static, unused*/, _stringLiteral3111784619, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3200128946;
extern const uint32_t UnityException__ctor_m1176480467_MetadataUsageId;
extern "C"  void UnityException__ctor_m1176480467 (UnityException_t3473321374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m1176480467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m3870771296(__this, _stringLiteral3200128946, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m743662351 (UnityException_t3473321374 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C"  void UnityException__ctor_m2780758535 (UnityException_t3473321374 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		Exception__ctor_m1328171222(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m2001840532 (UnityException_t3473321374 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		Exception__ctor_m3602014243(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityString_Format_m427603113_MetadataUsageId;
extern "C"  String_t* UnityString_Format_m427603113 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m427603113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m1517109030 (Vector2_t4282066565 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m1517109030_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	Vector2__ctor_m1517109030(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3517435145;
extern const uint32_t Vector2_get_Item_m2185542843_MetadataUsageId;
extern "C"  float Vector2_get_Item_m2185542843 (Vector2_t4282066565 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m2185542843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = __this->get_x_1();
		return L_3;
	}

IL_001b:
	{
		float L_4 = __this->get_y_2();
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral3517435145, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector2_get_Item_m2185542843_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_get_Item_m2185542843(_thisAdjusted, ___index0, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_Scale_m1743563745 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3928503634;
extern const uint32_t Vector2_ToString_m3859776067_MetadataUsageId;
extern "C"  String_t* Vector2_ToString_m3859776067 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m3859776067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral3928503634, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Vector2_ToString_m3859776067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_ToString_m3859776067(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m128434585 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
extern "C"  int32_t Vector2_GetHashCode_m128434585_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_GetHashCode_m128434585(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Equals_m3404198849_MetadataUsageId;
extern "C"  bool Vector2_Equals_m3404198849 (Vector2_t4282066565 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m3404198849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector2_t4282066565_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector2_t4282066565 *)((Vector2_t4282066565 *)UnBox (L_1, Vector2_t4282066565_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Vector2_Equals_m3404198849_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_Equals_m3404198849(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C"  float Vector2_SqrMagnitude_m4007443280 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t4282066565  Vector2_get_zero_m199872368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_Addition_m1173049553 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_Subtraction_m2097149401 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t4282066565  Vector2_op_Multiply_m1738245082 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_Multiply_m4063148634 (Il2CppObject * __this /* static, unused */, float ___d0, Vector2_t4282066565  ___a1, const MethodInfo* method)
{
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m1927481448 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___lhs0, Vector2_t4282066565  ___rhs1, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___lhs0;
		Vector2_t4282066565  L_1 = ___rhs1;
		Vector2_t4282066565  L_2 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m4007443280(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t4282066565  Vector2_op_Implicit_m4083860659 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  Vector2_op_Implicit_m482286037 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector3_t4282066566  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2926210380(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_pinvoke(const Vector2_t4282066565& unmarshaled, Vector2_t4282066565_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t4282066565_marshal_pinvoke_back(const Vector2_t4282066565_marshaled_pinvoke& marshaled, Vector2_t4282066565& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_pinvoke_cleanup(Vector2_t4282066565_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_com(const Vector2_t4282066565& unmarshaled, Vector2_t4282066565_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t4282066565_marshal_com_back(const Vector2_t4282066565_marshaled_com& marshaled, Vector2_t4282066565& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_com_cleanup(Vector2_t4282066565_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
